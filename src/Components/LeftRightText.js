import React, {Component} from 'react';
import {View, Text, StyleSheet} from 'react-native';
import colors from '../styles/colors';
import {useSelector} from 'react-redux';
import {
  moderateScale,
  moderateVerticalScale,
  textScale,
} from '../styles/responsiveSize';

const LeftRightText = ({
  leftText,
  rightText,
  isDarkMode,
  MyDarkTheme,
  leftTextStyle,
  rightTextStyle,
  marginBottom = 12,
  style,
}) => {
  const {ThemeColors, fontFamily} = useSelector(
    state => state?.init?.initAppData,
  );

  const styles = stylesFunc({ThemeColors, fontFamily});

  return (
    <View
      style={{
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginBottom: moderateVerticalScale(marginBottom),
        marginHorizontal: moderateScale(10),
        ...style,
      }}>
      <Text
        style={{
          ...styles.textStyle,
          color: colors.blackOpacity43,
          ...leftTextStyle,
          flex: 1,
        }}>
        {leftText}
      </Text>
      <Text
        style={{
          ...styles.textStyle,
          color: colors.blackOpacity43,
          ...rightTextStyle,
        }}>
        {rightText}
      </Text>
    </View>
  );
};

export function stylesFunc({ThemeColors, fontFamily}) {
  const styles = StyleSheet.create({
    textStyle: {
      fontFamily: fontFamily.medium,
      textAlign: 'left',
      fontSize: textScale(12),
    },
  });
  return styles;
}

export default React.memo(LeftRightText);
