//const API_BASE_URL = 'https://orders.editerzframes.com/api';

const API_BASE_URL = 'https://dev.editerzframes.com/api'; // development

const getApiUrl = endpoint => API_BASE_URL + endpoint;

//INIT APP Setting api

export const APP_INITIAL_SETTINGS = getApiUrl('/header');
export const LOGIN = getApiUrl('/login');
export const SIGNUP = getApiUrl('/register');
export const SOCIAL_LOGIN = getApiUrl('/socialLogin');
export const HOME = getApiUrl('/homepage');

// CHECK USER VERIFY STATUS

export const ISUSERVERIFIED = getApiUrl('/getUser');
export const REQUEST_OTP = getApiUrl('/sendOTP');
export const VERIFYEMAIL = getApiUrl('/verifyemail');
export const VERIFYMOBILE = getApiUrl('/verifyphone');

//OTP FOR FORGOT PASSWORD
export const REQUESTOTPFORPASSWORD = getApiUrl('/forgotSendOTP');
export const VERIFYOTPFORPASSWORD = getApiUrl('/forgotVerifyemail');
export const FORGOTPASSWORD = getApiUrl('/resetPassword');

export const CATEGORYS = getApiUrl('/categoryListing');
export const VENDORPRODUCTLIST = getApiUrl('/vendorProducts');
export const VENDORLIST = getApiUrl('/vendorListing');
export const CATEGORIESDETAILS = getApiUrl('/category/details');
export const PRODUCTLISTING = getApiUrl('/productListing');
export const PRODUCTDETAILS = getApiUrl('/productDetail');
export const SEARCH = getApiUrl('/search');
export const SEARCHPRODUCT = getApiUrl('/searchProducts');
export const ADDTOCART = getApiUrl('/addToCart');
export const SINGLEITEMREMOVECART = getApiUrl('/removeFromCart');
export const EMPTYCART = getApiUrl('/emptyCart');

export const GETCART = getApiUrl('/getCart');
export const CARTITEMINCREMENTDECREMENT = getApiUrl('/changeItemCount');
export const ADDORREMOVEWISHLIST = getApiUrl('/addOrRemoveWishlist');
export const GETWISHLIST = getApiUrl('/getWishlist');

//addresses Apis

export const ADDNEWADDRESS = getApiUrl('/address/store');
export const GETALLADDRESS = getApiUrl('/addresses');
export const UPDATEADDRESS = getApiUrl('/address/update');
export const EDITADDRESS = getApiUrl('/address/edit');
export const DELETEADDRESS = getApiUrl('/address/delete');
export const PRIMARYADDRESS = getApiUrl('/address/makePrimary');

export const VERIFYPHONE = getApiUrl('/phoneLogin');
export const SAVEUSERINFO = getApiUrl('/saveUserInfo');
export const APPLYCOUPON = getApiUrl('/applyCoupon');
export const REMOVECOUPON = getApiUrl('/removeCoupon');
export const CHECKOUT = getApiUrl('/checkout');
export const CREATEPAYMENT = getApiUrl('/createPayment');
export const UPDATEUSER = getApiUrl('/updateUser');
export const LANGUAGES = getApiUrl('/languages');
export const SEARCHADDRESS = getApiUrl('/address/show');
export const ALLPAYMENTMETHODS = getApiUrl('/paymentMethods');

// coupon apis

export const ALLCOUPON = getApiUrl('/getCoupons');
export const SEARCHCOUPON = getApiUrl('/coupon/search');
