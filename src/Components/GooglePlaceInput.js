import React, {useState} from 'react';
import {View, Text, Image} from 'react-native';
import {GooglePlacesAutocomplete} from 'react-native-google-places-autocomplete';
import imagePath from '../constants/imagePath';
import colors from '../styles/colors';
import {
  moderateScale,
  moderateVerticalScale,
  width,
} from '../styles/responsiveSize';

export default function GooglePlaceInput({
  handleAddressOnKeyUp,
  userCurrentLocation = '',
}) {
  return (
    <GooglePlacesAutocomplete
      minLength={2}
      isRowScrollable={true}
      placeholder="Search location"
      onPress={(data, details = null) => {
        handleAddressOnKeyUp(data.description);
      }}
      textInputProps={{
        onChangeText: text => {
          handleAddressOnKeyUp(text);
        },
        value: false,
      }}
      query={{
        key: 'AIzaSyBX6elzS6SDSE60EDdmF3WiZEDFpCGmHeI-4',
        language: 'en',
        components: 'country:in',
      }}
      nearbyPlacesAPI="GooglePlacesSearch"
      enablePoweredByContainer={false}
      styles={{
        container: {
          elevation: 10,
          marginHorizontal: moderateScale(10),
          backgroundColor: 'red',
        },
        textInputContainer: {
          borderRadius: 5,
          height: moderateVerticalScale(45),
          paddingHorizontal: 10,
          width: moderateScale(width / 1.6),
          backgroundColor: 'red',
        },
        description: {
          fontWeight: 'bold',
        },
        predefinedPlacesDescription: {
          color: '#1faadb',
        },
      }}
    />
  );
}
