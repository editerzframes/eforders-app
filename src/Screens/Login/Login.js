import React, {useState} from 'react';
import {View, Text, Image, TouchableOpacity} from 'react-native';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import HeaderWithFilters from '../../Components/HeaderWithFilters';
import TextInputWithLabel from '../../Components/TextInputWithLabel';
import WrapperContainer from '../../Components/WrapperContainer';
import imagePath from '../../constants/imagePath';
import colors from '../../styles/colors';
import {
  height,
  moderateScale,
  moderateVerticalScale,
  width,
  textScale,
} from '../../styles/responsiveSize';
import {stylesFunc} from './styles';
import ButtonComponent from '../../Components/ButtonComponent';
import fontFamily from '../../styles/fontFamily';
import navigationStrings from '../../constants/navigationStrings';
import {
  GoogleSignin,
  statusCodes,
} from '@react-native-google-signin/google-signin';
import {
  LoginManager,
  AccessToken,
  GraphRequest,
  GraphRequestManager,
} from 'react-native-fbsdk';
import validations from '../../utils/validations';
import {showMessage, hideMessage} from 'react-native-flash-message';
import actions from '../../redux/actions';
import {getColorCodeWithOpactiyNumber} from '../../utils/helperFunctions';
import {useSelector} from 'react-redux';
import ModalView from '../../Components/Modal';
import {
  CodeField,
  Cursor,
  useBlurOnFulfill,
  useClearByFocusCell,
} from 'react-native-confirmation-code-field';
import validator from 'validator';

const CELL_COUNT = 4;
export default function Login({navigation}) {
  const [state, setState] = useState({
    securePassord: true,
    secureEnrollmentCode: true,
    email: '',
    password: '',
    focusInput: '',
    forgotpassword: '',
    isModalVisible: false,
    isEmailOtp: false,
    userEmailOtp: '',
    isShowPasswordField: false,
    newpassword: '',
  });

  const {
    securePassord,
    secureEnrollmentCode,
    email,
    password,
    focusInput,
    forgotpassword,
    isModalVisible,
    isEmailOtp,
    userEmailOtp,
    isShowPasswordField,
    newpassword,
  } = state;

  const updateState = data => setState(state => ({...state, ...data}));
  const {ThemeColors, fontFamily, appAllKeysData} = useSelector(
    state => state?.init?.initAppData,
  );

  const styles = stylesFunc({fontFamily, ThemeColors});

  console.log(appAllKeysData, 'appAllKeysData in login screen ');

  const _securePasswordEnterEnable = type => {
    if (type === 'code') {
      updateState({
        secureEnrollmentCode: secureEnrollmentCode ? false : true,
      });
    } else {
      updateState({
        securePassord: securePassord ? false : true,
      });
    }
  };

  const _moveToNextScreen = type => {
    if (type === 'login') {
    } else if (type === 'signup') {
      navigation.navigate(navigationStrings.REGISTER);
    } else if (type === 'otpverification') {
      navigation.navigate(navigationStrings.OTP_VERIFICATION);
    }
  };

  const isValidData = () => {
    const error = validations({
      email: validator.trim(email),
      password: password,
    });
    if (error) {
      showMessage({
        message: error,
        type: 'danger',
      });
      return;
    }
    return true;
  };

  const onLogin = () => {
    let {callingCode} = state;
    const checkValid = isValidData();
    if (!checkValid) {
      return;
    }

    let data = {
      email: validator.trim(email),
      password: password,
      // device_token: device_token,
      // device_type: Platform.OS,
    };

    actions
      .login(data)
      .then(res => {
        showMessage({
          message: res.message,
          type: 'success',
        });
        // navigation.navigate(navigationStrings.HOME);
      })
      .catch(error =>
        showMessage({
          message: error.message,
          type: 'danger',
        }),
      );
  };

  //google login code

  const googleLogin = async () => {
    GoogleSignin.configure();
    try {
      // await GoogleSignin.hasPlayServices();
      // await GoogleSignin.revokeAccess();
      await GoogleSignin.signOut();
      const userInfo = await GoogleSignin.signIn();
      console.log(userInfo, 'userInfouserInfo');
      const data = {
        email: userInfo.user.email,
        name: userInfo.user.name,
        type: 'google',
        social_id: userInfo.user.id,
      };
      actions
        .socialLogin(data)
        .then(res => {
          console.log(res, 'response');
          showMessage({
            message: res.message,
            type: 'success',
          });
        })
        .catch(error => {
          showMessage({
            message: error.message,
            type: 'danger',
          });
        });
    } catch (error) {
      console.log(error, 'errorerror');
      if (error.code === statusCodes.SIGN_IN_CANCELLED) {
        return error;
      } else if (error.code === statusCodes.IN_PROGRESS) {
        return error;
      } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
        return error;
      } else {
        return error;
      }
    }
  };

  // facebook login code
  const loginWithFacebook = () => {
    // Attempt a login using the Facebook login dialog asking for default permissions.
    LoginManager.logInWithPermissions(['public_profile']).then(
      login => {
        if (login.isCancelled) {
          console.log('Login cancelled');
        } else {
          AccessToken.getCurrentAccessToken().then(data => {
            const accessToken = data.accessToken.toString();
            getInfoFromToken(accessToken);
          });
        }
      },
      error => {
        console.log('Login fail with error: ' + error);
      },
    );
  };

  const getInfoFromToken = token => {
    const PROFILE_REQUEST_PARAMS = {
      fields: {
        string: 'id, name,  first_name, last_name',
      },
    };
    const profileRequest = new GraphRequest(
      '/me',
      {token, parameters: PROFILE_REQUEST_PARAMS},
      (error, result) => {
        if (error) {
          console.log('login info has error: ' + error);
        } else {
          console.log('result:', result);
          const data = result.email
            ? {
                email: result.email,
                name: result.name,
                type: 'facebook',
                social_id: result.id,
              }
            : {
                name: result.name,
                type: 'facebook',
                social_id: result.id,
              };
          console.log(data, 'data object');
          actions
            .socialLogin(data)
            .then(res => {
              showMessage({
                message: res.message,
                type: 'success',
              });
            })
            .catch(error => {
              console.log(error, 'error');
              showMessage({
                message: error.message,
                type: 'danger',
              });
            });
        }
      },
    );
    new GraphRequestManager().addRequest(profileRequest).start();
  };

  const _onChangeText = key => val => {
    updateState({[key]: val});
  };

  const userFocus = firstname => {
    updateState({
      focusInput: firstname,
    });
  };

  const onVerifyEmail = () => {
    let data = {
      email: forgotpassword,
      otp_type: 'forgot',
      otp: userEmailOtp,
    };

    console.log(data, 'userEmailOtp');

    actions
      .verifyOtpForForgotPassword(data)
      .then(res => {
        updateState({
          isShowPasswordField: true,
        });
        showMessage({
          message: res.message,
          type: 'success',
        });
      })
      .catch(error =>
        showMessage({
          message: error.message,
          type: 'danger',
        }),
      );
  };

  const onEmailOtpSend = () => {
    let data = {
      email: forgotpassword,
    };

    actions
      .requestOtpForForgotPassword(data)
      .then(res => {
        console.log(res, 'opt for email');
        updateState({
          isEmailOtp: true,
        });
        showMessage({
          message: res.message,
          type: 'success',
        });
      })
      .catch(error =>
        showMessage({
          message: error.message,
          type: 'danger',
        }),
      );
  };

  const onSetNewPassword = () => {
    const data = {
      email: forgotpassword,
      password: newpassword,
    };
    actions
      .forgotPassword(data)
      .then(res => {
        console.log(res, 'opt for email');
        updateState({
          isModalVisible: false,
        });
        showMessage({
          message: res.message,
          type: 'success',
        });
      })
      .catch(error =>
        showMessage({
          message: error.message,
          type: 'danger',
        }),
      );
  };

  const modalMainContent = () => {
    return (
      <View
        style={{
          marginHorizontal: moderateVerticalScale(20),
        }}>
        <TouchableOpacity
          onPress={() =>
            updateState({
              isModalVisible: false,
            })
          }>
          <Image
            style={{alignSelf: 'flex-end', tintColor: colors.black}}
            source={imagePath?.close}
          />
        </TouchableOpacity>
        <View>
          {!isEmailOtp && (
            <TextInputWithLabel
              placeholder={'Your registered email'}
              customTextStyle={{
                marginVertical: moderateVerticalScale(10),
                borderColor:
                  focusInput === 'forgotpassword'
                    ? ThemeColors?.primary_color
                    : getColorCodeWithOpactiyNumber(
                        colors.textGreyLight.substr(1),
                        60,
                      ),
                borderRadius: 8,
                height: moderateVerticalScale(55),
                paddingHorizontal: moderateScale(16),
                fontSize: textScale(15),
              }}
              onChangeText={_onChangeText('forgotpassword')}
              userFocus={focus => userFocus('forgotpassword')}
              rightIcon={imagePath.rightarrow}
              onPressRightIcon={() => onEmailOtpSend()}
            />
          )}

          {isEmailOtp && (
            <>
              {!isShowPasswordField && (
                <CodeField
                  value={userEmailOtp}
                  onChangeText={_onChangeText('userEmailOtp')}
                  cellCount={CELL_COUNT}
                  rootStyle={styles.root}
                  blurOnSubmit
                  keyboardType="number-pad"
                  textContentType="oneTimeCode"
                  selectionColor={colors.themeColor}
                  renderCell={({index, symbol, isFocused}) => (
                    <Text
                      key={index}
                      style={[styles.cell, isFocused && styles.focusCell]}>
                      {symbol || (isFocused ? <Cursor /> : null)}
                    </Text>
                  )}
                />
              )}

              {isShowPasswordField && (
                <TextInputWithLabel
                  placeholder={'New Password'}
                  customTextStyle={{
                    marginVertical: moderateVerticalScale(10),
                    borderColor:
                      focusInput === 'forgotpassword'
                        ? ThemeColors?.primary_color
                        : getColorCodeWithOpactiyNumber(
                            colors.textGreyLight.substr(1),
                            60,
                          ),
                    borderRadius: 8,
                    height: moderateVerticalScale(55),
                    paddingHorizontal: moderateScale(16),
                    fontSize: textScale(15),
                  }}
                  onChangeText={_onChangeText('newpassword')}
                  userFocus={focus => userFocus('forgotpassword')}
                />
              )}

              <TouchableOpacity
                style={{
                  paddingVertical: moderateVerticalScale(20),
                  backgroundColor: ThemeColors?.primary_color,
                  alignItems: 'center',
                  justifyContent: 'center',
                  borderRadius: moderateScale(40),
                  marginVertical: moderateVerticalScale(20),
                }}
                onPress={
                  isShowPasswordField ? onSetNewPassword : onVerifyEmail
                }>
                <Text
                  style={{
                    color: colors.white,
                    fontFamily: fontFamily?.bold,
                    fontSize: textScale(15),
                  }}>
                  {isShowPasswordField ? 'Set New Password' : 'Verify Email'}
                </Text>
                {/* <Image source={imagePath.back} /> */}
              </TouchableOpacity>
            </>
          )}
        </View>
      </View>
    );
  };

  return (
    <WrapperContainer bgColor={{backgroundColor: colors.white}}>
      <View style={styles.headerContiner}>
        <HeaderWithFilters
          leftIcon={imagePath.backArrow}
          // centerTitle={'Sign In'}
        />
      </View>

      <View
        style={{
          // backgroundColor: colors.textGreyB,
          paddingBottom: moderateVerticalScale(10),
          paddingHorizontal: moderateScale(20),
        }}>
        <Text
          style={{
            color: colors.black,
            fontSize: textScale(20),
            fontFamily: fontFamily?.bold,
            marginVertical: moderateScale(10),
            textAlign: 'center',
          }}>
          Welcome back!
        </Text>
        <Text
          style={{
            color: colors.textGreyLight,
            fontSize: textScale(15),
            fontFamily: fontFamily?.regular,
            textAlign: 'center',
          }}>
          Log in to your existant account
        </Text>
      </View>
      <KeyboardAwareScrollView
        style={{
          height: height - 20,
          marginHorizontal: moderateScale(10),
          paddingVertical: moderateVerticalScale(10),
        }}
        showsVerticalScrollIndicator={false}>
        <View
          style={{
            backgroundColor: colors.white,
            shadowColor: colors.black,
            shadowOffset: {width: 0.3, height: 0.3},
            shadowOpacity: 0.3,
            shadowRadius: 8,
            elevation: 5,
            paddingHorizontal: moderateScale(10),
            paddingVertical: moderateVerticalScale(20),
            borderRadius: 10,
            marginHorizontal: 10,
          }}>
          <TextInputWithLabel
            placeholder={' Your email address'}
            customTextStyle={{
              marginVertical: moderateVerticalScale(10),
              borderColor:
                focusInput === 'email'
                  ? ThemeColors?.primary_color
                  : getColorCodeWithOpactiyNumber(
                      colors.textGreyLight.substr(1),
                      60,
                    ),
              borderRadius: 8,
              height: moderateVerticalScale(55),
              paddingHorizontal: moderateScale(16),
              fontSize: textScale(15),
            }}
            onChangeText={_onChangeText('email')}
            userFocus={focus => userFocus('email')}
            label={'E-mail Address'}
          />

          <View style={{marginVertical: moderateVerticalScale(10)}}>
            <TextInputWithLabel
              placeholder={'Password'}
              customTextStyle={{
                marginVertical: moderateVerticalScale(10),
                borderRadius: 8,
                borderColor:
                  focusInput === 'password'
                    ? ThemeColors?.primary_color
                    : getColorCodeWithOpactiyNumber(
                        colors.textGreyLight.substr(1),
                        60,
                      ),
                height: moderateVerticalScale(55),
                paddingHorizontal: moderateScale(16),
                fontSize: textScale(15),
              }}
              rightIcon={
                securePassord ? imagePath.close_eye : imagePath.open_eye
              }
              onPressRightIcon={() => _securePasswordEnterEnable('password')}
              secureTextEntry={securePassord}
              onChangeText={_onChangeText('password')}
              userFocus={focus => userFocus('password')}
              label={'Password'}
            />
          </View>

          <View>
            <TouchableOpacity
              onPress={() =>
                updateState({
                  isModalVisible: true,
                })
              }>
              <Text style={styles.forgotPasswordText}>Forgot Password ?</Text>
            </TouchableOpacity>
          </View>

          <ButtonComponent
            btnText={'Sign In'}
            btnStyle={{
              backgroundColor: ThemeColors?.primary_color,
              height: moderateVerticalScale(60),
              width: moderateScale(width / 2),
              borderRadius: 50,
              marginVertical: moderateVerticalScale(20),
              alignSelf: 'center',
            }}
            textStyle={{fontFamily: fontFamily?.bold, fontSize: textScale(18)}}
            onPress={onLogin}
          />
        </View>

        {appAllKeysData?.google_login || appAllKeysData?.facebook_login ? (
          <Text style={styles.screenTitleTextStyle}>Or connect with</Text>
        ) : null}
        <View
          style={[
            styles.socialIconContainer,
            {marginVertical: moderateVerticalScale(10)},
          ]}>
          {appAllKeysData?.google_login ? (
            <TouchableOpacity
              style={
                appAllKeysData?.google_login && appAllKeysData?.facebook_login
                  ? [styles.socialIconInnerContainer]
                  : [styles.socialIconInnerContainer, {width: width / 1.25}]
              }
              onPress={googleLogin}>
              <Image
                style={styles.socialIconStyle}
                source={imagePath.icon_google}
              />
              <Text style={styles.socialHeadingTextStyle}>Google</Text>
            </TouchableOpacity>
          ) : null}

          {appAllKeysData?.facebook_login ? (
            <TouchableOpacity
              style={
                appAllKeysData?.google_login && appAllKeysData?.facebook_login
                  ? [styles.socialIconInnerContainer]
                  : [styles.socialIconInnerContainer, {width: width / 1.25}]
              }
              onPress={loginWithFacebook}>
              <Image
                style={styles.socialIconStyle}
                source={imagePath.icon_facebook}
              />
              <Text
                style={[
                  styles.socialHeadingTextStyle,
                  {marginHorizontal: moderateScale(10)},
                ]}>
                Facebook
              </Text>
            </TouchableOpacity>
          ) : null}
        </View>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'center',
            marginVertical: moderateVerticalScale(20),
          }}>
          <Text style={styles.createNewAccountText}>
            New User Create Account ?
          </Text>
          <TouchableOpacity onPress={() => _moveToNextScreen('signup')}>
            <Text style={styles.signUpTextStyle}>Sign Up</Text>
          </TouchableOpacity>
        </View>
      </KeyboardAwareScrollView>
      <ModalView
        mainViewStyle={{width: width}}
        modalStyle={{
          position: 'absolute',
          bottom: 0,
          marginHorizontal: moderateScale(0),
          marginVertical: 0,
        }}
        isVisible={isModalVisible}
        modalMainContent={modalMainContent}
      />
    </WrapperContainer>
  );
}
