import React, {useState} from 'react';
import {View, Text, Image} from 'react-native';
import {useSelector} from 'react-redux';
import TransparentButtonWithTxtAndIcon from '../../Components/ButtonComponent';
import TextInputWithLabel from '../../Components/TextInputWithLabel';
import WrapperContainer from '../../Components/WrapperContainer';
import actions from '../../redux/actions';
import colors from '../../styles/colors';
import fontFamily from '../../styles/fontFamily';
import {
  height,
  moderateScale,
  moderateVerticalScale,
  textScale,
  width,
} from '../../styles/responsiveSize';
import {getColorCodeWithOpactiyNumber} from '../../utils/helperFunctions';
import {showMessage, hideMessage} from 'react-native-flash-message';
import strings from '../../constants/lang';
import {stylesFunc} from './styles';
import {ScrollView} from 'react-native-gesture-handler';
import imagePath from '../../constants/imagePath';

export default function AccountSettings() {
  const logedInUser = useSelector(state => state?.auth?.userData);

  const [state, setState] = useState({
    name: logedInUser?.name,
    email: logedInUser?.email,
    password: '',
    focusInput: '',
  });

  const {name, email, focusInput} = state;

  const {ThemeColors, fontFamily} = useSelector(
    state => state?.init?.initAppData,
  );

  const styles = stylesFunc({fontFamily, ThemeColors});

  const updateState = data => setState(state => ({...state, ...data}));

  const _onChangeText = key => val => {
    updateState({[key]: val});
  };

  const updateUser = () => {
    const data = {
      name: name,
      email: email,
    };
    actions
      .upadateUser(logedInUser?.access_token, data)
      .then(res => {
        console.log(res, 'ressssssssssssssssssssss');
        actions.updateProfile({...logedInUser, ...res?.data});

        showMessage({
          message: res?.message,
          type: 'success',
        });
      })
      .catch(error => {
        showMessage({
          message: error.message,
          type: 'danger',
        });
      });
  };

  const userFocus = firstname => {
    updateState({
      focusInput: firstname,
    });
  };

  return (
    <WrapperContainer>
      <View
        style={{
          flex: 0.3,
          backgroundColor: getColorCodeWithOpactiyNumber(
            ThemeColors?.primary_color.substr(1),
            40,
          ),
        }}>
        <Image
          style={{
            height: moderateVerticalScale(100),
            width: moderateScale(100),
            borderRadius: 8,
            position: 'absolute',
            bottom: -20,
            alignSelf: 'center',
          }}
          source={{uri: 'https://shorturl.at/fswE0'}}
        />
      </View>
      <ScrollView style={{flex: 0.7}}>
        <View>
          <Text
            style={{
              marginTop: moderateVerticalScale(40),
              textAlign: 'center',
              color: ThemeColors?.primary_color,
              fontSize: textScale(15),
            }}>
            {strings.UPLOAD}
          </Text>
          <View
            style={{
              paddingHorizontal: moderateScale(10),
              justifyContent: 'center',
              marginTop: moderateScale(20),
            }}>
            <TextInputWithLabel
              placeholder={strings.NAME}
              customTextStyle={{
                marginVertical: moderateVerticalScale(10),
                borderColor:
                  focusInput === 'name'
                    ? ThemeColors?.primary_color
                    : getColorCodeWithOpactiyNumber(
                        colors.textGreyLight.substr(1),
                        60,
                      ),
                borderRadius: 8,
                height: moderateVerticalScale(55),
                paddingHorizontal: moderateScale(16),
                fontSize: textScale(15),
              }}
              onChangeText={_onChangeText('name')}
              userFocus={focus => userFocus('name')}
              label={strings.NAME}
              value={name}
              onChangeText={_onChangeText('name')}
            />
          </View>
          <View
            style={{
              paddingHorizontal: moderateScale(10),
              justifyContent: 'center',
            }}>
            <TextInputWithLabel
              placeholder={strings.EMAILADDRESS}
              customTextStyle={{
                marginVertical: moderateVerticalScale(10),
                borderColor:
                  focusInput === 'email'
                    ? ThemeColors?.primary_color
                    : getColorCodeWithOpactiyNumber(
                        colors.textGreyLight.substr(1),
                        60,
                      ),
                borderRadius: 8,
                height: moderateVerticalScale(55),
                paddingHorizontal: moderateScale(16),
                fontSize: textScale(15),
              }}
              onChangeText={_onChangeText('email')}
              userFocus={focus => userFocus('email')}
              label={strings.EMAILADDRESS}
              value={email}
            />
          </View>
          <View
            style={{
              paddingHorizontal: moderateScale(10),
              justifyContent: 'center',
            }}>
            <TextInputWithLabel
              placeholder={strings.PHONENUMBER}
              customTextStyle={{
                marginVertical: moderateVerticalScale(10),
                borderColor:
                  focusInput === 'phonenumber'
                    ? ThemeColors?.primary_color
                    : getColorCodeWithOpactiyNumber(
                        colors.textGreyLight.substr(1),
                        60,
                      ),
                borderRadius: 8,
                height: moderateVerticalScale(55),
                paddingHorizontal: moderateScale(16),
                fontSize: textScale(15),
              }}
              value={logedInUser?.mobile_number}
              onChangeText={_onChangeText('phonenumber')}
              userFocus={focus => userFocus('phonenumber')}
              label={strings.PHONENUMBER}
            />
          </View>
          {/* <View
            style={{
              paddingHorizontal: moderateScale(10),
              justifyContent: 'center',
            }}>
            <TextInputWithLabel
              placeholder={strings.PASSWORD}
              customTextStyle={{
                marginVertical: moderateVerticalScale(10),
                borderColor:
                  focusInput === 'password'
                    ? ThemeColors?.primary_color
                    : getColorCodeWithOpactiyNumber(
                        colors.textGreyLight.substr(1),
                        60,
                      ),
                borderRadius: 8,
                height: moderateVerticalScale(55),
                paddingHorizontal: moderateScale(16),
                fontSize: textScale(15),
              }}
              onChangeText={_onChangeText('password')}
              userFocus={focus => userFocus('password')}
              label={strings.PASSWORD}
            />
          </View> */}
          <View
            style={{
              paddingHorizontal: moderateScale(10),
              marginVertical: moderateVerticalScale(20),
              alignSelf: 'center',
            }}>
            <TransparentButtonWithTxtAndIcon
              btnStyle={{
                backgroundColor: ThemeColors?.primary_color,
                borderRadius: 8,
                height: moderateVerticalScale(48),
                width: width - 40,
              }}
              btnText={strings.EDITPROFILE}
              textStyle={{fontFamily: fontFamily.bold, fontSize: textScale(16)}}
              onPress={() => updateUser()}
            />
          </View>
        </View>
      </ScrollView>
    </WrapperContainer>
  );
}
