import React from 'react';
import {StyleSheet, View} from 'react-native';
import Modal from 'react-native-modal';
import imagePath from '../constants/imagePath';
import colors from '../styles/colors';
import {moderateVerticalScale} from '../styles/responsiveSize';
import Header from './Header';

export default function ModalView({
  isVisible = false,
  onClose,
  modalStyle,
  transistionIn = 200,
  transistionOut = 200,
  leftIcon = imagePath.back,
  centerTitle,
  textStyle,
  horizontLine = true,
  rightIcon = '',
  onPressLeft,
  onPressRight,
  modalMainContent = () => {},
  modalBottomContent = () => {},
  mainViewStyle = {},
  topCustomComponent = () => {},
}) {
  return (
    <Modal
      isVisible={isVisible}
      onBackButtonPress={onClose}
      onBackdropPress={onClose}
      backdropTransitionInTiming={transistionIn}
      backdropTransitionInTiming={transistionOut}
      style={[styles.modalStyle, modalStyle]}>
      <View
        style={{
          // flex: 1,
          backgroundColor: colors.white,
          // borderRadius: 15,
          paddingTop: moderateVerticalScale(30),
          borderTopStartRadius: 15,
          borderTopEndRadius: 15,

          ...mainViewStyle,
        }}>
        {/* //Header */}
        {topCustomComponent ? (
          topCustomComponent()
        ) : (
          <Header
            leftIcon={leftIcon}
            centerTitle={centerTitle}
            rightIcon={rightIcon}
            onPressLeft={onPressLeft}
          />
        )}

        {/* center content */}
        <>{modalMainContent()}</>

        {/* bottom content */}
        <>{modalBottomContent()}</>
      </View>
    </Modal>
  );
}

const styles = StyleSheet.create({
  modalStyle: {
    marginHorizontal: moderateVerticalScale(20),
    marginVertical: moderateVerticalScale(50),
    // backgroundColor: colors.white,
    borderRadius: 15,
  },
});
