import React from 'react';
import {StyleSheet} from 'react-native';
import colors from '../../styles/colors';
import commonStyles from '../../styles/commonStyles';
import fontFamily from '../../styles/fontFamily';
import {
  height,
  moderateScale,
  moderateVerticalScale,
  textScale,
  width,
} from '../../styles/responsiveSize';

export const CELL_SIZE = 55;
export const CELL_BORDER_RADIUS = 8;
export const DEFAULT_CELL_BG_COLOR = '#fff';
export const NOT_EMPTY_CELL_BG_COLOR = '#3557b7';
export const ACTIVE_CELL_BG_COLOR = '#f7fafe';

export function stylesFunc({fontFamily, ThemeColors}) {
  const styles = StyleSheet.create({
    headerContiner: {
      paddingVertical: moderateVerticalScale(10),
    },
    screenTitleTextStyle: {
      fontFamily: fontFamily?.regular,
      fontSize: textScale(16),
      marginHorizontal: moderateScale(20),
      marginVertical: moderateVerticalScale(20),
      textAlign: 'center',
    },
    socialHeadingTextStyle: {
      fontFamily: fontFamily?.bold,
      fontSize: textScale(16),
      marginHorizontal: moderateScale(20),
    },
    socialIconContainer: {
      flexDirection: 'row',
      marginHorizontal: moderateScale(25),
      justifyContent: 'space-between',
    },
    socialIconInnerContainer: {
      flexDirection: 'row',
      borderColor: colors.textGreyLight,
      borderWidth: 0.5,
      width: width / 2.6,
      justifyContent: 'center',
      alignItems: 'center',
      borderRadius: 5,
      paddingVertical: moderateVerticalScale(10),
    },
    descriptionTextStyle: {
      fontSize: textScale(14),
      fontFamily: fontFamily?.medium,
      textAlign: 'center',
    },
    forgotPasswordText: {
      marginVertical: moderateVerticalScale(10),
      textAlign: 'center',
      fontFamily: fontFamily?.medium,
      fontSize: textScale(14),
      color: ThemeColors?.primary_color,
    },
    createNewAccountText: {
      fontFamily: fontFamily?.medium,
      fontSize: textScale(14),
    },
    signUpTextStyle: {
      marginHorizontal: moderateScale(8),
      fontFamily: fontFamily?.medium,
      fontSize: textScale(14),
      color: ThemeColors?.primary_color,
    },
  });
  return styles;
}
