export default {
  EFORDERS: 'EF Orders',
  VIEWALL: 'View All',
  ENDSIN: 'Ends in',
  PRODUCTDETAILS: 'Product Details',
  ADDTOCART: 'Add to Cart',
  BUYNOW: 'Buy Now',
  COLOR: 'Color',
  SIZE: 'Size',
  STOCK: 'In Stock',
  COLOUR: 'Colour',
  SIZE: 'Sizes',
  RELATEDPRODUCTS: 'Related Products',
  ALLPRODUCTS: 'All Products',
  CART: 'Cart',
  WISHLIST: 'Wishlist',
  PROFILE: 'Profile',
  CHECKOUT: 'Checkout',
  PAYMENT: 'Payment',
  SETTINGS: 'Settings',
  ADDRESSES: 'Addresses',
  ADDNEWADDRESS: 'Add New Address',
  CHOOSEYOURLOCATION: ' Choose your Location',
  SELECTDELIVERYLOCATION:
    '  Select a delivery location to see product availablity and delivery options',
  DEFAULTADDRESS: 'Default Address',
  ADDANADDRESS: 'Add an address or pick-up point',
  USEMYCURRENTLOCATION: 'Use my current location',
  SEARCH: 'Search',
  TOTALITEMS: 'Total Items',
  TOTALPRICE: 'Total Price',
  TOTALDISCOUNT: 'Total Discount',
  FINALPRICE: 'Final Price',
  CHECKOUT: 'CHECKOUT',
  MYPROFILE: ' My Profile',
  NOTIFICATION: 'Notification',
  SETTINGS: 'Settings',
  PAYMENT: 'Payment',
  ABOUTUS: 'About Us',
  SHAREAPP: ' Share app',
  LOGOUT: 'Log Out',
  LOGIN: 'Login',
  UPLOAD: ' Upload',
  NAME: 'Name',
  EMAILADDRESS: 'Email Address',
  PHONENUMBER: 'Phone Number',
  PASSWORD: 'Password',
  EDITPROFILE: 'Edit Profile',
  LANGUAGE: 'Language',
  ENGLISH: 'English',
  FILTER: 'FILTER',
  SORTBY: 'SORT BY',
  SEARCHPRODUCT: 'Search Product',
  FINDADDRESS: 'Find an address..',
  UPDATEDELIVERYLOCATION: 'Update delivery location',
  EDIT: 'Edit',
  REMOVE: 'Remove',
  ADDANEWADDRESS: 'Add a new address',
  USECURRENTLOCATION: 'Use current location',
  OR: 'OR',
  FIRSTNAME: 'First Name',
  LASTNAME: 'Last Name',
  PINCODE: 'PIN code',
  FLATHOUSENUMBER: 'Flat, House no.,Building, Company,Apartment',
  AREACOLONY: 'Area Colony, Street,Sector,Village',
  LANDMARK: 'Landmark,e.g. near Apollo Hospital',
  TOWNCITY: 'Town/City',
  STATE: 'State',
  COUNTRY: 'Country',
  UPDATE: 'Update',
  APPLY: 'Apply',
  SHIPPINGADDRESS: ' Shipping Address',
  ADDSHIPPINGNOTES: 'Add shipping notes',
  STROENAME: 'Store Name',
  SELLERNAME: 'Seller Name',
  QUENTITY: 'Quentity',
  SHIPPINGMETHOD: ' Shipping Method',
  ENTERVOUCHERCODE: 'Enter voucher code',
  CHOOSEPAYMENTMETHOD: 'Choose Payment Method',
  HOME: 'Home',
  CART: 'Cart',
  WISHLIST: 'Wishlist',
  ACCOUNT: 'Account',
  NOT_ADDED_CART_DETAIL_FOR_PAYMENT_METHOD: 'No Card Selected For Payment',
  CATEGORY: 'Category',
  LOADING: 'Loading',
  APPLY: 'APPLY',
  CHANGEOFFER: 'Change offer',
  CODE: 'Code',
  APPLIED: 'applied',
  APPLYYOURCOUPON: 'Apply your Coupon',
  OFFER: 'Offers',
  ORDERDETAILS: 'OrderDetails',
  OFF: 'OFF',
  VIEW_PROFILE: ' View Profile',
};
