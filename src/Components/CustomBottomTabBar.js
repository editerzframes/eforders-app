import React, {Fragment, useRef} from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';

import colors from '../styles/colors';
import {
  moderateScale,
  moderateVerticalScale,
  textScale,
  width,
} from '../styles/responsiveSize';

import {useSelector} from 'react-redux';
import {getColorCodeWithOpactiyNumber} from '../utils/helperFunctions';

export default function CustomBottomTabBar({
  state,
  descriptors,
  navigation,
  bottomTabNotify,
  ...props
}) {
  const styles = stylesData();
  const {ThemeColors, fontFamily} = useSelector(
    state => state?.init?.initAppData,
  );
  const cartBadge = useSelector(state => state?.cart?.cartBadge);
  const logedInUser = useSelector(state => state?.auth?.userData);

  console.log(cartBadge, 'cartBadgecartBadgecartBadge');

  return (
    <View
      style={{
        backgroundColor: colors.backgroundColor,
      }}>
      <View>
        <View style={[styles.tabBarStyle]}>
          {state.routes.map((route, index) => {
            const {options} = descriptors[route.key];
            const isFocused = state.index === index;
            const label =
              options.tabBarLabel !== undefined
                ? options.tabBarLabel
                : options.title !== undefined
                ? options.title
                : route.name;
            const onPress = () => {
              const event = navigation.emit({
                type: 'tabPress',
                target: route.key,
                canPreventDefault: true,
              });

              if (!isFocused && !event.defaultPrevented) {
                navigation.navigate(route.name);
              }
            };

            return (
              <Fragment key={route.name}>
                <TouchableOpacity
                  accessibilityRole="button"
                  accessibilityStates={isFocused ? ['selected'] : []}
                  accessibilityLabel={options.tabBarAccessibilityLabel}
                  testID={options.tabBarTestID}
                  onPress={onPress}
                  // onLongPress={onLongPress}
                  style={{
                    flex: isFocused ? 0.4 : 0.2,
                  }}>
                  <View
                    style={{
                      backgroundColor: isFocused
                        ? getColorCodeWithOpactiyNumber(
                            ThemeColors?.primary_color?.substr(1),
                            20,
                          )
                        : colors.white,
                      flexDirection: 'row',
                      alignItems: 'center',
                      justifyContent: 'space-evenly',
                      marginHorizontal: moderateScale(20),
                      paddingVertical: moderateVerticalScale(10),
                      borderRadius: 10,
                      borderWidth: 0.6,
                      borderColor: isFocused
                        ? ThemeColors?.primary_color
                        : colors.white,
                    }}>
                    {options.tabBarIcon({focused: isFocused})}
                    {index === 2 &&
                    cartBadge > 0 &&
                    logedInUser?.access_token ? (
                      <View
                        style={{
                          backgroundColor: 'red',
                          position: 'absolute',
                          zIndex: 1,
                          paddingHorizontal: 4,
                          paddingVertical: 2,
                          borderRadius: 50,
                          alignItems: 'center',
                          justifyContent: 'center',
                          right: isFocused ? -10 : 0,
                          top: isFocused ? -10 : 0,
                        }}>
                        <Text
                          style={{
                            color: colors.white,
                            fontSize: isFocused ? textScale(12) : textScale(8),
                          }}>
                          {cartBadge}
                        </Text>
                      </View>
                    ) : null}
                    {isFocused ? (
                      <Text
                        style={{
                          ...props.labelStyle,
                          ...styles.labelStyle,
                          color: ThemeColors?.primary_color,
                          fontFamily: fontFamily?.bold,
                          opacity: 1,
                          fontSize: textScale(14),
                        }}>
                        {label}
                      </Text>
                    ) : null}
                  </View>
                </TouchableOpacity>
              </Fragment>
            );
          })}
        </View>
      </View>
    </View>
  );
}

export function stylesData() {
  const styles = StyleSheet.create({
    tabBarStyle: {
      flexDirection: 'row',
      backgroundColor: colors.white,
      elevation: 11,

      paddingVertical: moderateVerticalScale(22),
    },
    labelStyle: {
      fontSize: textScale(16),
    },
  });
  return styles;
}
