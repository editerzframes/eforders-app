import React from 'react';
import navigationStrings from '../constants/navigationStrings';
import {createStackNavigator} from '@react-navigation/stack';
import {
  Account,
  AccountSettings,
  AddressList,
  EditAddress,
  Favourites,
  Settings,
} from '../Screens';

const Stack = createStackNavigator();

export default function Accounts() {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name={navigationStrings.ACCOUNT}
        options={{
          headerShown: false,
        }}
        component={Account}
      />
      <Stack.Screen
        name={navigationStrings.ADDRESSLIST}
        options={{
          headerShown: false,
        }}
        component={AddressList}
      />
      <Stack.Screen
        name={navigationStrings.EDITADDRESS}
        options={{
          headerShown: false,
        }}
        component={EditAddress}
      />
      <Stack.Screen
        name={navigationStrings.ACCOUNTSETTINGS}
        options={{
          headerShown: false,
        }}
        component={AccountSettings}
      />

      <Stack.Screen
        name={navigationStrings.FAVOURITES}
        options={{
          headerShown: false,
        }}
        component={Favourites}
      />
    </Stack.Navigator>
  );
}
