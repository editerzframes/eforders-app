import React from 'react';
import navigationStrings from '../constants/navigationStrings';
import {createStackNavigator} from '@react-navigation/stack';
import {
  Account,
  AddressList,
  AllVendorProducts,
  AllVendors,
  Basket,
  Checkout,
  EditAddress,
  GlobalSearch,
  Home,
  Login,
  ProductDetails,
  ProductList,
  ProductVideoPlayer,
  SubcategorieswithProducts,
  VideoPlayer,
} from '../Screens';
import {verticalAnimation} from '../utils/helperFunctions';

const Stack = createStackNavigator();

export default function HomeStack() {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name={navigationStrings.HOME}
        options={{
          headerShown: false,
        }}
        component={Home}
      />
      <Stack.Screen
        name={navigationStrings.PRODUCT_LIST}
        options={{
          headerShown: false,
        }}
        component={ProductList}
      />

      <Stack.Screen
        name={navigationStrings.SUBCATEGORIES}
        options={{
          headerShown: false,
        }}
        component={SubcategorieswithProducts}
      />
      <Stack.Screen
        name={navigationStrings.ADDRESSLIST}
        options={{
          headerShown: false,
        }}
        component={AddressList}
      />

      <Stack.Screen
        name={navigationStrings.GLOBALSEARCH}
        options={verticalAnimation}
        component={GlobalSearch}
      />
      <Stack.Screen
        name={navigationStrings.BASKET}
        options={{
          headerShown: false,
        }}
        component={Basket}
      />
      <Stack.Screen
        name={navigationStrings.ALLVENDORS}
        options={{
          headerShown: false,
        }}
        component={AllVendors}
      />
      <Stack.Screen
        name={navigationStrings.CHECKOUT}
        options={{
          headerShown: false,
        }}
        component={Checkout}
      />
    </Stack.Navigator>
  );
}
