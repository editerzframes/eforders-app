import React, {useEffect, useState} from 'react';
import {View, Text, TouchableOpacity, FlatList, Image} from 'react-native';
import HorizontalListCard from '../../Components/HorizontalListCard';
import TextInputWithLabel from '../../Components/TextInputWithLabel';
import WrapperContainer from '../../Components/WrapperContainer';
import imagePath from '../../constants/imagePath';
import strings from '../../constants/lang';
import colors from '../../styles/colors';
import fontFamily from '../../styles/fontFamily';
import {
  moderateScale,
  moderateVerticalScale,
  textScale,
} from '../../styles/responsiveSize';
import {getColorCodeWithOpactiyNumber} from '../../utils/helperFunctions';
import {cloneDeep, debounce} from 'lodash';
import actions from '../../redux/actions';
import {CircleListItem} from '../../Components/CircleListItem';
import styles from './styles';
import Header from '../../Components/Header';
import {useSelector} from 'react-redux';
import {showMessage} from 'react-native-flash-message';
import navigationStrings from '../../constants/navigationStrings';

export default function SubcategorieswithProducts(props) {
  const {navigation, route} = props;
  const paramData = route?.params?.selectedProduct;

  const [state, setState] = useState({
    pageNo: 1,
    limit: 12,
    allSubCategories: [],
    allProducts: [],
  });

  const {
    pageNo,
    limit,
    allSearchedProducts,
    searchedQuery,
    allSubCategories,
    allProducts,
  } = state;
  const updateState = data => setState(state => ({...state, ...data}));
  const logedInUser = useSelector(state => state?.auth?.userData);
  const totalCartBadge = useSelector(state => state?.cart?.cartBadge);
  const primaryAddress = useSelector(state => state?.home?.primaryAddress);

  useEffect(() => {
    onSearchProduct();
  }, []);

  const onSearchProduct = item => {
    let query = `/${item?.id ? item?.id : paramData?.id}`;

    let data = {
      latitude: primaryAddress?.latitude,
      longitude: primaryAddress?.longitude,
    };
    actions
      .categoriesDetails(query, data)
      .then(res =>
        updateState({
          allSubCategories: res?.categories?.data,
          allProducts: res?.products?.data,
        }),
      )
      .catch(error => console.log(error, 'error'));
  };

  //pagination of data
  const onEndReached = ({distanceFromEnd}) => {
    updateState({pageNo: pageNo + 1});
  };

  const onEndReachedDelayed = debounce(onEndReached, 1000, {
    leading: true,
    trailing: false,
  });

  const _onChangeText = key => val => {
    updateState({[key]: val});
  };

  const addToCart = (productId, productPrice) => {
    const data = {
      product_id: productId,
    };
    actions
      .addToCart(logedInUser?.access_token, data)
      .then(res => {
        showMessage({
          message: res.message,
          type: 'success',
        });
        getCartData();
        //actions.showCartBadge(1);
      })
      .catch(error => {
        showMessage({
          message: error.message,
          type: 'danger',
        });
      });
  };

  // get Cart data

  const getCartData = () => {
    actions
      .getCart({}, logedInUser?.access_token)
      .then(res => {
        console.log(res, 'response');

        actions.showCartBadge(res?.data?.item_count);
      })
      .catch(error => {
        showMessage({
          message: error.message,
          type: 'danger',
        });
      });
  };

  const onSubcategoryData = item => {
    onSearchProduct(item);
  };

  const _moveToNextScreen = (item, data) => {
    console.log(data, 'datatatata');
    navigation.navigate(item, {selectedProduct: data?.id});
  };

  const renderSearchedCategories = ({item, index}) => {
    return (
      <View style={{marginHorizontal: moderateScale(8)}}>
        <CircleListItem data={item} onPress={() => onSubcategoryData(item)} />
      </View>
    );
  };

  const searchResultsContainer = (data, index) => {
    return (
      <View>
        <HorizontalListCard
          data={data?.item}
          addTocart={addToCart}
          onPress={() =>
            _moveToNextScreen(navigationStrings.PRODUCT_DETAILS, data?.item)
          }
        />
      </View>
    );
  };

  const renderCategoryHeaderComponent = () => {
    return (
      <View
        style={
          allSubCategories?.length >= 4
            ? {
                alignItems: 'center',
                marginBottom: moderateVerticalScale(10),
              }
            : {marginBottom: moderateVerticalScale(10)}
        }>
        <FlatList
          horizontal
          data={allSubCategories}
          ItemSeparatorComponent={() => (
            <View style={{height: moderateVerticalScale(15)}} />
          )}
          renderItem={renderSearchedCategories}
          showsHorizontalScrollIndicator={false}
        />
      </View>
    );
  };

  return (
    <WrapperContainer
      bgColor={getColorCodeWithOpactiyNumber(colors.white.substr(1), 20)}>
      <View style={{paddingHorizontal: moderateScale(13)}}>
        <Header
          leftIcon={imagePath.back}
          textStyle={{color: colors.black}}
          centerTitle={paramData?.name}
          rightIcon={imagePath.cart}
          rightIconStyle={{tintColor: colors.black}}
          onPressRight={() => _moveToNextScreen(navigationStrings.CART_STACK)}
        />
        {totalCartBadge > 0 && logedInUser?.access_token ? (
          <View
            style={{
              backgroundColor: 'red',
              position: 'absolute',
              zIndex: 1,
              paddingHorizontal: 4,
              paddingVertical: 2,
              borderRadius: 50,
              alignItems: 'center',
              justifyContent: 'center',
              right: 6,
              top: 2,
            }}>
            <Text
              style={{
                color: colors.white,
                fontSize: textScale(8),
                fontFamily: fontFamily?.bold,
              }}>
              {totalCartBadge}
            </Text>
          </View>
        ) : null}
      </View>
      <View
        style={{
          marginTop: moderateVerticalScale(10),
          marginHorizontal: moderateScale(10),
        }}>
        <FlatList
          showsVerticalScrollIndicator={false}
          data={allProducts}
          ItemSeparatorComponent={() => (
            <View style={{height: moderateVerticalScale(5)}} />
          )}
          renderItem={searchResultsContainer}
          ListHeaderComponent={renderCategoryHeaderComponent}
          showsHorizontalScrollIndicator={false}
          onEndReached={onEndReachedDelayed}
          onEndReachedThreshold={0.5}
          ListFooterComponent={() => (
            <View style={{height: moderateVerticalScale(100)}} />
          )}
        />
      </View>
    </WrapperContainer>
  );
}
