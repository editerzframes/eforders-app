import React from 'react';
import {StyleSheet} from 'react-native';
import colors from '../../styles/colors';
import commonStyles from '../../styles/commonStyles';

import {
  height,
  moderateScale,
  moderateVerticalScale,
  textScale,
  width,
} from '../../styles/responsiveSize';
import {getColorCodeWithOpactiyNumber} from '../../utils/helperFunctions';

export function stylesFunc({fontFamily, ThemeColors}) {
  const styles = StyleSheet.create({
    addNewAddressText: {
      fontSize: textScale(14),
      fontFamily: fontFamily.bold,
      color: colors.black,
      marginVertical: moderateVerticalScale(10),
      marginHorizontal: moderateScale(10),
    },

    currentLocationButton: {
      backgroundColor: getColorCodeWithOpactiyNumber(
        colors.textGreyLight.substr(1),
        25,
      ),
      height: moderateVerticalScale(45),
      width: moderateScale(width / 1.1),
      alignSelf: 'center',
      borderColor: colors.textGreyLight,
      borderWidth: 0.5,
      borderRadius: 8,
      marginTop: moderateVerticalScale(10),
    },
    firstNameLastNameContainer: {
      flexDirection: 'row',
      marginTop: moderateVerticalScale(10),
      alignItems: 'center',
      alignSelf: 'center',
    },
    lineViewStyle: {
      height: 0.5,
      backgroundColor: colors.textGreyLight,
      width: width / 2.25,
    },
    addressFormContainer: {
      paddingHorizontal: moderateScale(20),
      marginBottom: moderateVerticalScale(20),
      backgroundColor: getColorCodeWithOpactiyNumber(
        colors.textGreyLight.substr(1),
        10,
      ),
    },
  });
  return styles;
}
