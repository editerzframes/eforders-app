import React, {useEffect, useRef, useState} from 'react';
import {View, Text, FlatList, Image, TouchableOpacity} from 'react-native';
import {useSelector} from 'react-redux';
import StepIndicator from 'react-native-step-indicator';
import {
  height,
  moderateScale,
  moderateVerticalScale,
  textScale,
  width,
} from '../styles/responsiveSize';

export default function CustomStepIndicator() {
  const primaryLanguage = useSelector(state => state?.home?.primaryLanguage);
  console.log(primaryLanguage, 'primaryLanguage');
  const [state, setState] = useState({
    currentPosition: 4,
    labels: [
      'Accepcted',
      'Proccessing',
      'Order Summary',
      'Out For Delivery',
      'Delivered',
    ],
  });

  const {currentPosition, labels} = state;
  const updateState = data => setState(state => ({...state, ...data}));
  const {ThemeColors, fontFamily} = useSelector(
    state => state?.init?.initAppData,
  );

  //  Indicator Style
  const customStyles = {
    stepIndicatorSize: 25,
    currentStepIndicatorSize: 30,
    separatorStrokeWidth: 2,
    currentStepStrokeWidth: 3,
    stepStrokeCurrentColor: ThemeColors?.primary_color,
    stepStrokeWidth: 3,
    stepStrokeFinishedColor: ThemeColors?.primary_color,
    stepStrokeUnFinishedColor: '#aaaaaa',
    separatorFinishedColor: ThemeColors?.primary_color,
    separatorUnFinishedColor: '#aaaaaa',
    stepIndicatorFinishedColor: ThemeColors?.primary_color,
    stepIndicatorUnFinishedColor: '#ffffff',
    stepIndicatorCurrentColor: '#ffffff',
    stepIndicatorLabelFontSize: 13,
    currentStepIndicatorLabelFontSize: 13,
    stepIndicatorLabelCurrentColor: ThemeColors?.primary_color,
    stepIndicatorLabelFinishedColor: '#ffffff',
    stepIndicatorLabelUnFinishedColor: '#aaaaaa',
    labelColor: '#999999',
    labelSize: 13,
    currentStepLabelColor: ThemeColors?.primary_color,
  };

  return (
    <View>
      <StepIndicator
        customStyles={customStyles}
        currentPosition={currentPosition}
        labels={labels}
        stepCount={labels?.length}
      />
    </View>
  );
}
