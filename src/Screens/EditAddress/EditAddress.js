import React, {useState, useMemo} from 'react';
import {
  View,
  Text,
  ScrollView,
  PermissionsAndroid,
  TouchableOpacity,
} from 'react-native';
import TransparentButtonWithTxtAndIcon from '../../Components/ButtonComponent';
import TextInputWithLabel from '../../Components/TextInputWithLabel';
import WrapperContainer from '../../Components/WrapperContainer';
import colors from '../../styles/colors';

import {
  height,
  moderateScale,
  moderateVerticalScale,
  textScale,
  width,
} from '../../styles/responsiveSize';
import {getColorCodeWithOpactiyNumber} from '../../utils/helperFunctions';
import Geocoder from 'react-native-geocoding';
import {showMessage, hideMessage} from 'react-native-flash-message';
import {check, PERMISSIONS, RESULTS} from 'react-native-permissions';
import actions from '../../redux/actions';
import {useSelector} from 'react-redux';
import Header from '../../Components/Header';
import imagePath from '../../constants/imagePath';
import strings from '../../constants/lang';
import {chekLocationPermission} from '../../utils/permissions';
navigator.geolocation = require('react-native-geolocation-service');
import {stylesFunc} from './styles';
import {googlePlacesApi} from '../../utils/googlePlaceApi';
Geocoder.init('AIzaSyCwLPzcHGY93MND4Qm6ShNn2KOGZGoiSrM');

export default function EditAddress(props) {
  const {navigation, route} = props;
  const paramData = route?.params;

  const [state, setState] = useState({
    firstName: '',
    lastName: '',
    phoneNumber: paramData?.phone_number ? paramData?.phone_number : '',
    city: paramData?.city ? paramData?.city : '',
    stete: paramData?.state ? paramData?.state : '',
    country: paramData?.country ? paramData?.country : '',
    houseNumber: paramData?.house_no ? paramData?.house_no : '',
    street: paramData?.street ? paramData?.street : '',
    landmark: paramData?.landmark ? paramData?.landmark : '',
    pinCode: paramData?.pincode ? paramData?.pincode : '',
    focusInput: '',
    addressTypeView: [
      {id: 1, addressType: 'Home'},
      {id: 2, addressType: 'Work'},
      {id: 3, addressType: 'Other'},
    ],
    selectedAddressType: {id: 1, addressType: 'Home'},
    currentLatitude: null,
    currentLongitude: null,
  });

  const {
    firstName,
    lastName,
    phoneNumber,
    city,
    stete,
    country,
    houseNumber,
    street,
    landmark,
    pinCode,
    focusInput,
    addressTypeView,
    selectedAddressType,
    currentLatitude,
    currentLongitude,
  } = state;

  const updateState = data => setState(state => ({...state, ...data}));
  const logedInUser = useSelector(state => state?.auth?.userData);
  const {ThemeColors, fontFamily} = useSelector(
    state => state?.init?.initAppData,
  );
  const styles = stylesFunc({fontFamily, ThemeColors});

  const currentLocation = () => {
    chekLocationPermission()
      .then(result => {
        if (result !== 'goback') {
          getCurrentLocation();
        }
      })
      .catch(error => console.log('error while accessing location ', error));
  };

  const getCurrentLocation = () => {
    return navigator.geolocation.default.getCurrentPosition(
      position => {
        Geocoder.from({
          latitude: position.coords.latitude,
          longitude: position.coords.longitude,
        })
          .then(json => {
            var addressComponent = json.results[0].formatted_address;
            var geometry = json.results[0].address_components;
            let details = {};
            details = {
              formatted_address: addressComponent,
              geometry: {
                location: {
                  lat: position.coords.latitude,
                  lng: position.coords.longitude,
                },
              },
              address_components: json.results[0].address_components,
            };

            updateState({
              houseNumber: geometry[0]?.short_name,
              street: geometry[3]?.short_name,
              stete: geometry[4]?.short_name,
              city: geometry[4]?.short_name,
              pinCode: geometry[geometry?.length - 1]?.short_name,
              country: geometry[geometry?.length - 2]?.long_name,
              currentLatitude: position.coords.latitude,
              currentLongitude: position.coords.longitude,
            });
          })
          .catch(error => alert(JSON.stringify(error), 'error'));
      },
      error => console.log(error.message),
      {enableHighAccuracy: true, timeout: 20000},
    );
  };

  const addNewAddress = () => {
    if (currentLatitude && currentLongitude) {
      _onNewAddAddress();
    } else {
      _onfindLatLongUsingManualAddress();
    }
  };

  const _onfindLatLongUsingManualAddress = () => {
    const queryData = {
      houseNumber: houseNumber,
      street: street,
      city: city,
      pinCode: pinCode,
      stete: stete,
      country: country,
    };
    const apiKey = 'AIzaSyCwLPzcHGY93MND4Qm6ShNn2KOGZGoiSrM&v=3';

    googlePlacesApi(queryData, apiKey)
      .then(res => {
        onLatlongusingapi(res?.results[0]?.geometry?.location);
      })
      .catch(error => {
        console.log(error, 'error from manual address');
      });
  };

  const onLatlongusingapi = latlongUsingManualAddress => {
    _onNewAddAddress(latlongUsingManualAddress);
  };

  const _onNewAddAddress = latlongUsingManualAddress => {
    const data = {
      name: `${firstName} ${lastName}`,
      phone_number: phoneNumber,
      city: city,
      state: stete,
      country: country,
      house_no: houseNumber,
      street: street,
      landmark: landmark,
      pincode: pinCode,
      address_type: selectedAddressType?.addressType,
      latitude: latlongUsingManualAddress?.lat
        ? latlongUsingManualAddress?.lat
        : currentLatitude,
      longitude: latlongUsingManualAddress?.lng
        ? latlongUsingManualAddress?.lng
        : currentLongitude,
    };
    console.log(data, 'data address');
    actions
      .addNewAddress(logedInUser?.access_token, data)
      .then(res => {
        showMessage({
          message: res?.message,
          type: 'success',
        });
        navigation.goBack();
      })
      .catch(error => {
        showMessage({
          message: error?.message,
          type: 'danger',
        });
      });
  };

  const _onChangeText = key => val => {
    updateState({[key]: val});
  };

  const userFocus = firstname => {
    updateState({
      focusInput: firstname,
    });
  };

  const userselectedAddressType = item => {
    updateState({
      selectedAddressType: item,
    });
  };

  const updateAddress = () => {
    let query = `/${paramData?.id}`;
    const data = {
      name: `${firstName} ${lastName}`,
      phone_number: phoneNumber,
      city: city,
      state: stete,
      country: country,
      house_no: houseNumber,
      street: street,
      landmark: landmark,
      pincode: pinCode,
      address_type: selectedAddressType?.addressType,
    };

    actions
      .updateAddress(query, data, logedInUser?.access_token)
      .then(res => {
        showMessage({
          message: res.message,
          type: 'success',
        });
        navigation.goBack();
      })
      .catch(error => {
        showMessage({
          message: error.message,
          type: 'danger',
        });
      });
  };

  return (
    <WrapperContainer
      statusBarColor={getColorCodeWithOpactiyNumber(
        colors.white.substr(1),
        20,
      )}>
      <View style={{paddingHorizontal: moderateScale(10)}}>
        <Header
          leftIcon={imagePath.back}
          textStyle={{color: colors.black, fontFamily: fontFamily.bold}}
          centerTitle={strings.ADDNEWADDRESS}
          // rightIcon={imagePath.search}
        />
      </View>

      <ScrollView showsVerticalScrollIndicator={false}>
        <View>
          <TransparentButtonWithTxtAndIcon
            textStyle={{color: colors.black, fontFamily: fontFamily.bold}}
            btnText={strings.USECURRENTLOCATION}
            btnStyle={styles.currentLocationButton}
            onPress={() => currentLocation()}
          />
          <View style={styles.firstNameLastNameContainer}>
            <View style={styles.lineViewStyle} />
            <Text
              style={{
                color: colors.black,
                fontFamily: fontFamily.bold,
                fontSize: textScale(16),
              }}>
              {strings.OR}
            </Text>
            <View style={styles.lineViewStyle} />
          </View>
          <View style={styles.addressFormContainer}>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
              }}>
              <View style={{width: width / 2.3}}>
                <TextInputWithLabel
                  placeholder={strings.FIRSTNAME}
                  customTextStyle={{
                    height: moderateVerticalScale(50),
                    borderColor:
                      focusInput == 'firstname'
                        ? ThemeColors?.primary_color
                        : colors.white,
                    backgroundColor: colors.white,
                    paddingHorizontal: moderateScale(10),
                    fontFamily: fontFamily.bold,
                  }}
                  onChangeText={_onChangeText('firstName')}
                  userFocus={focus => userFocus('firstname')}
                />
              </View>
              <View style={{width: width / 2.3}}>
                <TextInputWithLabel
                  placeholder={strings.LASTNAME}
                  customTextStyle={{
                    height: moderateVerticalScale(50),
                    borderColor:
                      focusInput == 'lastname'
                        ? ThemeColors?.primary_color
                        : colors.white,
                    backgroundColor: colors.white,
                    paddingHorizontal: moderateScale(10),
                    fontFamily: fontFamily.bold,
                  }}
                  onChangeText={_onChangeText('lastName')}
                  userFocus={focus => userFocus('lastname')}
                />
              </View>
            </View>

            <TextInputWithLabel
              placeholder={strings.PHONENUMBER}
              customTextStyle={{
                height: moderateVerticalScale(50),
                borderColor:
                  focusInput == 'phonenumber'
                    ? ThemeColors?.primary_color
                    : colors.white,
                backgroundColor: colors.white,
                paddingHorizontal: moderateScale(10),
                fontFamily: fontFamily.bold,
              }}
              onChangeText={_onChangeText('phoneNumber')}
              userFocus={focus => userFocus('phonenumber')}
              value={phoneNumber}
            />
            <TextInputWithLabel
              placeholder={strings.PINCODE}
              customTextStyle={{
                height: moderateVerticalScale(50),
                borderColor:
                  focusInput == 'pincode'
                    ? ThemeColors?.primary_color
                    : colors.white,
                backgroundColor: colors.white,
                paddingHorizontal: moderateScale(10),
                fontFamily: fontFamily.bold,
              }}
              onChangeText={_onChangeText('pinCode')}
              userFocus={focus => userFocus('pincode')}
              value={pinCode}
            />
            <TextInputWithLabel
              placeholder={strings.FLATHOUSENUMBER}
              customTextStyle={{
                height: moderateVerticalScale(50),
                borderColor:
                  focusInput == 'housenumber'
                    ? ThemeColors?.primary_color
                    : colors.white,
                backgroundColor: colors.white,
                paddingHorizontal: moderateScale(10),
                fontFamily: fontFamily.bold,
              }}
              onChangeText={_onChangeText('houseNumber')}
              userFocus={focus => userFocus('housenumber')}
              value={houseNumber}
            />
            <TextInputWithLabel
              placeholder={strings.AREACOLONY}
              customTextStyle={{
                height: moderateVerticalScale(50),
                borderColor:
                  focusInput == 'street'
                    ? ThemeColors?.primary_color
                    : colors.white,
                backgroundColor: colors.white,
                paddingHorizontal: moderateScale(10),
                fontFamily: fontFamily.bold,
              }}
              onChangeText={_onChangeText('street')}
              userFocus={focus => userFocus('street')}
              value={street}
            />
            <TextInputWithLabel
              placeholder={strings.LANDMARK}
              customTextStyle={{
                height: moderateVerticalScale(50),
                borderColor:
                  focusInput == 'landmark'
                    ? ThemeColors?.primary_color
                    : colors.white,
                backgroundColor: colors.white,
                paddingHorizontal: moderateScale(10),
                fontFamily: fontFamily.bold,
              }}
              onChangeText={_onChangeText('landmark')}
              userFocus={focus => userFocus('landmark')}
              value={landmark}
            />
            <TextInputWithLabel
              placeholder={strings.TOWNCITY}
              customTextStyle={{
                height: moderateVerticalScale(50),
                borderColor:
                  focusInput == 'city'
                    ? ThemeColors?.primary_color
                    : colors.white,
                backgroundColor: colors.white,
                paddingHorizontal: moderateScale(10),
                fontFamily: fontFamily.bold,
              }}
              onChangeText={_onChangeText('city')}
              userFocus={focus => userFocus('city')}
              value={city}
            />
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
              }}>
              <View style={{width: width / 2.3}}>
                <TextInputWithLabel
                  placeholder={strings.STATE}
                  customTextStyle={{
                    height: moderateVerticalScale(50),
                    borderColor:
                      focusInput == 'state'
                        ? ThemeColors?.primary_color
                        : colors.white,
                    backgroundColor: colors.white,
                    paddingHorizontal: moderateScale(10),
                    fontFamily: fontFamily.bold,
                  }}
                  onChangeText={_onChangeText('stete')}
                  userFocus={focus => userFocus('state')}
                  value={stete}
                />
              </View>
              <View style={{width: width / 2.3}}>
                <TextInputWithLabel
                  placeholder={strings.COUNTRY}
                  customTextStyle={{
                    height: moderateVerticalScale(50),
                    borderColor:
                      focusInput == 'country'
                        ? ThemeColors?.primary_color
                        : colors.white,
                    backgroundColor: colors.white,
                    paddingHorizontal: moderateScale(10),
                    fontFamily: fontFamily.bold,
                  }}
                  onChangeText={_onChangeText('country')}
                  userFocus={focus => userFocus('country')}
                  value={country}
                />
              </View>
            </View>

            <View
              style={{
                flexDirection: 'row',
                marginVertical: moderateVerticalScale(30),
              }}>
              {addressTypeView?.map((item, index) => {
                return (
                  <TouchableOpacity
                    style={{
                      marginRight: moderateScale(20),
                      backgroundColor:
                        selectedAddressType?.id == item?.id
                          ? ThemeColors?.primary_color
                          : getColorCodeWithOpactiyNumber(
                              ThemeColors?.primary_color.substr(1),
                              20,
                            ),
                      paddingHorizontal: moderateScale(20),
                      paddingVertical: moderateVerticalScale(10),
                      borderRadius: 20,
                    }}
                    onPress={() => userselectedAddressType(item)}>
                    <Text
                      style={{
                        color:
                          selectedAddressType?.id == item?.id
                            ? colors.white
                            : ThemeColors?.primary_color,
                        fontFamily: fontFamily.bold,
                      }}>
                      {item?.addressType}
                    </Text>
                  </TouchableOpacity>
                );
              })}
            </View>

            <TransparentButtonWithTxtAndIcon
              textStyle={{
                color: colors.white,
                fontFamily: fontFamily.bold,
                fontSize: textScale(14),
              }}
              btnText={paramData?.country ? strings.UPDATE : strings.APPLY}
              btnStyle={{
                backgroundColor: ThemeColors?.primary_color,
                height: moderateVerticalScale(50),
                width: moderateScale(width / 1.6),
                marginVertical: moderateVerticalScale(10),
                alignSelf: 'center',
                borderRadius: 50,
              }}
              onPress={
                paramData?.country
                  ? () => updateAddress()
                  : () => addNewAddress()
              }
            />
          </View>
        </View>
      </ScrollView>
    </WrapperContainer>
  );
}
