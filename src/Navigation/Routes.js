import * as React from 'react';
//import NavigationService from './navigation/NavigationService';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';

import AuthStack from './AuthStack';
import MainStack from './MainStack';
import navigationStrings from '../constants/navigationStrings';
import NullStack from './NullStack';
import {useSelector} from 'react-redux';

const Stack = createStackNavigator();

export default function Routes() {
  const logedInUser = useSelector(state => state?.auth?.userData);

  if (true) {
    return (
      <NavigationContainer>
        <Stack.Navigator>
          {!!logedInUser?.access_token ? (
            <>{MainStack(Stack)}</>
          ) : (
            <>{AuthStack(Stack)}</>
          )}

          {/* <>{MainStack(Stack)}</> */}
        </Stack.Navigator>
      </NavigationContainer>
    );
  } else {
    return <></>;
  }
}
