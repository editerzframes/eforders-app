import React from 'react';
import {
  View,
  Text,
  TextInput,
  StyleSheet,
  TouchableOpacity,
  Image,
} from 'react-native';
import {
  moderateScale,
  moderateVerticalScale,
  textScale,
  width,
} from '../styles/responsiveSize';
import colors from '../styles/colors';
import commonStyles, {hitSlopProp} from '../styles/commonStyles';
import fontFamily from '../styles/fontFamily';
import {useSelector} from 'react-redux';

const TextInputWithLabel = ({
  label,
  onChangeText,
  value,
  active = false,
  secureTextEntry = false,
  rightIcon,
  customTextStyle = {},
  placeholder = '',
  onPress = () => {},
  onPressRightIcon = () => {},
  userFocus,
  placeholdertextstyle,
  autoFocus = false,
  ...rest
}) => {
  const {ThemeColors, fontFamily} = useSelector(
    state => state?.init?.initAppData,
  );
  const styles = stylesFunc({fontFamily, ThemeColors});

  let currentColor = colors.textGreyLight;
  return (
    <View>
      <Text
        style={{
          ...commonStyles.mediumFont16,
          color: colors.textGreyLight,
          // marginBottom: moderateVerticalScale(7),
        }}>
        {label}
      </Text>
      <View style={{flexDirection: 'row'}}>
        <TextInput
          onFocus={focus => userFocus(focus)}
          selectionColor={ThemeColors?.primary_color}
          {...rest}
          placeholder={placeholder}
          secureTextEntry={secureTextEntry}
          // onFocus={onFocus}
          style={{
            flex: 1,
            ...styles.textInput,
            borderColor: currentColor,
            height: moderateVerticalScale(40),
            ...customTextStyle,
          }}
          onChangeText={onChangeText}
          value={value}
        />
        <View style={{position: 'absolute', end: 20, alignSelf: 'center'}}>
          {!!rightIcon && (
            <TouchableOpacity
              hitSlop={hitSlopProp}
              onPress={onPressRightIcon}
              style={{
                alignItems: 'center',
                justifyContent: 'center',
                marginLeft: 6,
              }}>
              <Image
                style={{tintColor: colors.textGreyLight}}
                source={rightIcon}
              />
            </TouchableOpacity>
          )}
        </View>
      </View>
    </View>
  );
};

export default TextInputWithLabel;

export function stylesFunc({fontFamily, ThemeColors}) {
  const styles = StyleSheet.create({
    textInput: {
      borderWidth: 1,
      borderColor: ThemeColors?.primary_color,
      height: moderateVerticalScale(30),
      fontSize: moderateVerticalScale(17.5),
      fontFamily: fontFamily?.regular,
      paddingVertical: 0,
      textAlignVertical: 'center',
      borderRadius: 5,
    },
  });

  return styles;
}
