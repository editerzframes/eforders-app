import {StyleSheet} from 'react-native';
import colors from '../../styles/colors';
import fontFamily from '../../styles/fontFamily';
import {
  height,
  moderateScale,
  moderateVerticalScale,
  textScale,
  width,
} from '../../styles/responsiveSize';

export function stylesFunc({fontFamily, ThemeColors}) {
  const styles = StyleSheet.create({
    container: {
      height: height,
      backgroundColor: colors.black,
    },
    InnerMainBlackContainer: {
      height: height,
      backgroundColor: colors.black,
    },
    userInfoContainer: {
      flex: 0.08,
      backgroundColor: colors.black,
      justifyContent: 'center',
      marginHorizontal: moderateScale(20),
    },
    InnerMainWhiteContainer: {
      flex: 0.92,
      backgroundColor: colors.backgroundColor,
      borderTopLeftRadius: 50,
      borderTopRightRadius: 50,
      justifyContent: 'center',
    },
    screenTitle: {
      color: colors.white,
      fontFamily: fontFamily.bold,
      fontSize: textScale(20),
    },
    flatListContainer: {
      marginTop: moderateVerticalScale(10),
      flex: 1,
      marginHorizontal: moderateScale(10),
      marginBottom: moderateVerticalScale(105),
    },
    buttonInnerViewStyle: {
      position: 'absolute',
      left: 0,
      right: 0,
      top: 0,
      bottom: 0,
    },
    btnTextContainer: {flex: 1, justifyContent: 'center', alignItems: 'center'},

    btnTextStyle: {
      color: colors.white,
      textAlign: 'center',
      fontSize: textScale(22),
      fontFamily: fontFamily.semiBold,
      marginTop: moderateVerticalScale(-15),
    },
  });

  return styles;
}
