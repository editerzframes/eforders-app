import {StyleSheet} from 'react-native';
import colors from '../../styles/colors';
import fontFamily from '../../styles/fontFamily';
import {
  height,
  moderateScale,
  moderateVerticalScale,
  textScale,
  width,
} from '../../styles/responsiveSize';

export const styles = StyleSheet.create({
  container: {
    height: height,
    backgroundColor: colors.black,
  },
  InnerMainBlackContainer: {
    height: height,
    backgroundColor: colors.black,
  },
  userInfoContainer: {
    flex: 0.08,
    backgroundColor: colors.black,
    justifyContent: 'center',
    marginHorizontal: moderateScale(20),
  },
  InnerMainWhiteContainer: {
    flex: 0.92,
    backgroundColor: colors.backgroundColor,
    borderTopLeftRadius: 50,
    borderTopRightRadius: 50,
    alignItems: 'center',
  },
  screenTitle: {
    color: colors.white,
    fontFamily: fontFamily.bold,
    fontSize: textScale(20),
  },
  flatListContainer: {
    marginTop: moderateVerticalScale(10),
    flex: 1,
    marginBottom: moderateVerticalScale(105),
  },
  listFooterContainer: {
    height: height / 2.4,
    alignItems: 'center',
    marginVertical: moderateVerticalScale(20),
  },
  listFooterContainerShadowBox: {
    shadowOpacity: 0.08,
    shadowRadius: 10,
    borderRadius: 20,
    backgroundColor: colors.white,
    width: moderateScale(width / 1.24),
    height: height / 3.2,
  },
  listFooterContainerShadowBoxInnerContainer: {
    width: '90%',
    height: '55%',
    alignSelf: 'center',
    paddingVertical: moderateVerticalScale(10),
    paddingHorizontal: moderateScale(10),
    marginTop: moderateVerticalScale(10),
    flexDirection: 'column',
    justifyContent: 'space-evenly',
  },
  productAddedDetails: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  addedproductLabel: {
    fontSize: textScale(16),
    fontFamily: fontFamily.Medium,
  },
  checkoutButtonContainer: {
    marginBottom: moderateVerticalScale(20),
    justifyContent: 'center',
    marginLeft: moderateScale(5),
    alignItems: 'center',
  },
});
