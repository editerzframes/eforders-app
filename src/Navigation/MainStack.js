import React from 'react';
import navigationStrings from '../constants/navigationStrings';
import {
  EditAddress,
  ProductDetails,
  ProductList,
  ProductVideoPlayer,
  Settings,
} from '../Screens';
import DrawerRoutes from './DrawerRoutes';
import TabRoutes from './TabRoutes';

export default function (Stack) {
  return (
    <>
      <Stack.Screen
        name={navigationStrings.DRAWER_ROUTES}
        options={{
          headerShown: false,
        }}
        component={DrawerRoutes}
      />
      <Stack.Screen
        name={navigationStrings.PRODUCT_DETAILS}
        options={{
          headerShown: false,
        }}
        component={ProductDetails}
      />
      <Stack.Screen
        name={navigationStrings.PRODUCTVIDEOPLAYER}
        options={{
          headerShown: false,
        }}
        component={ProductVideoPlayer}
      />
      <Stack.Screen
        name={navigationStrings.SETTINGS}
        options={{
          headerShown: false,
        }}
        component={Settings}
      />
      <Stack.Screen
        name={navigationStrings.EDITADDRESS}
        options={{
          headerShown: false,
        }}
        component={EditAddress}
      />
    </>
  );
}
