import React, {useEffect, useState} from 'react';
import {View, Text, TouchableOpacity, FlatList, Image} from 'react-native';
import HorizontalListCard from '../../Components/HorizontalListCard';
import TextInputWithLabel from '../../Components/TextInputWithLabel';
import WrapperContainer from '../../Components/WrapperContainer';
import imagePath from '../../constants/imagePath';
import strings from '../../constants/lang';
import colors from '../../styles/colors';
import fontFamily from '../../styles/fontFamily';
import {
  moderateScale,
  moderateVerticalScale,
  textScale,
} from '../../styles/responsiveSize';
import {getColorCodeWithOpactiyNumber} from '../../utils/helperFunctions';
import {cloneDeep, debounce} from 'lodash';
import actions from '../../redux/actions';
import {CircleListItem} from '../../Components/CircleListItem';
import styles from './styles';
import {useSelector} from 'react-redux';
import {showMessage} from 'react-native-flash-message';
import navigationStrings from '../../constants/navigationStrings';

export default function GlobalSearch(props) {
  const {navigation} = props;
  const [state, setState] = useState({
    pageNo: 1,
    limit: 12,
    allSearchedProducts: [],
    searchedQuery: '',
    allSearchedCategories: [],
  });

  const {
    pageNo,
    limit,
    allSearchedProducts,
    searchedQuery,
    allSearchedCategories,
  } = state;
  const updateState = data => setState(state => ({...state, ...data}));
  const logedInUser = useSelector(state => state?.auth?.userData);

  useEffect(() => {
    if (searchedQuery?.length >= 0) {
      onSearchProduct();
    } else {
      updateState({
        pageNo: 1,
        searchedQuery: '',
      });
    }
  }, [searchedQuery]);

  const onSearchProduct = () => {
    let query = `/${searchedQuery}?page=${
      allSearchedProducts?.length >= 10 ? pageNo : 1
    }`;
    actions
      .search(query)
      .then(res =>
        updateState({
          allSearchedProducts: res?.products?.data,
          allSearchedCategories: res?.categories?.data,
        }),
      )
      .catch(error => console.log(error, 'error'));
  };

  //pagination of data
  const onEndReached = ({distanceFromEnd}) => {
    updateState({pageNo: pageNo + 1});
  };

  const onEndReachedDelayed = debounce(onEndReached, 1000, {
    leading: true,
    trailing: false,
  });

  const _onChangeText = key => val => {
    updateState({[key]: val});
  };

  const addToCart = (productId, productPrice) => {
    const data = {
      product_id: productId,
    };
    actions
      .addToCart(logedInUser?.access_token, data)
      .then(res => {
        showMessage({
          message: res.message,
          type: 'success',
        });
        getCartData();
        //actions.showCartBadge(1);
      })
      .catch(error => {
        showMessage({
          message: error.message,
          type: 'danger',
        });
      });
  };

  // get Cart data

  const getCartData = () => {
    actions
      .getCart({}, logedInUser?.access_token)
      .then(res => {
        console.log(res, 'response');

        actions.showCartBadge(res?.data?.item_count);
      })
      .catch(error => {
        showMessage({
          message: error.message,
          type: 'danger',
        });
      });
  };

  const _moveToNextScreen = (item, data) => {
    navigation.navigate(item, {selectedProduct: data});
  };

  const renderSearchedCategories = ({item, index}) => {
    return (
      <View style={{marginHorizontal: moderateScale(8)}}>
        {index >= 7 ? (
          <TouchableOpacity activeOpacity={0.8}>
            <View style={styles.container}>
              <View style={styles.circularView}>
                <Text style={styles.categoryText}>{'More'}</Text>
              </View>
            </View>
          </TouchableOpacity>
        ) : (
          <CircleListItem
            data={item}
            onPress={() =>
              _moveToNextScreen(navigationStrings.SUBCATEGORIES, item)
            }
          />
        )}
      </View>
    );
  };

  const searchResultsContainer = (data, index) => {
    return (
      <HorizontalListCard
        data={data?.item}
        addTocart={addToCart}
        onPress={() =>
          _moveToNextScreen(navigationStrings.PRODUCT_DETAILS, data?.item?.id)
        }
      />
    );
  };

  const renderCategoryHeaderComponent = () => {
    return (
      <View
        style={allSearchedCategories?.length >= 4 && {alignItems: 'center'}}>
        <FlatList
          numColumns={4}
          data={allSearchedCategories}
          ItemSeparatorComponent={() => (
            <View style={{height: moderateVerticalScale(15)}} />
          )}
          renderItem={renderSearchedCategories}
          showsHorizontalScrollIndicator={false}
        />
      </View>
    );
  };

  return (
    <WrapperContainer bgColor={colors.white}>
      <View
        style={{
          paddingVertical: moderateVerticalScale(10),
          backgroundColor: colors.white,
        }}>
        <View style={{marginHorizontal: moderateScale(10)}}>
          <TextInputWithLabel
            placeholder={strings.SEARCH}
            customTextStyle={{
              height: moderateVerticalScale(50),
              borderColor: getColorCodeWithOpactiyNumber(
                colors.textGreyLight.substr(1),
                20,
              ),
              backgroundColor: getColorCodeWithOpactiyNumber(
                colors.textGreyLight.substr(1),
                20,
              ),
              paddingHorizontal: moderateScale(15),
              fontFamily: fontFamily.regular,
            }}
            rightIcon={imagePath.search}
            userFocus={focus => <></>}
            onChangeText={_onChangeText('searchedQuery')}
          />
        </View>
      </View>
      <View
        style={{
          marginTop: moderateVerticalScale(10),
          marginHorizontal: moderateScale(10),
        }}>
        <FlatList
          showsVerticalScrollIndicator={false}
          data={allSearchedProducts}
          ItemSeparatorComponent={() => (
            <View style={{height: moderateVerticalScale(2)}} />
          )}
          renderItem={searchResultsContainer}
          ListHeaderComponent={renderCategoryHeaderComponent}
          showsHorizontalScrollIndicator={false}
          onEndReached={onEndReachedDelayed}
          onEndReachedThreshold={0.5}
          ListFooterComponent={() => (
            <View style={{height: moderateVerticalScale(100)}} />
          )}
        />
      </View>
    </WrapperContainer>
  );
}
