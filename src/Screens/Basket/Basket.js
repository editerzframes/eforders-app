import React, {useEffect, useState} from 'react';
import {View, Text, Image, FlatList} from 'react-native';
import Header from '../../Components/Header';
import ProductListView from '../../Components/ProductListView';
import WrapperContainer from '../../Components/WrapperContainer';
import imagePath from '../../constants/imagePath';
import strings from '../../constants/lang';
import actions from '../../redux/actions';
import colors from '../../styles/colors';
import fontFamily from '../../styles/fontFamily';
import {
  height,
  moderateScale,
  moderateVerticalScale,
  textScale,
  width,
} from '../../styles/responsiveSize';
import {useFocusEffect} from '@react-navigation/native';
import {showMessage, hideMessage} from 'react-native-flash-message';
import {useSelector} from 'react-redux';
import TransparentButtonWithTxtAndIcon from '../../Components/ButtonComponent';
import navigationStrings from '../../constants/navigationStrings';

export default function Basket(props) {
  const {navigation} = props;
  const logedInUser = useSelector(state => state?.auth?.userData);
  const primaryLanguage = useSelector(state => state?.home?.primaryLanguage);
  const primaryAddress = useSelector(state => state?.home?.primaryAddress);
  const [state, setstate] = useState({
    list: [],
    payableAmount: null,
    cartBadge: null,
    totalDiscount: null,
  });

  const {
    list,
    payableAmount,
    cartBadge,
    isModalVisible,
    suggestion,
    totalDiscount,
  } = state;

  const updateState = data => setstate(state => ({...state, ...data}));
  const {ThemeColors, fontFamily} = useSelector(
    state => state?.init?.initAppData,
  );

  console.log(primaryAddress, 'primaryAddress');

  useFocusEffect(
    React.useCallback(() => {
      if (logedInUser?.access_token) {
        getCartData();
      }
    }, [primaryAddress]),
  );

  useEffect(() => {
    getCartData();
  }, [cartBadge]);

  const getCartData = () => {
    const data = {
      latitude: Number(primaryAddress?.latitude),
      longitude: Number(primaryAddress?.longitude),
    };

    actions
      .getCart(data, logedInUser?.access_token)
      .then(res => {
        console.log(res, 'response');
        updateState({
          list: res?.data?.cart_product,
          payableAmount: res?.data?.total_price,
          cartBadge: res?.data?.item_count,
        });
        actions.showCartBadge(res?.data?.item_count);
      })
      .catch(error => {
        showMessage({
          message: error.message,
          type: 'danger',
        });
      });
  };

  const cratItemIncrementDecrement = (type, id, price) => {
    const data = {
      type: type == 'minus' ? 'minus' : 'add',
      product_id: id,
    };

    actions
      .cratItemIncrementDecrement(logedInUser?.access_token, data)
      .then(res => {
        getCartData();
        updateState({
          list: res?.data?.cart_product,
          payableAmount: res?.data?.total_price,
        });
      })
      .catch(error => {
        showMessage({
          message: error.message,
          type: 'danger',
        });
      });
  };

  const onSingleItemRemoveFromCart = id => {
    console.log(id, 'id in reomove item from cart');
    const data = {
      product_id: id,
    };

    actions
      .singleItemRemoveFromCart(logedInUser?.access_token, data)
      .then(res => {
        getCartData();
      })
      .catch(error => {
        showMessage({
          message: error.message,
          type: 'danger',
        });
      });
  };

  const onAllRemoveFromCart = id => {
    actions
      .AllRemoveFromCart(logedInUser?.access_token)
      .then(res => {
        getCartData();
      })
      .catch(error => {
        showMessage({
          message: error.message,
          type: 'danger',
        });
      });
  };

  const _moveToNextScreen = (item, data) => {
    // console.log(, 'datatatata');
    navigation.navigate(item, data);
  };

  const checkProductaAvailability = available => {
    console.log(available?.is_available, 'availableavailable');
    return available?.is_available == 1;
  };

  const _onMoveTOCheckoutScreen = () => {
    const nowMoveToCheckout = list.every(checkProductaAvailability);

    if (nowMoveToCheckout) {
      _moveToNextScreen(navigationStrings.CHECKOUT);
    } else {
      showMessage({
        type: 'danger',
        message: 'Please remove product from cart which is not deliverable',
      });
    }
  };

  return (
    <WrapperContainer bgColor={colors.white}>
      <View style={{marginHorizontal: moderateScale(10)}}>
        <Header
          leftIcon={imagePath.back}
          textStyle={{color: colors.black}}
          centerTitle={strings.CART}
          rightIcon={imagePath.delete}
          rightIconStyle={{tintColor: colors.black}}
          onPressRight={() => onAllRemoveFromCart()}
        />
      </View>
      <View>
        <FlatList
          data={list}
          showsVerticalScrollIndicator={false}
          keyExtractor={(item, index) => String(index)}
          style={{
            marginTop: moderateVerticalScale(15),
            backgroundColor: colors.backgroundColor,
          }}
          renderItem={({item, index}) => {
            return (
              <>
                <ProductListView
                  data={item}
                  qtyIncriment={cratItemIncrementDecrement}
                  qtyDecriment={cratItemIncrementDecrement}
                  outFromCart={false}
                  onSingleItemRemoveFromCart={onSingleItemRemoveFromCart}
                />
                {item?.is_available ? null : (
                  <Text
                    style={{
                      color: colors.red,
                      marginHorizontal: moderateScale(10),
                      fontSize: textScale(8),
                      fontFamily: fontFamily.bold,
                    }}>
                    {'Product is not deliverable at this location'}
                  </Text>
                )}
              </>
            );
          }}
          ItemSeparatorComponent={() => {
            return (
              <View
                style={{
                  height: moderateVerticalScale(20),
                }}
              />
            );
          }}
          ListEmptyComponent={() => {
            return (
              <View
                style={{
                  height: height / 1.8,
                  width: width,
                  alignItems: 'center',
                }}>
                <Image
                  style={{
                    height: moderateVerticalScale(100),
                    width: moderateScale(100),
                  }}
                  source={imagePath.emptyCart}
                />
              </View>
            );
          }}
          ListFooterComponent={
            list?.length
              ? () => {
                  return (
                    <>
                      <View
                        style={{
                          marginTop: moderateVerticalScale(10),
                          width: width - 20,
                          borderWidth: moderateScale(0.5),
                          backgroundColor: ThemeColors?.primary_color,
                          marginHorizontal: moderateScale(10),
                          borderRadius: 5,
                          paddingBottom: moderateVerticalScale(20),
                        }}>
                        <View
                          style={{
                            flexDirection: 'row',
                            justifyContent: 'space-between',
                            paddingVertical: moderateVerticalScale(10),
                            paddingHorizontal: moderateScale(10),
                          }}>
                          <Text
                            style={{
                              fontSize: textScale(14),
                              fontFamily: fontFamily.regular,
                              color: colors.white,
                            }}>
                            {strings.TOTALITEMS}
                          </Text>
                          <Text
                            style={{
                              fontSize: textScale(14),
                              fontFamily: fontFamily.regular,
                              color: colors.white,
                            }}>
                            {list?.length}
                          </Text>
                        </View>
                        <View
                          style={{
                            flexDirection: 'row',
                            justifyContent: 'space-between',
                            paddingVertical: moderateVerticalScale(10),
                            paddingHorizontal: moderateScale(10),
                          }}>
                          {payableAmount ? (
                            <>
                              <Text
                                style={{
                                  fontSize: textScale(14),
                                  fontFamily: fontFamily.regular,
                                  color: colors.white,
                                }}>
                                {strings.TOTALPRICE}
                              </Text>
                              <Text
                                style={{
                                  fontSize: textScale(14),
                                  fontFamily: fontFamily.regular,
                                  color: colors.white,
                                }}>
                                {payableAmount}
                              </Text>
                            </>
                          ) : null}
                        </View>
                        {totalDiscount ? (
                          <View
                            style={{
                              flexDirection: 'row',
                              justifyContent: 'space-between',
                              paddingVertical: moderateVerticalScale(10),
                              paddingHorizontal: moderateScale(10),
                            }}>
                            <Text
                              style={{
                                fontSize: textScale(14),
                                fontFamily: fontFamily.regular,
                                color: colors.white,
                              }}>
                              {strings.TOTALDISCOUNT}
                            </Text>
                            <Text
                              style={{
                                fontSize: textScale(14),
                                fontFamily: fontFamily.regular,
                                color: colors.white,
                              }}>
                              {totalDiscount}
                            </Text>
                          </View>
                        ) : null}

                        <View
                          style={{
                            flexDirection: 'row',
                            justifyContent: 'space-between',
                            paddingVertical: moderateVerticalScale(20),
                            paddingHorizontal: moderateScale(10),
                          }}>
                          <Text
                            style={{
                              fontSize: textScale(16),
                              fontFamily: fontFamily.bold,
                              color: colors.white,
                            }}>
                            {strings.FINALPRICE}
                          </Text>
                          {payableAmount ? (
                            <Text
                              style={{
                                fontSize: textScale(16),
                                fontFamily: fontFamily.bold,
                                color: colors.white,
                              }}>
                              {payableAmount}
                            </Text>
                          ) : null}
                        </View>
                        <View
                          style={{
                            marginLeft: moderateScale(20),
                            marginRight: moderateScale(20),
                          }}>
                          <TransparentButtonWithTxtAndIcon
                            btnStyle={{
                              height: moderateVerticalScale(60),
                              borderRadius: 10,
                              backgroundColor: colors.white,
                            }}
                            textStyle={{
                              color: colors.black,
                              fontSize: textScale(16),
                              fontFamily: fontFamily.bold,
                            }}
                            btnText={strings.CHECKOUT}
                            onPress={() => _onMoveTOCheckoutScreen()}
                          />
                        </View>
                      </View>
                      <View style={{height: moderateVerticalScale(60)}} />
                    </>
                  );
                }
              : null
          }
        />
      </View>
    </WrapperContainer>
  );
}
