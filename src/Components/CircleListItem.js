import React from 'react';
import {Image, StyleSheet, Text, View} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';
import colors from '../styles/colors';
import fontFamily from '../styles/fontFamily';
import {moderateScale, textScale, width} from '../styles/responsiveSize';
import {getImageUrl} from '../utils/helperFunctions';

export const CircleListItem = ({data, onPress = () => {}}) => {
  return (
    <TouchableOpacity activeOpacity={0.8} onPress={() => onPress(data)}>
      <View style={styles.container}>
        <View style={styles.circularView}>
          <Image
            style={styles.circularListImage}
            source={{
              uri: getImageUrl(
                data?.icon?.image_fit,
                data?.icon?.image_path,
                '1000/1000',
              ),
            }}
          />
        </View>
        <Text style={styles.categoryText}>{data?.name}</Text>
      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  circularView: {
    height: moderateScale(70),
    width: moderateScale(70),
    borderWidth: 1,
    borderColor: colors.borderColorc,
    borderRadius: moderateScale(width / 8),
    alignItems: 'center',
    justifyContent: 'center',
    overflow: 'hidden',
  },
  categoryText: {
    color: colors.black,
    fontSize: textScale(12),
    fontFamily: fontFamily.bold,
  },
  circularListImage: {height: moderateScale(70), width: moderateScale(70)},
});
