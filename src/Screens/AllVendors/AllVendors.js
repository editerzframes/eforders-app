import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  ScrollView,
  TouchableOpacity,
  Image,
  FlatList,
} from 'react-native';
import {cloneDeep, debounce} from 'lodash';
import {showMessage} from 'react-native-flash-message';
import Header from '../../Components/Header';
import ModalView from '../../Components/Modal';
import WrapperContainer from '../../Components/WrapperContainer';
import imagePath from '../../constants/imagePath';
import strings from '../../constants/lang';
import actions from '../../redux/actions';
import colors from '../../styles/colors';
import fontFamily from '../../styles/fontFamily';
import {
  height,
  moderateScale,
  moderateVerticalScale,
  textScale,
  width,
} from '../../styles/responsiveSize';
import {stylesFunc} from './styles';
import navigationStrings from '../../constants/navigationStrings';
import TextInputWithLabel from '../../Components/TextInputWithLabel';
import {getColorCodeWithOpactiyNumber} from '../../utils/helperFunctions';
import ProductViewContainer from '../../Components/ProductViewContainer';
import LargeCartList from '../../Components/LargeCartList';
import HorizontalListCard from '../../Components/HorizontalListCard';
import {useSelector} from 'react-redux';

import {useFocusEffect} from '@react-navigation/native';
import VendorCard from '../../Components/VendorCard';

export default function AllVendors(props) {
  const {navigation, route} = props;

  const [state, setState] = useState({
    isModalVisible: false,
    pageNo: 1,
    limit: 12,
    allLoadedProducts: [],
    selectedFilterWithPrice: null,
    selectedFilterWithRange: null,
    filterModalType: null,
    gridView: true,
    searchedQuery: '',
    allLoadedVendors: [],
  });

  const {
    isModalVisible,
    pageNo,
    limit,
    allLoadedProducts,
    selectedFilterWithPrice,
    selectedFilterWithRange,
    gridView,
    searchedQuery,
    allLoadedVendors,
  } = state;
  const updateState = data => setState(state => ({...state, ...data}));

  const logedInUser = useSelector(state => state?.auth?.userData);
  const {ThemeColors, fontFamily} = useSelector(
    state => state?.init?.initAppData,
  );
  const styles = stylesFunc({fontFamily, ThemeColors});

  const setModalVisiblity = type => {
    updateState({
      filterModalType: type,
      isModalVisible: isModalVisible ? false : true,
    });
  };

  useEffect(() => {
    // do something
    allVendors();
    updateState({
      pageNo: 1,
    });
  }, []);

  const allVendors = () => {
    actions
      .vendorListing()
      .then(res => {
        updateState({
          allLoadedVendors:
            pageNo == 1 ? res?.data : [...allLoadedVendors, ...res?.data],
        });
      })
      .catch(error => {
        showMessage({
          message: error.message,
          type: 'danger',
        });
      });
  };

  //pagination of data
  const onEndReached = ({distanceFromEnd}) => {
    updateState({pageNo: pageNo + 1});
  };

  const onEndReachedDelayed = debounce(onEndReached, 1000, {
    leading: true,
    trailing: false,
  });

  const _moveToNextScreen = (redirectOn, data) => {
    navigation.navigate(navigationStrings.PRODUCT_LIST, {
      selectedVendor: data,
      isVendor: true,
    });
  };

  return (
    <WrapperContainer>
      <View style={styles.mainContainer}>
        <View style={styles.headerContainer}>
          <Header
            leftIcon={imagePath.back}
            textStyle={{color: colors.black}}
            centerTitle={'All Vendors'}
            // rightIcon={imagePath.filter}
            headerStyle={{marginHorizontal: moderateScale(10)}}
          />
        </View>
        {/* <View
          style={{
            marginHorizontal: moderateScale(10),
            marginTop: moderateVerticalScale(-10),
          }}>
          <TextInputWithLabel
            placeholder={''}
            customTextStyle={{
              height: moderateVerticalScale(50),
              borderColor: getColorCodeWithOpactiyNumber(
                colors.textGreyLight.substr(1),
                20,
              ),
              backgroundColor: getColorCodeWithOpactiyNumber(
                colors.textGreyLight.substr(1),
                20,
              ),
              paddingHorizontal: moderateScale(15),
              fontFamily: fontFamily.regular,
            }}
            editable={true}
            rightIcon={imagePath.search}
            userFocus={focus => <></>}
            // onChangeText={_onChangeText('searchedQuery')}
          />
        </View> */}

        <View
          style={[
            {
              alignItems: 'center',
            },
          ]}>
          <FlatList
            data={allLoadedVendors}
            showsVerticalScrollIndicator={false}
            renderItem={({item, index}) => {
              return (
                <VendorCard
                  data={item}
                  onPress={() =>
                    _moveToNextScreen(navigationStrings.PRODUCT_LIST, item)
                  }
                />
              );
            }}
            onEndReached={onEndReachedDelayed}
            onEndReachedThreshold={0.5}
            ItemSeparatorComponent={() => {
              return (
                <View
                  style={{
                    height: moderateVerticalScale(1),
                  }}
                />
              );
            }}
            ListFooterComponent={() => {
              return (
                <View
                  style={{
                    height: moderateVerticalScale(170),
                  }}
                />
              );
            }}
          />
        </View>
      </View>
    </WrapperContainer>
  );
}
