import React, {useState} from 'react';
import {View, Text, Image, TouchableOpacity, FlatList} from 'react-native';
import {useSelector} from 'react-redux';
import Header from '../../Components/Header';
import ModalView from '../../Components/Modal';
import TextInputWithLabel from '../../Components/TextInputWithLabel';
import WrapperContainer from '../../Components/WrapperContainer';
import imagePath from '../../constants/imagePath';
import strings from '../../constants/lang';
import navigationStrings from '../../constants/navigationStrings';
import actions from '../../redux/actions';
import colors from '../../styles/colors';
import fontFamily from '../../styles/fontFamily';
import {
  height,
  moderateScale,
  moderateVerticalScale,
  textScale,
  width,
} from '../../styles/responsiveSize';
import {getColorCodeWithOpactiyNumber} from '../../utils/helperFunctions';
import {stylesFunc} from './styles';
import {
  CodeField,
  Cursor,
  useBlurOnFulfill,
  useClearByFocusCell,
} from 'react-native-confirmation-code-field';
import {useFocusEffect} from '@react-navigation/native';
import {showMessage, hideMessage} from 'react-native-flash-message';

const CELL_COUNT = 4;
export default function Account(props) {
  const {navigation} = props;
  const logedInUser = useSelector(state => state?.auth?.userData);
  const [state, setstate] = useState({
    cardInfo: null,
    focusInput: null,
    verifyEmail: '',
    verifyMobile: '',
    userMobileOtp: '',
    userEmailOtp: '',
    accountOptions: [
      {
        id: 1,
        icon: imagePath.account,
        name: 'My Profile',
        key: 'profile',
      },
      // {
      //   id: 2,
      //   // icon: imagePath.notification,
      //   name: 'Notification',
      //   key: 'notification',
      // },
      {
        id: 2,
        icon: imagePath.bookmark,
        name: 'Wishlist',
        key: 'wishlist',
      },
      {
        id: 3,
        icon: imagePath.location,
        name: 'Addresses',
        key: 'addresses',
      },
      {
        id: 4,
        icon: imagePath.settings,
        name: 'Settings',
        key: 'settings',
      },
      // {
      //   id: 5,
      //   icon: imagePath.contactus,
      //   name: 'Support',
      //   key: 'support',
      // },
    ],
    isEmailVerification: null,
    isMobileVerification: null,
    verificationModalType: null,
    isModalVisible: false,
    isEmailOtp: false,
    isMobileOtp: false,
    profileIconBackgroundColor: '',
  });

  const {
    focusInput,
    userMobileOtp,
    userEmailOtp,
    accountOptions,
    isEmailVerification,
    isMobileVerification,
    isModalVisible,
    verificationModalType,
    verifyEmail,
    verifyMobile,
    isEmailOtp,
    isMobileOtp,
    profileIconBackgroundColor,
  } = state;

  const updateState = data => setstate(state => ({...state, ...data}));

  const {ThemeColors, fontFamily} = useSelector(
    state => state?.init?.initAppData,
  );

  const styles = stylesFunc({fontFamily, ThemeColors});

  const usernameFirstlater =
    !!logedInUser?.name && logedInUser?.name?.charAt(0);

  const generateColor = () => {
    const randomColor = Math.floor(Math.random() * 16777215)
      .toString(16)
      .padStart(6, '0');
    return `#${randomColor}`;
  };

  const goToNextScreen = (item, data) => {
    navigation.navigate(item, data);
  };

  const logout = () => {
    actions.onLogout();
  };

  const userFocus = firstname => {
    updateState({
      focusInput: firstname,
    });
  };

  const _onChangeText = key => val => {
    updateState({[key]: val});
  };

  const onEmailOtpSend = () => {
    let {callingCode} = state;

    let data = {
      type: verificationModalType,
      email: verifyEmail,
    };

    actions
      .requestOtp(data)
      .then(res => {
        updateState({
          isEmailOtp: true,
        });
        showMessage({
          message: res.message,
          type: 'success',
        });
      })
      .catch(error =>
        showMessage({
          message: error.message,
          type: 'danger',
        }),
      );
  };

  const onMobileNumberOtpSend = () => {
    let {callingCode} = state;

    let data = {
      type: verificationModalType,
      phone_code: '91',
      phone_no: verifyMobile,
    };

    actions
      .requestOtp(data)
      .then(res => {
        updateState({
          isMobileOtp: true,
        });
        showMessage({
          message: res.message,
          type: 'success',
        });
      })
      .catch(error =>
        showMessage({
          message: error.message,
          type: 'danger',
        }),
      );
  };

  const onVerifyEmail = () => {
    let data = {
      email: verifyEmail,
      otp_type: 'register',
      otp: userEmailOtp,
    };
    console.log(data, 'userEmailOtp');
    actions
      .verifyEmail(data)
      .then(res => {
        updateState({
          isModalVisible: false,
          isMobileOtp: '',
          isEmailOtp: '',
        });
        onIsUserVerified();
        showMessage({
          message: res.message,
          type: 'success',
        });
      })
      .catch(error =>
        showMessage({
          message: error.message,
          type: 'danger',
        }),
      );
  };

  const onVerifyMobile = () => {
    let {callingCode} = state;

    let data = {
      phone_code: '91',
      phone_no: verifyMobile,
      otp_type: 'register',
      otp: userMobileOtp,
    };

    actions
      .verifyMobile(data)
      .then(res => {
        updateState({
          isModalVisible: false,
          isMobileOtp: '',
          isEmailOtp: '',
        });
        onIsUserVerified();
        showMessage({
          message: res.message,
          type: 'success',
        });
      })
      .catch(error =>
        showMessage({
          message: error.message,
          type: 'danger',
        }),
      );
  };

  useFocusEffect(
    React.useCallback(() => {
      if (logedInUser?.access_token) {
        onIsUserVerified();
      }
      updateState({
        profileIconBackgroundColor: generateColor(),
      });
    }, []),
  );

  const onIsUserVerified = () => {
    actions
      .checkUserVerification(logedInUser?.access_token)
      .then(res => {
        console.log(res, 'user verification');
        updateState({
          isEmailVerification: res?.data?.verify_email,
          isMobileVerification: res?.data?.verify_phone,
        });
      })
      .catch(error =>
        showMessage({
          message: error.message,
          type: 'danger',
        }),
      );
  };

  const onVerification = type => {
    if (type == 'email') {
      updateState({
        verificationModalType: 'email',
        isModalVisible: true,
      });
    }
    if (type == 'mobile') {
      updateState({
        verificationModalType: 'mobile',
        isModalVisible: true,
      });
    }
  };

  const _moveToNextScreen = (data, redirectOn) => {
    switch (redirectOn) {
      case 'profile':
        navigation.navigate(navigationStrings.ACCOUNTSETTINGS, {
          selectedProduct: data?.id,
        });
        break;
      case 'addresses':
        navigation.navigate(navigationStrings.ADDRESSLIST);
        break;
      case 'wishlist':
        navigation.navigate(navigationStrings.FAVOURITES);
        break;
      case 'settings':
        navigation.navigate(navigationStrings.SETTINGS, {
          selectedProduct: data,
        });
        break;
    }
  };

  const modalMainContent = () => {
    return (
      <View
        style={{
          marginHorizontal: moderateVerticalScale(20),
        }}>
        <TouchableOpacity
          onPress={() =>
            updateState({
              isModalVisible: false,
            })
          }>
          <Image
            style={{alignSelf: 'flex-end', tintColor: colors.black}}
            source={imagePath?.close}
          />
        </TouchableOpacity>
        <View>
          {!(isEmailOtp || isMobileOtp) ? (
            <TextInputWithLabel
              placeholder={
                verificationModalType == 'email'
                  ? 'Your email'
                  : 'Your Mobile Number'
              }
              customTextStyle={{
                marginVertical: moderateVerticalScale(10),
                borderColor:
                  focusInput === 'verifyEmail'
                    ? ThemeColors?.primary_color
                    : getColorCodeWithOpactiyNumber(
                        colors.textGreyLight.substr(1),
                        60,
                      ),
                borderRadius: 8,
                height: moderateVerticalScale(55),
                paddingHorizontal: moderateScale(16),
                fontSize: textScale(15),
              }}
              onChangeText={_onChangeText(
                verificationModalType == 'email'
                  ? 'verifyEmail'
                  : 'verifyMobile',
              )}
              userFocus={focus => userFocus('verifyEmail')}
              rightIcon={imagePath.rightarrow}
              onPressRightIcon={
                verificationModalType == 'email'
                  ? () => onEmailOtpSend()
                  : () => onMobileNumberOtpSend()
              }
            />
          ) : null}

          {isEmailOtp || isMobileOtp ? (
            <>
              <CodeField
                value={
                  verificationModalType == 'email'
                    ? userEmailOtp
                    : userMobileOtp
                }
                onChangeText={
                  verificationModalType == 'email'
                    ? _onChangeText('userEmailOtp')
                    : _onChangeText('userMobileOtp')
                }
                cellCount={CELL_COUNT}
                rootStyle={styles.root}
                blurOnSubmit
                keyboardType="number-pad"
                textContentType="oneTimeCode"
                selectionColor={colors.themeColor}
                renderCell={({index, symbol, isFocused}) => (
                  <Text
                    key={index}
                    style={[styles.cell, isFocused && styles.focusCell]}>
                    {symbol || (isFocused ? <Cursor /> : null)}
                  </Text>
                )}
              />
              <TouchableOpacity
                style={{
                  paddingVertical: moderateVerticalScale(20),
                  backgroundColor: ThemeColors?.primary_color,
                  alignItems: 'center',
                  justifyContent: 'center',
                  borderRadius: moderateScale(40),
                  marginVertical: moderateVerticalScale(20),
                }}
                onPress={
                  verificationModalType == 'email'
                    ? onVerifyEmail
                    : onVerifyMobile
                }>
                <Text
                  style={{
                    color: colors.white,
                    fontFamily: fontFamily?.bold,
                    fontSize: textScale(15),
                  }}>
                  {verificationModalType == 'email'
                    ? 'Verify Email'
                    : 'Verify Mobile Number'}
                </Text>
                {/* <Image source={imagePath.back} /> */}
              </TouchableOpacity>
            </>
          ) : null}
        </View>
      </View>
    );
  };

  const userVerificationView = () => {
    return (
      <>
        {isEmailVerification ? (
          <View
            style={{
              flexDirection: 'row',
              paddingVertical: moderateVerticalScale(5),
              borderColor: colors.red,
              paddingHorizontal: moderateScale(20),
              justifyContent: 'space-between',
              alignItems: 'center',
              borderWidth: 0.5,
              borderRadius: 8,
              marginVertical: moderateVerticalScale(10),
              marginHorizontal: moderateScale(20),
            }}>
            <TouchableOpacity
              onPress={() => onVerification('email')}
              style={{
                flexDirection: 'row',
              }}>
              <Text
                style={{
                  fontSize: textScale(12),
                  marginHorizontal: moderateScale(20),
                  marginVertical: moderateVerticalScale(8),
                  color: colors.red,
                  fontFamily: fontFamily?.regular,
                  width: width / 2,
                }}>
                {'Verify Email'}
              </Text>
            </TouchableOpacity>
          </View>
        ) : null}
        {isMobileVerification ? (
          <View
            style={{
              flexDirection: 'row',
              paddingVertical: moderateVerticalScale(5),
              borderColor: colors.red,
              paddingHorizontal: moderateScale(20),
              justifyContent: 'space-between',
              alignItems: 'center',
              borderRadius: 8,
              borderWidth: 0.5,
              marginBottom: moderateVerticalScale(10),
              marginHorizontal: moderateScale(20),
            }}>
            <TouchableOpacity
              onPress={() => onVerification('mobile')}
              style={{
                flexDirection: 'row',
              }}>
              <Text
                style={{
                  fontSize: textScale(12),
                  marginHorizontal: moderateScale(20),
                  marginVertical: moderateVerticalScale(8),
                  color: colors.red,
                  fontFamily: fontFamily?.regular,
                  width: width / 2,
                }}>
                {'Verify Mobile Number'}
              </Text>
            </TouchableOpacity>
          </View>
        ) : null}
      </>
    );
  };

  return (
    <WrapperContainer
      bgColor={colors.lightgray}
      statusBarColor={colors.lightgray}>
      <View
        style={{
          paddingHorizontal: moderateScale(20),
          marginTop: moderateVerticalScale(5),
        }}>
        <Header
          leftIcon={imagePath.back}
          textStyle={{color: colors.black}}
          centerTitle={strings.ACCOUNT}
        />
      </View>
      <View
        style={{
          marginTop: moderateVerticalScale(50),
          alignItems: 'center',
          justifyContent: 'flex-end',
        }}>
        <View
          style={{
            width: width / 1.1,
            justifyContent: 'center',
            backgroundColor: colors.white,
            borderRadius: 10,
            overflow: 'hidden',
            shadowColor: colors.black,
            shadowOffset: {width: 0, height: 1},
            shadowOpacity: 0.8,
            shadowRadius: 2,
            elevation: 5,
            alignSelf: 'center',
            paddingVertical: moderateVerticalScale(20),
          }}>
          <View style={{flexDirection: 'row'}}>
            <View
              style={{
                flex: 0.3,
                alignItems: 'center',
              }}>
              <View
                style={{
                  borderRadius: 20,
                  backgroundColor: profileIconBackgroundColor,
                  paddingHorizontal: moderateScale(20),
                  paddingVertical: moderateScale(10),
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                <Text
                  style={{
                    fontSize: textScale(30),
                    color: colors.white,
                    fontFamily: fontFamily?.bold,
                    marginTop: moderateVerticalScale(-2),
                  }}>
                  {usernameFirstlater}
                </Text>
              </View>
            </View>
            <View
              style={{
                flex: 0.6,
              }}>
              <Text
                numberOfLines={1}
                style={{
                  fontSize: textScale(17),
                  fontFamily: fontFamily?.bold,
                  color: colors.black,
                }}>
                {logedInUser?.name}
              </Text>

              <Text
                style={{
                  fontSize: textScale(14),
                  fontFamily: fontFamily?.regular,
                  color: colors.black,
                }}>
                {logedInUser?.email}
              </Text>
            </View>
          </View>
        </View>
      </View>
      <View>
        <FlatList
          showsVerticalScrollIndicator={false}
          data={accountOptions}
          style={{marginTop: moderateVerticalScale(20)}}
          ListFooterComponent={() => {
            return (
              <View
                style={{
                  flexDirection: 'row',
                  paddingVertical: moderateVerticalScale(20),
                  backgroundColor: colors.white,
                  paddingHorizontal: moderateScale(20),
                  justifyContent: 'space-between',
                  borderBottomLeftRadius: 10,
                  borderBottomRightRadius: 10,
                  alignItems: 'center',
                  marginVertical: moderateVerticalScale(15),
                }}>
                {logedInUser?.access_token ? (
                  <View style={{flexDirection: 'row'}}>
                    <Image
                      // style={{tintColor: colors.inactiveColor}}
                      source={imagePath.logout}></Image>
                    <TouchableOpacity onPress={() => logout()}>
                      <Text
                        style={{
                          fontSize: textScale(16),
                          marginHorizontal: moderateScale(20),
                          color: colors.black,
                          fontFamily: fontFamily?.regular,
                        }}>
                        {strings.LOGOUT}
                      </Text>
                    </TouchableOpacity>
                  </View>
                ) : (
                  <View style={{flexDirection: 'row'}}>
                    <Image source={imagePath.profileIcon}></Image>
                    <TouchableOpacity onPress={() => goToNextScreen('login')}>
                      <Text
                        style={{
                          fontSize: textScale(16),
                          marginHorizontal: moderateScale(20),
                          color: colors.black,
                          fontFamily: fontFamily?.regular,
                        }}>
                        {strings.LOGIN}
                      </Text>
                    </TouchableOpacity>
                  </View>
                )}
                <Image source={imagePath.rightarrow} />
              </View>
            );
          }}
          ItemSeparatorComponent={() => {
            return <View style={{marginVertical: moderateVerticalScale(5)}} />;
          }}
          ListHeaderComponent={userVerificationView}
          keyExtractor={item => item.id}
          renderItem={({item, index}) => (
            <TouchableOpacity
              style={{
                flexDirection: 'row',
                paddingVertical: moderateVerticalScale(20),
                backgroundColor: colors.white,
                paddingHorizontal: moderateScale(20),
                justifyContent: 'space-between',
                borderBottomLeftRadius: 10,
                borderBottomRightRadius: 10,
                alignItems: 'center',
              }}
              onPress={() => _moveToNextScreen({}, item?.key)}>
              <View
                style={{
                  flexDirection: 'row',
                }}>
                <Image
                  style={{tintColor: colors.inactiveColor}}
                  source={item?.icon}></Image>
                <Text
                  style={{
                    fontSize: textScale(16),
                    marginHorizontal: moderateScale(20),
                    color: colors.black,
                    fontFamily: fontFamily?.regular,
                  }}>
                  {item?.name}
                </Text>
              </View>
              <Image source={imagePath.rightarrow} />
            </TouchableOpacity>
          )}
        />
      </View>

      <ModalView
        mainViewStyle={{width: width}}
        modalStyle={{
          position: 'absolute',
          bottom: 0,
          marginHorizontal: moderateScale(0),
          marginVertical: 0,
        }}
        isVisible={isModalVisible}
        modalMainContent={modalMainContent}
      />
    </WrapperContainer>
  );
}
