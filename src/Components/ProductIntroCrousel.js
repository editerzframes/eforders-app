// import React from 'react';
// import {
//   View,
//   Text,
//   StyleSheet,
//   TouchableOpacity,
//   ImageBackground,
//   Platform,
//   Image,
// } from 'react-native';
// import {FlatListSlider} from 'react-native-flatlist-slider';

// import {
//   height,
//   moderateScale,
//   moderateVerticalScale,
//   width,
// } from '../styles/responsiveSize';
// import colors from '../styles/colors';

// import imagePath from '../constants/imagePath';
// import {useNavigation} from '@react-navigation/native';
// import navigationStrings from '../constants/navigationStrings';
// import {useSelector} from 'react-redux';
// import {getImageUrl} from '../utils/helperFunctions';

// const Preview = ({item, imageKey, onPress, resizeMode = 'stretch'}) => {
//   const navigation = useNavigation();
//   const _MoveToVideoPlaryScreen = url => {
//     navigation.navigate(navigationStrings.PRODUCTVIDEOPLAYER, {videoUrl: url});
//   };

//   const {ThemeColors, fontFamily} = useSelector(
//     state => state?.init?.initAppData,
//   );
//   const styles = stylesFunc({fontFamily, ThemeColors});

//   return (
//     <TouchableOpacity activeOpacity={1} onPress={() => onPress(item)}>
//       <View style={[styles.imageContainer]}>
//         {item.video ? (
//           <View
//             style={{height: moderateVerticalScale(height / 2), width: width}}>
//             <Image
//               style={{
//                 height: moderateVerticalScale(height / 2),
//                 width: width,
//                 resizeMode: resizeMode,
//               }}
//               source={{uri: item.uri}}
//             />
//             <TouchableOpacity
//               style={{
//                 position: 'absolute',
//                 top: 0,
//                 left: 0,
//                 right: 0,
//                 bottom: 0,
//                 justifyContent: 'center',
//                 alignItems: 'center',
//               }}
//               onPress={() =>
//                 _MoveToVideoPlaryScreen('https://vjs.zencdn.net/v/oceans.mp4')
//               }>
//               <Image
//                 style={{tintColor: ThemeColors?.primary_color}}
//                 source={imagePath.play}
//               />
//             </TouchableOpacity>
//           </View>
//         ) : (
//           <Image
//             style={{
//               height: moderateVerticalScale(height / 2),
//               width: width,
//               resizeMode: resizeMode,
//             }}
//             source={{
//               uri: getImageUrl(
//                 item?.image?.image_fit,
//                 item?.image?.image_path,
//                 '1000/1000',
//               ),
//             }}
//           />
//         )}
//       </View>
//     </TouchableOpacity>
//   );
// };

// function ProductIntroCrousel({
//   bannerData = {},
//   cardViewStyle = {},
//   indicator = true,
//   autoscroll = true,
// }) {
//   const {ThemeColors, fontFamily} = useSelector(
//     state => state?.init?.initAppData,
//   );

//   const styles = stylesFunc({fontFamily, ThemeColors});
//   return (
//     <View
//       style={{
//         alignItems: 'center',
//         height: moderateVerticalScale(height / 2),
//         width: width,
//         alignContent: 'center',
//       }}>
//       {bannerData?.length && (
//         <FlatListSlider
//           data={bannerData}
//           onPress={item => {}}
//           component={<Preview />}
//           indicator={bannerData?.length > 1 ? indicator : false}
//           indicatorContainerStyle={styles.indecatorViewStyle}
//           indicatorActiveColor={ThemeColors?.primary_color}
//           indicatorInActiveColor={colors.textGreyLight}
//           indicatorActiveWidth={7}
//           animation
//           autoscroll={autoscroll}
//           imageKey={'image'}
//         />
//       )}
//     </View>
//   );
// }
// export function stylesFunc({fontFamily, ThemeColors}) {
//   const styles = StyleSheet.create({
//     imageStyle: {
//       position: 'absolute',
//       top: 0,
//       left: 0,
//       bottom: 0,
//       right: 0,
//     },
//     dotStyle: {
//       height: 12,
//       width: 12,
//       borderRadius: 12 / 2,
//     },
//     imageContainer: {
//       height: moderateVerticalScale(80),
//       width: width,
//       alignItems: 'center',
//     },

//     indecatorViewStyle: {
//       position: 'absolute',
//       bottom: -20,
//       elevation: 5,
//       shadowRadius: 12,
//     },
//   });
//   return styles;
// }

// export default ProductIntroCrousel;

import React, {useState} from 'react';
import {
  Image,
  ImageBackground,
  StyleSheet,
  TouchableOpacity,
  View,
} from 'react-native';

import {UIActivityIndicator} from 'react-native-indicators';
import Carousel, {Pagination} from 'react-native-snap-carousel';
import {useSelector} from 'react-redux';
import {
  height,
  moderateScale,
  moderateVerticalScale,
  width,
} from '../styles/responsiveSize';
import {getImageUrl} from '../utils/helperFunctions';

const ProductIntroCrousel = ({
  imagestyle = {},
  // slider1ActiveSlide = 0,
  bannerData = [],
  dotStyle = {},
  bannerRef,
  cardViewStyle = {},
  sliderWidth = width,
  itemWidth = width,
  onSnapToItem,
  pagination = true,
  resizeMode = 'cover',
  setActiveState = () => {},
  onPress = () => {},
  childView = null,
  showLightbox = false,
}) => {
  const {ThemeColors, fontFamily} = useSelector(
    state => state?.init?.initAppData,
  );

  const [state, setState] = useState({
    slider1ActiveSlide: 0,
    showLightboxView: false,
    imageLoader: true,
    // profileInfo: null
  });
  const updateState = data => setState(state => ({...state, ...data}));
  const {slider1ActiveSlide, showLightboxView} = state;
  const setSnapState = index => {
    updateState({slider1ActiveSlide: index});
    setActiveState(index);
  };
  const bannerDataImages = ({item, index}) => {
    const imageUrl = item?.image?.path
      ? getImageUrl(
          item?.image?.image_fit,
          item?.image?.image_path,
          '1000/1000',
        )
      : getImageUrl(item?.image?.image_fit, item.image.image_path, '1000/1000');

    return (
      <>
        <TouchableOpacity
          activeOpacity={1}
          style={[styles.imageStyle, imagestyle]}
          onPress={() => onPress(item)}>
          {/* <Lightbox
            underlayColor={'black'}
            renderContent={() => renderCarousel(imageUrl)}> */}

          <Image
            source={{uri: imageUrl}}
            //  onLoadStart={()=>}
            onLoadEnd={() => updateState({imageLoader: false})}
            style={{height: height / 2.5, width: width, ...imagestyle}}
            resizeMode={resizeMode}>
            {/* {!!state.imageLoader && (
              <UIActivityIndicator
                color={ThemeColors.primary_color}
                size={40}
              />
            )} */}
          </Image>

          {/* </Lightbox> */}
        </TouchableOpacity>
      </>
    );
  };
  return (
    <View style={[styles.cardViewStyle, cardViewStyle]}>
      <Carousel
        //ref={bannerRef}
        data={bannerData}
        renderItem={bannerDataImages}
        autoplay={false}
        loop={true}
        sliderWidth={sliderWidth}
        itemWidth={itemWidth}
        onSnapToItem={index => setSnapState(index)}
      />
      {bannerData?.length > 1 && (
        <View>
          <Pagination
            dotsLength={bannerData.length}
            activeDotIndex={slider1ActiveSlide}
            containerStyle={{paddingTop: 5}}
            dotColor={ThemeColors?.primary_color}
            dotStyle={[styles.dotStyle, dotStyle]}
            inactiveDotColor={'black'}
            inactiveDotOpacity={0.4}
            inactiveDotScale={0.8}
          />
        </View>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  imageStyle: {
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
    // height: 180,
    // width: width - 20,
  },
  dotStyle: {
    height: 6,
    width: 6,
    borderRadius: 6 / 2,
  },
  cardViewStyle: {
    alignItems: 'center',
    height: moderateVerticalScale(height / 2.5),
    width: width,

    // overflow: 'visible',

    // marginRight: 20
  },
});

export default ProductIntroCrousel;
