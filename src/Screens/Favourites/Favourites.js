import React, {useEffect, useState} from 'react';
import {View, Text, FlatList} from 'react-native';
import Header from '../../Components/Header';
import ProductListView from '../../Components/ProductListView';
import WrapperContainer from '../../Components/WrapperContainer';
import imagePath from '../../constants/imagePath';
import strings from '../../constants/lang';
import actions from '../../redux/actions';
import colors from '../../styles/colors';
import fontFamily from '../../styles/fontFamily';
import {
  height,
  moderateScale,
  moderateVerticalScale,
  textScale,
  width,
} from '../../styles/responsiveSize';
import {useSelector} from 'react-redux';
import {showMessage, hideMessage} from 'react-native-flash-message';
import {useFocusEffect} from '@react-navigation/native';

export default function Favourites(props) {
  const {navigation} = props;
  const logedInUser = useSelector(state => state?.auth?.userData);
  const [state, setstate] = useState({
    list: [],
    inWishlist: 1,
  });

  const {list, inWishlist} = state;

  const updateState = data => setstate(state => ({...state, ...data}));
  const {ThemeColors, fontFamily} = useSelector(
    state => state?.init?.initAppData,
  );

  const _moveToNextScreen = selectedProduct => {
    navigation.navigate(navigationStrings.PRODUCT_DETAILS, {selectedProduct});
  };

  useFocusEffect(
    React.useCallback(() => {
      if (logedInUser?.access_token) {
        getWishListData();
      }
    }, []),
  );

  useEffect(() => {
    getWishListData();
  }, [inWishlist]);

  const getWishListData = () => {
    actions
      .getWishlist({}, logedInUser?.access_token)
      .then(res => {
        updateState({
          list: res?.data?.map((item, index) => {
            return item;
          }),
        });
      })
      .catch(error => {
        showMessage({
          message: error.message,
          type: 'danger',
        });
      });
  };

  const addToCart = (productId, productPrice) => {
    const data = {
      product_id: productId,
      price: productPrice,
    };
    actions
      .addToCart(logedInUser?.access_token, data)
      .then(res => {
        showMessage({
          message: res.message,
          type: 'success',
        });
        getCartData();
        //actions.showCartBadge(1);
      })
      .catch(error => {
        showMessage({
          message: error.message,
          type: 'danger',
        });
      });
  };

  const getCartData = () => {
    actions
      .getCart({}, logedInUser?.access_token)
      .then(res => {
        actions.showCartBadge(res?.data?.item_count);
      })
      .catch(error => {
        showMessage({
          message: error.message,
          type: 'danger',
        });
      });
  };

  const removeWishlistItem = id => {
    const data = {
      product_id: id,
    };
    actions
      .addOrRemoveWishlist(logedInUser?.access_token, data)
      .then(res => {
        console.log(res, 'resss');
        updateState({
          inWishlist: inWishlist + 1,
        });
        showMessage({
          message: res.message,
          type: 'success',
        });
      })
      .catch(error => {
        showMessage({
          message: error.message,
          type: 'danger',
        });
      });
  };

  return (
    <WrapperContainer bgColor={colors.white}>
      <View style={{marginHorizontal: moderateScale(10)}}>
        <Header
          leftIcon={null}
          textStyle={{color: colors.black}}
          centerTitle={strings.WISHLIST}
          rightIcon={imagePath.search}
          rightIconStyle={{tintColor: ThemeColors?.primary_color}}
        />
      </View>
      <View>
        <FlatList
          data={list}
          showsVerticalScrollIndicator={false}
          keyExtractor={(item, index) => String(index)}
          style={{
            marginTop: moderateVerticalScale(15),
            backgroundColor: colors.backgroundColor,
          }}
          renderItem={({item, index}) => {
            return (
              <ProductListView
                data={item}
                outFromCart={true}
                addTocart={addToCart}
                removeWishlistItem={removeWishlistItem}
              />
            );
          }}
          ItemSeparatorComponent={() => {
            return (
              <View
                style={{
                  height: moderateVerticalScale(20),
                }}
              />
            );
          }}
          ListEmptyComponent={() => {
            return (
              <View
                style={{
                  height: height / 1.8,
                  width: width,
                  alignItems: 'center',
                }}></View>
            );
          }}
          ListFooterComponent={() => {
            return (
              <View
                style={{
                  height: moderateVerticalScale(50),
                }}
              />
            );
          }}
        />
      </View>
    </WrapperContainer>
  );
}
