import {StyleSheet} from 'react-native';
import colors from '../../styles/colors';

import {
  height,
  moderateScale,
  moderateVerticalScale,
  textScale,
} from '../../styles/responsiveSize';

export function stylesFunc({fontFamily, ThemeColors}) {
  const styles = StyleSheet.create({
    container: {
      height: height,
      backgroundColor: colors.black,
    },
    InnerMainBlackContainer: {
      height: height,
      backgroundColor: colors.black,
    },
    userInfoContainer: {
      backgroundColor: colors.black,
      justifyContent: 'center',
      marginHorizontal: moderateScale(20),
    },
    InnerMainWhiteContainer: {
      backgroundColor: colors.backgroundColor,
      borderTopLeftRadius: 50,
      borderTopRightRadius: 50,
      alignItems: 'center',
    },
    card: {
      backgroundColor: 'white',
      borderRadius: 8,
      paddingVertical: 45,
      paddingHorizontal: 25,
      width: '100%',
      marginVertical: 10,
    },
    shadowProp: {
      shadowColor: 'red',
      shadowOffset: {width: 2, height: 4},
      shadowOpacity: 0.2,
      shadowRadius: 3,
      elevation: 20,
    },
    screenTitle: {
      color: colors.white,
      fontFamily: fontFamily.bold,
      fontSize: textScale(20),
    },
    buttonInnerViewStyle: {
      position: 'absolute',
      left: 0,
      right: 0,
      top: 0,
      bottom: 0,
    },
    btnTextContainer: {flex: 1, justifyContent: 'center', alignItems: 'center'},

    btnTextStyle: {
      color: colors.white,
      textAlign: 'center',
      fontSize: textScale(22),
      fontFamily: fontFamily.regular,
      marginTop: moderateVerticalScale(-15),
    },
  });

  return styles;
}
