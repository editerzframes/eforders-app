import React from 'react';
import {StatusBar, View, ActivityIndicator} from 'react-native';
import colors from '../styles/colors';
import {SafeAreaProvider, SafeAreaView} from 'react-native-safe-area-context';
import LottieView from 'lottie-react-native';
import Modal from 'react-native-modal';
import {
  height,
  moderateScale,
  moderateVerticalScale,
  width,
} from '../styles/responsiveSize';
import {useSelector} from 'react-redux';
import {getColorCodeWithOpactiyNumber} from '../utils/helperFunctions';
import CustomAnimatedLoader from './CustomAnimatedLoader';
import {loaderOne} from './Loaders/AnimatedLoaderFiles';

const WrapperContainer = ({
  children,
  bgColor = colors.white,
  statusBarColor = colors.white,
  barStyle = 'dark-content',
  isLoading = false,
  source = loaderOne,
  loadercolor,
  loaderHeightWidth,
}) => {
  const {ThemeColors, fontFamily} = useSelector(
    state => state?.init?.initAppData,
  );

  const customColor = loadercolor ? loadercolor : ThemeColors?.primary_color;

  return (
    <>
      <SafeAreaView style={{flex: 1, backgroundColor: statusBarColor}}>
        <StatusBar backgroundColor={statusBarColor} barStyle={barStyle} />
        <View style={{backgroundColor: bgColor, flex: 1}}>{children}</View>
        <CustomAnimatedLoader
          source={source}
          loaderTitle={'Loading'}
          containerColor={colors.white}
          loadercolor={customColor}
          animationStyle={[
            {
              height: moderateVerticalScale(40),
              width: moderateScale(40),
            },
            {...loaderHeightWidth},
          ]}
          visible={isLoading}
        />
      </SafeAreaView>
    </>
  );
};

export default WrapperContainer;
