import {StyleSheet} from 'react-native';
import colors from '../../styles/colors';
import {
  height,
  moderateScale,
  moderateVerticalScale,
  textScale,
} from '../../styles/responsiveSize';

export function stylesFunc({fontFamily, ThemeColors}) {
  const styles = StyleSheet.create({
    container: {
      flex: 0.4,
      marginVertical: moderateVerticalScale(40),
      backgroundColor: colors.white,

      borderTopStartRadius: 2,
      borderTopEndRadius: 2,
    },
    container1: {
      flex: 0.7,
      justifyContent: 'center',
      borderTopStartRadius: 2,
      borderTopEndRadius: 2,
    },
    root: {marginTop: 20, alignItems: 'center'},
    cell: {
      width: textScale(48),
      height: textScale(48),
      fontFamily: fontFamily?.regular,
      lineHeight: textScale(47),
      fontSize: 24,
      borderWidth: 2,
      borderRadius: textScale(8),
      borderColor: colors.black,
      color: colors.black,
      textAlign: 'center',

      marginBottom: moderateVerticalScale(20),
    },
    focusCell: {
      borderColor: ThemeColors?.primary_color,
    },
  });

  return styles;
}
