import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  Image,
  SectionList,
  FlatList,
  Animated,
  TouchableOpacity,
  ProgressBarAndroid,
  PermissionsAndroid,
} from 'react-native';

import Crousel from '../../Components/Crousel';
import Header from '../../Components/Header';
import LargeCartList from '../../Components/LargeCartList';
import ProductListCard from '../../Components/ProductListCard';
import SmallCard from '../../Components/SmallCard';
import WrapperContainer from '../../Components/WrapperContainer';
import imagePath from '../../constants/imagePath';
import strings from '../../constants/lang';
import navigationStrings from '../../constants/navigationStrings';
import colors from '../../styles/colors';
import {
  height,
  moderateScale,
  moderateVerticalScale,
  textScale,
  width,
} from '../../styles/responsiveSize';
import {
  getColorCodeWithOpactiyNumber,
  getScaleTransformationStyle,
  pressInAnimation,
  pressOutAnimation,
} from '../../utils/helperFunctions';
import {stylesFunc} from './styles';
import {useFocusEffect} from '@react-navigation/native';
import actions from '../../redux/actions';
import ModalView from '../../Components/Modal';
import TextInputWithLabel from '../../Components/TextInputWithLabel';
import {useSelector} from 'react-redux';
import Geocoder from 'react-native-geocoding';
import {check, PERMISSIONS, RESULTS} from 'react-native-permissions';
import {cloneDeep, debounce} from 'lodash';
import {chekLocationPermission} from '../../utils/permissions';
navigator.geolocation = require('react-native-geolocation-service');
Geocoder.init('AIzaSyCwLPzcHGY93MND4Qm6ShNn2KOGZGoiSrM');

export default function Home(props) {
  const {navigation} = props;
  const primaryAddress = useSelector(state => state?.home?.primaryAddress);
  const [state, setState] = useState({
    homeData: [],
    isAddressModalVisible: false,
    selectPrimaryAddress: primaryAddress,
    allAddresses: [],
    userCurrentLocation: {},
    isLoading: false,
    latitude: null,
    longitude: null,
    pageNo: 1,
    limit: 12,
  });

  const {
    homeData,
    isAddressModalVisible,
    selectPrimaryAddress,
    allAddresses,
    city,
    stete,
    country,
    houseNumber,
    street,
    landmark,
    pinCode,
    userCurrentLocation,
    isLoading,
    latitude,
    longitude,
    pageNo,
    limit,
  } = state;

  const updateState = data => setState(state => ({...state, ...data}));
  const logedInUser = useSelector(state => state?.auth?.userData);

  const {ThemeColors, fontFamily} = useSelector(
    state => state?.init?.initAppData,
  );

  const styles = stylesFunc({fontFamily, ThemeColors});

  const _moveToNextScreen = (data, redirectOn) => {
    switch (redirectOn) {
      case 'productDetail':
        navigation.navigate(navigationStrings.PRODUCT_DETAILS, {
          selectedProduct: data?.id,
        });
        break;
      case 'search':
        navigation.navigate(navigationStrings.GLOBALSEARCH, {
          selectedProduct: data?.id,
        });

        break;
      case 'categoryDetail':
        navigation.navigate(navigationStrings.SUBCATEGORIES, {
          selectedProduct: data,
        });
        break;
      case 'categoryDetail1':
        navigation.navigate(navigationStrings.SUBCATEGORIES, {
          selectedProduct: data,
        });
        break;
      case 'addressList':
        navigation.navigate(navigationStrings.ADDRESSLIST, {
          selectedProduct: data,
        });
        break;
      default:
        navigation.navigate(navigationStrings.PRODUCT_LIST, {
          selectedVendor: data,
          isVendor: true,
        });
        break;
    }
  };

  const RootProducts = ({item, horizontal, redirectOn}) => {
    return (
      <ProductListCard
        data={item}
        horizontal={horizontal}
        onPress={data => _moveToNextScreen(data, redirectOn)}
      />
    );
  };

  const RootcategoryView = ({item, redirectOn}) => {
    return (
      <SmallCard
        data={item}
        onPress={data => _moveToNextScreen(data, redirectOn)}
      />
    );
  };

  const RootvendorView = ({item, redirectOn}) => {
    return (
      <LargeCartList
        data={item}
        onPress={data => _moveToNextScreen(data, redirectOn)}
      />
    );
  };

  // set all primary addresses on home page

  useFocusEffect(
    React.useCallback(() => {
      getAllAddresses();
    }, []),
  );

  useEffect(() => {
    getAllAddresses();
  }, []);

  const getAllAddresses = () => {
    actions
      .getAllAddresses()
      .then(res => {
        const priviousSeletedPrimaryAddress = res?.data[0]?.is_primary
          ? res?.data[0]
          : null;

        actions._setPrimaryAddressForApplicationGlobaly(
          priviousSeletedPrimaryAddress,
        );

        updateState({
          allAddresses: res?.data,
          selectPrimaryAddress: priviousSeletedPrimaryAddress,
        });
      })
      .catch(error => console.log(error));
  };

  const _onSelectPrimaryAddress = item => {
    let query = `/${item?.id}`;
    actions
      .setPrimaryAddress(query)
      .then(res =>
        updateState({
          selectPrimaryAddress: res?.data,
        }),
      )
      .catch(error => console.log(error, 'error'));
  };

  useEffect(() => {
    currentLocation();
    {
      logedInUser?.access_token && getCartData();
    }
  }, []);

  const currentLocation = () => {
    chekLocationPermission()
      .then(result => {
        if (result !== 'goback') {
          getCurrentLocation();
        }
      })
      .catch(error => console.log('error while accessing location ', error));
  };

  const getCurrentLocation = async () => {
    return await navigator.geolocation.default.getCurrentPosition(
      position => {
        Geocoder.from({
          latitude: position.coords.latitude,
          longitude: position.coords.longitude,
        })
          .then(json => {
            var addressComponent = json.results[0].formatted_address;
            var geometry = json.results[0].address_components;
            let details = {};
            details = {
              formatted_address: addressComponent,
              geometry: {
                location: {
                  lat: position.coords.latitude,
                  lng: position.coords.longitude,
                },
              },
              address_components: json.results[0].address_components,
            };

            updateState({
              userCurrentLocation: {
                latitude: position.coords.latitude,
                longitude: position.coords.longitude,
                currentFormatedAddress: details?.formatted_address,
                currentGeometry: details?.geometry?.location,
              },
            });
          })
          .catch(error => alert(JSON.stringify(error), 'error'));
      },
      error => console.log(error.message),
      {enableHighAccuracy: true, timeout: 20000},
    );
  };

  // render home page data

  useEffect(() => {
    if (
      (selectPrimaryAddress?.longitude && selectPrimaryAddress?.latitude) ||
      (userCurrentLocation?.latitude && userCurrentLocation?.longitude)
    ) {
      getHomeData();
    }
  }, [
    userCurrentLocation?.longitude,
    userCurrentLocation?.latitude,
    selectPrimaryAddress?.longitude,
    selectPrimaryAddress?.latitude,
  ]);

  useEffect(() => {
    if (
      userCurrentLocation?.latitude != null &&
      userCurrentLocation?.longitude != null
    ) {
      actions._setPrimaryAddressForApplicationGlobaly(userCurrentLocation);
    }
  }, [userCurrentLocation?.longitude, userCurrentLocation?.latitude]);

  const getHomeData = () => {
    console.log(limit, pageNo, 'pageNopageNopageNo');
    let query = `/${''}?limit=${limit}&page=${pageNo}`;

    const headerData = {
      latitude:
        selectPrimaryAddress?.latitude != undefined
          ? Number(selectPrimaryAddress?.latitude)
          : userCurrentLocation?.latitude,
      longitude:
        selectPrimaryAddress?.longitude != undefined
          ? Number(selectPrimaryAddress?.longitude)
          : userCurrentLocation?.longitude,
    };
    actions
      .homePage(query, headerData)
      .then(res => {
        updateState({
          homeData: res?.data,
          // pageNo == 1 ? res?.data : [...homeData, ...res?.data],
          //  pageNo == 1 ? res?.data : [...homeData, ...res?.data],
          isLoading: false,
        });
      })
      .catch(
        error =>
          showMessage({
            message: error,
            type: 'danger',
          }),
        updateState({
          isLoading: false,
        }),
      );
  };

  // get cart data for update badge on bottom bar

  const getCartData = () => {
    actions
      .getCart({}, logedInUser?.access_token)
      .then(res => {
        console.log(res, 'response');

        actions.showCartBadge(res?.data?.item_count);
      })
      .catch(error => {
        showMessage({
          message: error.message,
          type: 'danger',
        });
      });
  };

  const onViewPressed = type => {
    switch (type) {
      case 'rootproducts':
        navigation.navigate(navigationStrings.PRODUCT_LIST, {
          selectedVendor: {},
          isVendor: false,
        });
        break;
      case 'rootvendors':
        navigation.navigate(navigationStrings.ALLVENDORS);
        break;
      default:
        break;
    }
  };

  //pagination of data
  const onEndReached = ({distanceFromEnd}) => {
    updateState({pageNo: pageNo + 1});
  };

  const onEndReachedDelayed = debounce(onEndReached, 1000, {
    leading: true,
    trailing: false,
  });

  const modalMainContent = () => {
    return (
      <View style={{height: height / 3}}>
        <TouchableOpacity
          onPress={() =>
            updateState({
              isAddressModalVisible: false,
            })
          }>
          <Image style={styles.closeModalImage} source={imagePath.close} />
        </TouchableOpacity>
        <View style={{flex: 1}}>
          <View
            style={{
              paddingHorizontal: moderateScale(10),
            }}>
            <Text style={styles.chooseAddressText}>
              {strings.CHOOSEYOURLOCATION}
            </Text>
            <Text style={styles.selectDeliveryLocationText}>
              {strings.SELECTDELIVERYLOCATION}
            </Text>
          </View>
          <View
            style={{
              paddingHorizontal: moderateScale(10),
              marginTop: moderateVerticalScale(20),
            }}>
            <FlatList
              horizontal
              data={allAddresses?.length ? allAddresses : []}
              ItemSeparatorComponent={() => {
                return <View style={{width: moderateVerticalScale(10)}} />;
              }}
              renderItem={({item, index}) => {
                return (
                  <TouchableOpacity
                    activeOpacity={0.5}
                    style={{
                      height: moderateVerticalScale(150),
                      width: moderateScale(150),
                      borderColor:
                        selectPrimaryAddress?.id === item?.id
                          ? ThemeColors?.primary_color
                          : colors.black,
                      borderWidth:
                        selectPrimaryAddress?.id === item?.id ? 1 : 0.5,
                      paddingVertical: moderateVerticalScale(5),
                      paddingHorizontal: moderateScale(5),
                      backgroundColor:
                        selectPrimaryAddress?.id === item?.id
                          ? getColorCodeWithOpactiyNumber(
                              ThemeColors?.primary_color.substr(1),
                              10,
                            )
                          : colors.white,
                    }}
                    onPress={() => _onSelectPrimaryAddress(item)}>
                    <Text
                      numberOfLines={1}
                      style={{
                        fontSize: textScale(16),
                        color: colors.black,
                        fontFamily: fontFamily?.bold,
                      }}>
                      {item?.name}
                    </Text>
                    <Text
                      style={{
                        fontSize: textScale(14),
                        color: colors.textGreyB,
                        fontFamily: fontFamily?.regular,
                        marginRight: moderateScale(10),
                      }}>
                      {item?.house_no && item?.house_no} {'\n'}
                      {item?.landmark && item?.landmark} {'\n'}
                      {item?.state && item?.state}
                      {` ${item?.pincode && item?.pincode}`} {'\n'}
                      {item?.country && item?.country}
                    </Text>
                    {selectPrimaryAddress?.id === item?.id ? (
                      <Text
                        style={{
                          fontSize: textScale(13),
                          color: colors.textGreyB,
                          fontFamily: fontFamily?.regular,
                          marginRight: moderateScale(10),
                          marginTop: moderateVerticalScale(10),
                        }}>
                        {strings.DEFAULTADDRESS}
                      </Text>
                    ) : null}
                  </TouchableOpacity>
                );
              }}
              showsHorizontalScrollIndicator={false}
              ListFooterComponent={() => {
                return (
                  <TouchableOpacity
                    style={styles.addnewAddressView}
                    onPress={() => {
                      _moveToNextScreen({}, 'addressList'),
                        updateState({isAddressModalVisible: false});
                    }}>
                    <Text style={styles.addnewaddresstext}>
                      {strings.ADDANADDRESS}
                    </Text>
                  </TouchableOpacity>
                );
              }}
            />
          </View>
        </View>
      </View>
    );
  };

  return (
    <WrapperContainer bgColor={colors.white} isLoading={isLoading}>
      <View style={styles.mainContainer}>
        <View style={styles.headerContainer}>
          <View
            style={{
              width: width - 20,
              height: moderateVerticalScale(30),
              alignItems: 'center',
              flexDirection: 'row',
              justifyContent: 'space-between',
            }}>
            <TouchableOpacity onPress={() => navigation.openDrawer()}>
              <Image
                style={{tintColor: ThemeColors?.primary_color}}
                source={imagePath.menu}
              />
            </TouchableOpacity>
            <View
              style={{
                flexDirection: 'row',
              }}>
              <TouchableOpacity
                style={styles.loactionInnerContainer}
                onPress={() =>
                  updateState({
                    isAddressModalVisible: true,
                  })
                }>
                <Image
                  style={{tintColor: colors.inactiveColor}}
                  source={imagePath.location}
                />
                {selectPrimaryAddress ? (
                  <Text numberOfLines={1} style={styles.locationTextStyle}>
                    {selectPrimaryAddress?.house_no &&
                      selectPrimaryAddress?.house_no}{' '}
                    {selectPrimaryAddress?.landmark &&
                      selectPrimaryAddress?.landmark}{' '}
                    {selectPrimaryAddress?.state && selectPrimaryAddress?.state}
                    {` ${
                      selectPrimaryAddress?.pincode &&
                      selectPrimaryAddress?.pincode
                    }`}
                  </Text>
                ) : (
                  <Text numberOfLines={1} style={styles.locationTextStyle}>
                    {userCurrentLocation?.currentFormatedAddress}
                  </Text>
                )}
              </TouchableOpacity>
            </View>

            <View style={{flex: 0.2}}></View>
          </View>
        </View>

        <TouchableOpacity
          style={{
            marginTop: moderateVerticalScale(5),
            width: width - 40,
            alignSelf: 'center',
          }}
          onPress={() =>
            _moveToNextScreen(navigationStrings.GLOBALSEARCH, 'search')
          }>
          <TextInputWithLabel
            placeholder={strings.SEARCH}
            customTextStyle={styles.searchBoxStyle}
            editable={false}
            rightIcon={imagePath.search}
            onPressRightIcon={() =>
              _moveToNextScreen(navigationStrings.GLOBALSEARCH, 'search')
            }
          />
        </TouchableOpacity>
        {/* </View> */}
        <View style={styles.mainHomeScreenDataContainer}>
          <SectionList
            showsVerticalScrollIndicator={false}
            stickySectionHeadersEnabled={false}
            sections={homeData}
            renderSectionHeader={({section}) => (
              <>
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    marginTop: moderateVerticalScale(10),
                  }}>
                  {section?.horizontal ? (
                    section?.type === 'banner' ||
                    section?.type === 'rootcategory' ||
                    !section?.data?.length ? null : (
                      <Text style={styles.sectionHeader}>{section?.title}</Text>
                    )
                  ) : null}
                  {section?.horizontal ? (
                    section?.type === 'banner' ||
                    section?.type === 'rootcategory' ||
                    !section?.data?.length ? null : (
                      <TouchableOpacity
                        onPress={() => onViewPressed(section?.type)}>
                        <Text style={styles.viewAllTextStyle}>
                          {strings.VIEWALL}
                        </Text>
                      </TouchableOpacity>
                    )
                  ) : null}
                </View>
                {section?.type === 'banner' && section?.horizontal ? (
                  <Crousel indicator bannerData={section?.data} />
                ) : null}

                {section?.horizontal ? (
                  (section?.horizontal && section?.type === 'rootcategory') ||
                  section?.type === 'rootproducts' ||
                  section?.type === 'rootvendors' ? (
                    <FlatList
                      horizontal
                      data={section?.data}
                      renderItem={
                        section?.type === 'rootvendors'
                          ? ({item}) => <RootvendorView item={item} />
                          : section?.type === 'rootproducts'
                          ? ({item}) => (
                              <RootProducts
                                item={item}
                                horizontal={section?.horizontal}
                                redirectOn={section?.redirect_url}
                              />
                            )
                          : ({item}) => (
                              <RootcategoryView
                                item={item}
                                redirectOn={section?.redirect_url}
                              />
                            )
                      }
                      showsHorizontalScrollIndicator={false}
                    />
                  ) : null
                ) : null}
              </>
            )}
            renderItem={({item, section}) => {
              if (section?.horizontal) {
                return null;
              }
              return <RootProducts item={item} />;
            }}
            onEndReached={onEndReachedDelayed}
            onEndReachedThreshold={0.5}
            ListFooterComponent={() => (
              <View style={{height: moderateVerticalScale(50)}} />
            )}
          />
        </View>
      </View>
      <ModalView
        mainViewStyle={{width: width}}
        modalStyle={{
          position: 'absolute',
          bottom: 0,
          marginHorizontal: moderateScale(0),
          marginVertical: 0,
        }}
        isVisible={isAddressModalVisible}
        modalMainContent={modalMainContent}
      />
    </WrapperContainer>
  );
}
