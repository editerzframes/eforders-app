import React from 'react';
import {View, Text, Image, TouchableOpacity} from 'react-native';
import {useSelector} from 'react-redux';
import imagePath from '../constants/imagePath';
import colors from '../styles/colors';
import fontFamily from '../styles/fontFamily';
import {
  moderateScale,
  moderateVerticalScale,
  textScale,
  width,
} from '../styles/responsiveSize';
import {getImageUrl} from '../utils/helperFunctions';

export default function ProductListView({
  data,
  qtyIncriment,
  qtyDecriment,
  outFromCart,
  addTocart,
  removeWishlistItem,
  onSingleItemRemoveFromCart,
}) {
  const {ThemeColors, fontFamily} = useSelector(
    state => state?.init?.initAppData,
  );

  console.log(data, 'datadatadatadata+++++++');
  const imageUrl = data?.product?.image
    ? getImageUrl(
        data?.product?.icon?.image_fit,
        data.product.icon?.image_path,
        '1000/1000',
      )
    : getImageUrl(
        data?.product?.icon?.image_fit,
        data?.product?.icon?.image_path,
        '1000/1000',
      );

  return (
    <View
      style={{
        width: width,
        height: moderateVerticalScale(120),
        paddingHorizontal: moderateScale(10),
      }}>
      <View
        style={{
          flex: 1,
          flexDirection: 'row',
          justifyContent: 'space-between',
          alignItems: 'center',
        }}>
        <View
          style={{
            flexDirection: 'row',
          }}>
          <Image
            resizeMode={'cover'}
            style={{
              width: moderateScale(width / 3),
              height: moderateVerticalScale(120),
              borderRadius: 20,
            }}
            source={{uri: imageUrl}}
          />
          <View
            style={{
              paddingLeft: moderateScale(20),
            }}>
            <Text
              style={{
                fontSize: textScale(14),
                fontFamily: fontFamily.regular,
                color: colors.themeColor,
              }}>
              {data?.product?.price}
            </Text>
            <Text
              numberOfLines={2}
              style={{
                fontSize: textScale(14),
                fontFamily: fontFamily.bold,
                width: width / 2.3,
                color: colors.black,
              }}>
              {data?.product.name}
            </Text>

            {!outFromCart ? (
              <>
                <View
                  style={{
                    width: moderateScale(120),
                    height: moderateVerticalScale(38),
                    backgroundColor: colors.lightgray,
                    borderRadius: 2,
                    justifyContent: 'space-between',
                    alignItems: 'center',
                    flexDirection: 'row',
                    marginVertical: moderateVerticalScale(10),
                  }}>
                  <TouchableOpacity
                    onPress={() =>
                      qtyIncriment(
                        'add',
                        data?.product?.id,
                        data?.product?.price,
                      )
                    }
                    style={{
                      backgroundColor: colors.white,
                      height: moderateVerticalScale(36),
                      width: moderateScale(40),
                      marginLeft: moderateScale(1),
                      alignItems: 'center',
                      justifyContent: 'center',
                      borderRadius: 2,
                    }}>
                    <Text
                      style={{
                        fontSize: textScale(20),
                        color: colors.textGreyLight,
                      }}>
                      +
                    </Text>
                  </TouchableOpacity>
                  <Text
                    style={{
                      marginVertical: moderateVerticalScale(6),
                      color: colors.black,
                    }}>
                    {data?.quantity}
                  </Text>
                  <TouchableOpacity
                    onPress={() =>
                      qtyDecriment(
                        'minus',
                        data?.product?.id,
                        data?.product?.price,
                      )
                    }
                    style={{
                      backgroundColor: colors.white,
                      height: moderateVerticalScale(36),
                      width: moderateScale(40),
                      marginRight: moderateScale(1),
                      alignItems: 'center',
                      justifyContent: 'center',
                      borderRadius: 2,
                    }}>
                    <Text
                      style={{
                        fontSize: textScale(20),
                        color: colors.textGreyLight,
                      }}>
                      -
                    </Text>
                  </TouchableOpacity>
                </View>
              </>
            ) : (
              <TouchableOpacity
                onPress={() =>
                  addTocart(data?.product?.id, data?.product?.price)
                }
                style={{
                  height: moderateVerticalScale(35),
                  backgroundColor: ThemeColors?.primary_color,
                  borderRadius: 2,
                  flexDirection: 'row',
                  justifyContent: 'space-evenly',
                  alignItems: 'center',
                  marginVertical: moderateVerticalScale(10),

                  marginRight: moderateScale(25),
                }}>
                <View
                  style={{
                    backgroundColor: colors.white,
                    paddingHorizontal: moderateScale(5),
                    paddingVertical: moderateVerticalScale(5),
                    borderRadius: 20,
                  }}>
                  <Image
                    style={{tintColor: ThemeColors?.primary_color}}
                    source={imagePath.cart}
                  />
                </View>
                <Text
                  style={{
                    color: colors.white,
                    fontFamily: fontFamily.bold,
                    fontSize: textScale(12),
                  }}>
                  ADD TO CART
                </Text>
              </TouchableOpacity>
            )}
          </View>
        </View>

        <TouchableOpacity
          style={{
            marginRight: moderateScale(10),
          }}
          onPress={
            outFromCart
              ? () => removeWishlistItem(data?.product?.id)
              : () => onSingleItemRemoveFromCart(data?.product?.id)
          }>
          <Image
            style={{tintColor: ThemeColors?.primary_color}}
            source={imagePath.delete}
          />
        </TouchableOpacity>
      </View>
    </View>
  );
}
