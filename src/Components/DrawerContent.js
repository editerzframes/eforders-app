import React, {useEffect, useState} from 'react';
import {
  View,
  StyleSheet,
  Image,
  Button,
  FlatList,
  TouchableOpacity,
  Share,
} from 'react-native';
import {Title, Text} from 'react-native-paper';

import imagePath from '../constants/imagePath';
import colors from '../styles/colors';
import {
  moderateScale,
  moderateVerticalScale,
  textScale,
  width,
} from '../styles/responsiveSize';

import strings from '../constants/lang';

import commonStyles from '../styles/commonStyles';
import {useSelector} from 'react-redux';
import navigationStrings from '../constants/navigationStrings';
import ZendeskChat from 'react-native-zendesk-chat';
import DeviceInfo from 'react-native-device-info';

export default function DrawerContent(props) {
  const [state, setstate] = useState({
    drawerItems: [
      // {
      //   id: 1,
      //   icon: imagePath.orders,
      //   name: 'My Orders',
      //   key: 'my_orders',
      // },
      // {
      //   id: 2,
      //   // icon: imagePath.notification,
      //   name: 'Notification',
      //   key: 'notification',
      // },
      {
        id: 3,
        icon: imagePath.star,
        name: 'Rate Us',
        key: 'rate_us',
      },
      {
        id: 4,
        icon: imagePath.share,
        name: 'Share',
        key: 'share',
      },
      {
        id: 5,
        icon: imagePath.contactus,
        name: 'Support',
        key: 'support',
      },
    ],
  });
  const {drawerItems} = state;
  const userData = useSelector(reduxState => reduxState?.auth?.userData);
  const {ThemeColors, fontFamily} = useSelector(
    state => state?.init?.initAppData,
  );
  const {navigation} = props;

  const usernameFirstlater = !!userData?.name && userData?.name?.charAt(0);

  const generateColor = () => {
    const randomColor = Math.floor(Math.random() * 16777215)
      .toString(16)
      .padStart(6, '0');
    return `#${randomColor}`;
  };

  const goToNextScreen = type => {
    if (type == 'login') {
      navigation.navigate(navigationStrings.LOGIN);
    }
    if (type == 'signUp') {
      navigation.navigate(navigationStrings.SIGNUP);
    }
    if (type == 'account') {
      navigation.navigate(navigationStrings.ACCOUNT);
    }
  };

  const styles = stylesFunc({fontFamily, ThemeColors});

  const onShare = async () => {
    try {
      const result = await Share.share({
        title: 'Please Join EFOrders',
        message:
          'React Native | A framework for building native apps using React',
        url: 'https://m.media-amazon.com/images/M/MV5BMTJhZGRhZjMtMWNhNS00NGE5LWEzNTEtZmQzODA0ZDcyZTRhXkEyXkFqcGdeQXVyNDAzNDk0MTQ@._V1_.jpg',
      });
      if (result.action === Share.sharedAction) {
        if (result.activityType) {
          // shared with activity type of result.activityType
        } else {
          // shared
        }
      } else if (result.action === Share.dismissedAction) {
        // dismissed
      }
    } catch (error) {
      alert(error.message);
    }
  };

  // Zendesk Initialization

  useEffect(() => {
    ZendeskChat.init(
      'hkj6wV0p0qW45bXDMtdTSCEenFuTZhFR',
      '882ad89551868abec6d361472fee131462c1ea5ebebbb63f',
    );
  }, []);

  //zendesk chat funcation

  const onStartChat = () => {
    ZendeskChat.startChat({
      name: userData?.name,
      email: userData?.email,
      phone: userData?.mobile_number,
      // The behaviorFlags are optional, and each default to 'true' if omitted
      behaviorFlags: {
        showAgentAvailability: true,
        showChatTranscriptPrompt: true,
        showPreChatForm: true,
        showOfflineForm: true,
      },
      // The preChatFormOptions are optional & each defaults to "optional" if omitted
      preChatFormOptions: {
        name: userData?.name ? 'required' : 'optional',
        email: 'optional',
        phone: 'optional',
        department: 'required',
      },
      localizedDismissButtonTitle: 'Enable',
    });
  };

  const onMoveAction = item => {
    switch (item?.key) {
      case 'my_orders':
        break;
      case 'rate_us':
        break;
      case 'share':
        onShare();
        break;
      case 'support':
        onStartChat();
        break;
      default:
        break;
    }
  };

  return (
    <View style={styles.container}>
      {userData?.access_token ? (
        <View style={styles.drawerContent}>
          <View style={styles.userInfoSection}>
            <View style={styles.userInfoContainer1}>
              <View
                style={[
                  styles.userProfileImage1,
                  {
                    backgroundColor: generateColor(),
                    alignItems: 'center',
                    justifyContent: 'center',
                  },
                ]}>
                <Text
                  style={{
                    fontSize: textScale(30),
                    color: colors.white,
                    fontFamily: fontFamily?.bold,
                  }}>
                  {usernameFirstlater}
                </Text>
              </View>
              <Title style={styles.userName}>{userData?.name}</Title>
            </View>
            <TouchableOpacity
              style={{
                alignItems: 'center',
              }}
              onPress={() => goToNextScreen('account')}>
              <Text
                style={{
                  fontFamily: fontFamily?.bold,
                  color: ThemeColors?.primary_color,
                  borderBottomWidth: 0.5,
                  borderColor: ThemeColors?.primary_color,
                }}>
                {strings.VIEW_PROFILE}
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      ) : (
        <View style={styles.drawerContent}>
          <View style={styles.userInfoSection}>
            <View>
              <View style={styles.userInfoContainer}>
                <Image
                  style={styles.userProfileImage}
                  source={{uri: 'https://shorturl.at/fswE0'}}
                />
                <Title style={styles.userName}>Hello Guest</Title>
              </View>
              <View style={styles.lineView} />
              <View style={styles.loginRegisterContainer}>
                <TouchableOpacity onPress={() => goToNextScreen('login')}>
                  <View>
                    <Text
                      style={{
                        color: colors.white,
                        fontFamily: fontFamily?.regular,
                      }}>
                      {strings.LOGIN}
                    </Text>
                  </View>
                </TouchableOpacity>
                <View style={styles.verticaleLineView} />
                <TouchableOpacity onPress={() => goToNextScreen('signUp')}>
                  <View>
                    <Text
                      style={{
                        color: colors.white,
                        fontFamily: fontFamily?.regular,
                      }}>
                      {strings.SIGN_UP}
                    </Text>
                  </View>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </View>
      )}
      <View style={styles.innerContainer}>
        <View
          style={{
            marginVertical: moderateVerticalScale(6),
          }}>
          <FlatList
            showsVerticalScrollIndicator={false}
            data={drawerItems}
            ItemSeparatorComponent={() => {
              return (
                <View style={{marginVertical: moderateVerticalScale(15)}} />
              );
            }}
            keyExtractor={item => item.id}
            renderItem={({item, index}) => (
              <TouchableOpacity
                style={{
                  flexDirection: 'row',
                  marginHorizontal: moderateScale(10),
                }}
                onPress={() => onMoveAction(item)}>
                <Image
                  style={styles.drawerItmeImage}
                  source={item.icon}></Image>
                <Text style={styles.drawerItemName}>{item.name}</Text>
              </TouchableOpacity>
            )}
          />
        </View>
        <View style={styles.offerBy}>
          <Text>Powered By</Text>
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <Text
              style={{
                marginVertical: moderateVerticalScale(10),
                fontSize: textScale(18),
                fontFamily: fontFamily?.bold,
              }}>
              EFORDERS
            </Text>
            <Text
              style={{
                fontFamily: fontFamily?.regular,
              }}>{` (v ${DeviceInfo.getVersion()})`}</Text>
          </View>
        </View>
      </View>
    </View>
  );
}
export function stylesFunc({fontFamily, ThemeColors}) {
  const styles = StyleSheet.create({
    container: {
      flex: 1,
    },
    drawerContent: {
      flex: 0.25,
      backgroundColor: colors.themeColorOpecity,
    },
    userInfoContainer: {
      flexDirection: 'row',
      alignItems: 'center',
      marginVertical: moderateVerticalScale(10),
    },
    userInfoContainer1: {
      alignItems: 'center',
      marginVertical: moderateVerticalScale(10),
    },
    userName: {
      color: colors.black,
      marginHorizontal: moderateScale(10),
      fontFamily: fontFamily?.regular,
    },
    lineView: {
      height: moderateVerticalScale(1),
      backgroundColor: colors.white,
    },
    verticaleLineView: {
      height: moderateVerticalScale(50),
      backgroundColor: colors.white,
      width: moderateScale(1),
    },
    loginRegisterContainer: {
      flexDirection: 'row',
      justifyContent: 'space-around',
      alignItems: 'center',
    },
    userProfileImage: {
      height: moderateVerticalScale(50),
      width: moderateScale(50),
      borderRadius: 50,
      tintColor: colors.white,
      marginHorizontal: moderateScale(10),
    },
    userProfileImage1: {
      height: moderateVerticalScale(70),
      width: moderateScale(70),
      borderRadius: 70,
      marginHorizontal: moderateScale(10),
      marginVertical: moderateVerticalScale(5),
    },
    innerContainer: {
      flex: 0.85,
    },
    drawerItemName: {
      ...commonStyles.mediumFont16,
      marginHorizontal: moderateScale(15),
    },
    drawerItmeImage: {
      tintColor: colors.black,
      marginHorizontal: moderateScale(20),
    },
    offerBy: {
      width: width / 1.4,
      position: 'absolute',
      bottom: 0,
      alignItems: 'center',
    },
  });
  return styles;
}
