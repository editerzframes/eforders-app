import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  Image,
  FlatList,
  TouchableOpacity,
  Animated,
} from 'react-native';

import {useSelector} from 'react-redux';
import WrapperContainer from '../../Components/WrapperContainer';
import colors from '../../styles/colors';

import SegmentedControlTab from 'react-native-segmented-control-tab';
import imagePath from '../../constants/imagePath';
import Header from '../../Components/Header';
import strings from '../../constants/lang';
import {
  height,
  moderateScale,
  moderateVerticalScale,
  textScale,
  width,
} from '../../styles/responsiveSize';

import TextInputWithLabel from '../../Components/TextInputWithLabel';
import {
  getColorCodeWithOpactiyNumber,
  pressInAnimation,
  pressOutAnimation,
  getScaleTransformationStyle,
  getImageUrl,
} from '../../utils/helperFunctions';
import actions from '../../redux/actions';
import {useFocusEffect} from '@react-navigation/native';
import {CardField, createToken, initStripe} from '@stripe/stripe-react-native';
import {showMessage, hideMessage} from 'react-native-flash-message';
import TransparentButtonWithTxtAndIcon from '../../Components/ButtonComponent';
import RazorpayCheckout from 'react-native-razorpay';
import navigationStrings from '../../constants/navigationStrings';
import {stylesFunc} from './styles';
import ConfettiCannon from 'react-native-confetti-cannon';

export default function Checkout(props) {
  const {navigation, route} = props;

  const paramsData = route?.params;

  const logedInUser = useSelector(state => state?.auth?.userData);
  const primaryLanguage = useSelector(state => state?.home?.primaryLanguage);
  const primaryAddress = useSelector(state => state?.home?.primaryAddress);
  const scaleInAnimated = new Animated.Value(0);
  const [state, setstate] = useState({
    selectedIndex: 0,
    selectedPaymentMethod: null,
    allPaymentMethods: [],
    allProducts: [],
    payableAmount: null,
    totalDiscount: null,
    cardInfo: null,
    shippingAddress: primaryAddress,
    isApplyButton: false,
    mycoupon: '',
    allCoupons: [],
  });

  const {
    selectedIndex,
    selectedPaymentMethod,
    allPaymentMethods,
    allProducts,
    payableAmount,
    totalDiscount,
    cardInfo,
    mycoupon,
    allCoupons,
  } = state;

  const updateState = data => setstate(state => ({...state, ...data}));
  const {ThemeColors, fontFamily} = useSelector(
    state => state?.init?.initAppData,
  );

  const styles = stylesFunc({fontFamily, ThemeColors});

  const handleIndexChange = index => {
    updateState({
      selectedIndex: index,
    });
  };

  const selectPaymentMethod = item => {
    updateState({
      selectedPaymentMethod: item,
    });
  };

  useFocusEffect(
    React.useCallback(() => {
      if (logedInUser?.access_token) {
        getCartData();
        onAllPaymentMethods();
      }
    }, []),
  );

  const getCartData = () => {
    actions
      .getCart({}, logedInUser?.access_token)
      .then(res => {
        console.log(res, 'response');
        updateState({
          allProducts: res?.data?.cart_product,
          payableAmount: res?.data?.total_price,
        });
        // actions.showCartBadge(res?.data?.total_quantity);
      })
      .catch(error => {
        showMessage({
          message: error.message,
          type: 'danger',
        });
      });
  };

  const onAllPaymentMethods = () => {
    actions
      .allPaymentMethods({}, logedInUser?.access_token)
      .then(res => {
        console.log(res, 'all Payment Methods');
        updateState({
          allPaymentMethods: res?.data,
        });
        // actions.showCartBadge(res?.data?.total_quantity);
      })
      .catch(error => {
        showMessage({
          message: error.message,
          type: 'danger',
        });
      });
  };

  const _onChangeStripeData = cardDetails => {
    if (cardDetails?.complete) {
      updateState({
        cardInfo: {
          brand: cardDetails.brand,
          complete: true,
          expiryMonth: cardDetails?.expiryMonth,
          expiryYear: cardDetails?.expiryYear,
          last4: cardDetails?.last4,
          postalCode: cardDetails?.postalCode,
        },
      });
    } else {
      updateState({cardInfo: null});
    }
  };

  // useEffect(() => {}, [allPaymentMethods]);

  const userselectedPaymentMethod = async () => {
    // updateState({
    //   isLoading: true,
    // });
    console.log('stripe called');
    if (cardInfo) {
      await createToken(cardInfo)
        .then(res => {
          console.log(res, 'res>>');
          const data = {
            stripeToken: res?.token?.id,
            price: paramsData
              ? !paramsData?.off_type
                ? payableAmount - paramsData?.off_amount
                : totalDiscountUsingCounpon()
              : payableAmount,
            address: `${
              primaryAddress?.id
                ? primaryAddress?.house_no &&
                  primaryAddress?.house_no &&
                  primaryAddress?.landmark &&
                  primaryAddress?.landmark &&
                  primaryAddress?.state &&
                  primaryAddress?.state &&
                  primaryAddress?.pincode &&
                  primaryAddress?.pincode &&
                  primaryAddress?.country &&
                  primaryAddress?.country
                : primaryAddress?.currentFormatedAddress
            }`,
            code: 'stripe',
          };

          console.log(data, 'datadata');

          actions
            .createPayment(logedInUser?.access_token, data)
            .then(res => {
              // navigation.goBack();
              checkoutOrder(res?.data?.id);
              console.log(res, 'resssss in stripe');
            })
            .catch(error => console.log(error, 'error'));
        })
        .catch(err => {
          console.log(err, 'err>>');
        });
    } else {
      showError(strings.NOT_ADDED_CART_DETAIL_FOR_PAYMENT_METHOD);
    }
  };

  const onPlaceOrderWithGateway = () => {
    if (selectedPaymentMethod?.title === 'Razorpay') {
      const paymentGatewayKeys = JSON.parse(selectedPaymentMethod?.credentials);

      var options = {
        description: 'Credits towards consultation',
        image: 'https://i.imgur.com/3g7nmJC.png',
        currency: 'INR',
        key: paymentGatewayKeys?.api_key, // Your api key
        amount: paramsData
          ? !paramsData?.off_type
            ? (payableAmount - paramsData?.off_amount) * 100
            : totalDiscountUsingCounpon() * 100
          : payableAmount * 100,
        name: 'Pavan',
        prefill: {
          email: 'void@eforders.com',
          contact: '9191919191',
          name: 'EfOrders',
        },
        theme: {color: ThemeColors?.primary_color},
      };
      RazorpayCheckout.open(options)
        .then(data => {
          // handle success

          checkoutOrder(data.razorpay_payment_id);
        })
        .catch(error => {
          // handle failure
          alert(`Error: ${error.code} | ${error.description}`);
        });
    } else if (selectedPaymentMethod?.title === 'Stripe') {
      const paymentGatewayKeys = JSON.parse(selectedPaymentMethod?.credentials);
      console.log(paymentGatewayKeys?.publishable_key, 'paymentGatewayKeys');
      initStripe({
        publishableKey: paymentGatewayKeys?.publishable_key,
        merchantIdentifier: 'merchant.identifier',
      });
      userselectedPaymentMethod();
    }
  };

  // total discount by coupon

  const totalDiscountUsingCounpon = () => {
    if (paramsData?.id) {
      if (paramsData?.off_type) {
        return (payableAmount * paramsData?.off_amount) / 100;
      } else {
        return paramsData?.off_amount;
      }
    } else {
      return;
    }
  };

  const _onChangeText = key => val => {
    updateState({[key]: val});
  };

  const _moveToNextScreen = redircetOn => {
    navigation.navigate(redircetOn);
  };

  //checkout order now

  console.log(primaryAddress, 'primaryAddressprimaryAddress');

  const checkoutOrder = paymentId => {
    let data = {
      address: primaryAddress?.id
        ? primaryAddress?.house_no &&
          primaryAddress?.house_no &&
          primaryAddress?.landmark &&
          primaryAddress?.landmark &&
          primaryAddress?.state &&
          primaryAddress?.state &&
          primaryAddress?.pincode &&
          primaryAddress?.pincode &&
          primaryAddress?.country &&
          primaryAddress?.country
        : primaryAddress?.currentFormatedAddress,
      latitude: primaryAddress?.latitude,
      longitude: primaryAddress?.longitude,
      tip: '0',
      aditional_info: '',
      couponCode: paramsData?.name ? paramsData?.name : '',
      transaction_id: paymentId,
      payment_type: selectedPaymentMethod?.title,
    };

    console.log(data, 'data in order chekout ');

    actions
      .checkoutOrder(logedInUser?.access_token, data)
      .then(res => {
        showMessage({
          message: res?.message,
          type: 'success',
        });
        // navigation.goBack();
        navigation.navigate(navigationStrings.ORDERDETAILS);
        updateState({
          totalPayableAmount: res?.data?.total_payable,
          couponApplied: false,
        });
      })
      .catch(error => {
        showMessage({
          message: error.message,
          type: 'danger',
        });
      });
  };

  // chnage tabIndex By Button Click

  const onChangeTab = () => {
    updateState({
      selectedIndex: 1,
    });
  };

  const renderCheckoutView = () => {
    return (
      <View
        style={{
          flex: 1,
          marginTop: moderateVerticalScale(10),
        }}>
        <View style={{flex: 1}}>
          <FlatList
            data={allProducts}
            showsVerticalScrollIndicator={false}
            keyExtractor={(item, index) => String(index)}
            style={{
              marginTop: moderateVerticalScale(15),
              backgroundColor: colors.backgroundColor,
            }}
            ListHeaderComponent={() => {
              return (
                <View style={styles.addressViewContainer}>
                  <View style={styles.addressViewInnerContainer1}>
                    <View style={styles.addressViewInnerContainer2}>
                      <Image source={imagePath?.location} />
                      <Text style={styles.shppingAddressTitleText}>
                        {strings.SHIPPINGADDRESS}
                      </Text>
                    </View>
                    <View>
                      <Image source={imagePath?.more} />
                    </View>
                  </View>
                  <View style={styles.shppingAddressTextContainer}>
                    {primaryAddress?.id ? (
                      <Text
                        style={{
                          fontSize: textScale(14),
                          fontFamily: fontFamily.regular,
                        }}>
                        {primaryAddress?.house_no && primaryAddress?.house_no}{' '}
                        {primaryAddress?.landmark && primaryAddress?.landmark}{' '}
                        {primaryAddress?.state && primaryAddress?.state}
                        {` ${
                          primaryAddress?.pincode && primaryAddress?.pincode
                        }`}{' '}
                        {primaryAddress?.country && primaryAddress?.country}
                      </Text>
                    ) : (
                      <Text
                        style={{
                          fontSize: textScale(14),
                          fontFamily: fontFamily.regular,
                        }}>
                        {primaryAddress?.currentFormatedAddress}
                      </Text>
                    )}
                  </View>
                  <View
                    style={{
                      flex: 0.3,
                      alignItems: 'center',
                    }}>
                    <Text
                      style={{
                        fontSize: textScale(15),
                        fontFamily: fontFamily.regular,
                        color: ThemeColors?.primary_color,
                      }}>
                      {strings.ADDSHIPPINGNOTES}
                    </Text>
                  </View>
                </View>
              );
            }}
            renderItem={({item, index}) => {
              return (
                <View style={styles.checkoutProductContainer}>
                  <View style={styles.checkoutProductInnerContainer}>
                    <View style={{flexDirection: 'row'}}>
                      <Image
                        style={{marginHorizontal: moderateScale(10)}}
                        source={imagePath?.store}
                      />
                      <Text
                        style={{
                          fontSize: textScale(14),
                          fontFamily: fontFamily.bold,
                        }}>
                        {item?.product?.store?.name}
                      </Text>
                    </View>
                  </View>
                  <View style={styles.productImageContainer}>
                    <View>
                      <Image
                        style={{
                          width: moderateScale(120),
                          height: moderateVerticalScale(110),
                        }}
                        source={{
                          uri: getImageUrl(
                            item?.product?.icon?.image_fit,
                            item?.product?.icon?.image_path,
                            '1000/1000',
                          ),
                        }}
                      />
                    </View>
                    <View
                      style={{
                        marginHorizontal: moderateScale(5),
                      }}>
                      <Text
                        numberOfLines={1}
                        style={{
                          fontFamily: fontFamily.bold,
                          fontSize: textScale(14),
                          width: width / 1.8,
                        }}>
                        {item?.product?.name}
                      </Text>
                      <Text
                        style={{
                          fontFamily: fontFamily.bold,
                          fontSize: textScale(14),
                          color: ThemeColors?.primary_color,
                        }}>
                        {`RP${item?.product?.price}`}
                      </Text>
                      {/* <Text
                        style={{
                          fontFamily: fontFamily.bold,
                          fontSize: textScale(14),
                        }}>
                        Varient: Blue
                      </Text>
                      <Text
                        style={{
                          fontFamily: fontFamily.bold,
                          fontSize: textScale(14),
                        }}>
                        Size : EU-20
                      </Text> */}
                      <Text
                        style={{
                          fontFamily: fontFamily.bold,
                          fontSize: textScale(14),
                        }}>
                        {`${strings.QUENTITY} : ${item?.quantity}`}
                      </Text>
                    </View>
                  </View>
                  <View style={styles.productBottomViewContainer}>
                    <View style={{flexDirection: 'row'}}>
                      <Text style={{marginRight: moderateScale(5)}}>
                        {strings.SHIPPINGMETHOD} :-
                      </Text>
                      <Text>Regular shipping</Text>
                    </View>
                    <View style={{flexDirection: 'row'}}>
                      <Text style={{marginHorizontal: moderateScale(5)}}>
                        (3-4 working Days)
                      </Text>
                    </View>
                  </View>
                </View>
              );
            }}
            ItemSeparatorComponent={() => {
              return (
                <View
                  style={{
                    height: moderateVerticalScale(20),
                  }}
                />
              );
            }}
            ListEmptyComponent={() => {
              return (
                <View
                  style={{
                    height: height / 1.8,
                    width: width,
                    alignItems: 'center',
                  }}></View>
              );
            }}
            ListFooterComponent={
              true
                ? () => {
                    return (
                      <>
                        <TouchableOpacity
                          activeOpacity={8}
                          style={styles.paymentSummeryAndCouponView}
                          onPress={() =>
                            _moveToNextScreen(navigationStrings.COUPONCARDS)
                          }>
                          <Text
                            style={{
                              fontSize: textScale(16),
                              color: colors.black,
                              fontFamily: fontFamily?.bold,
                            }}>
                            {strings.OFFER}
                          </Text>
                          <View
                            style={{
                              marginVertical: moderateVerticalScale(20),
                              flexDirection: 'row',
                              justifyContent: 'space-between',
                            }}>
                            <View
                              style={{
                                flexDirection: 'row',
                                flex: 0.7,
                              }}>
                              {paramsData ? (
                                <Image
                                  style={{
                                    height: moderateVerticalScale(18),
                                    width: moderateScale(18),
                                  }}
                                  source={imagePath.successful}
                                />
                              ) : null}

                              <View>
                                <Text
                                  style={{
                                    fontSize: textScale(12),
                                    color: colors.black,
                                    fontFamily: fontFamily?.regular,
                                    marginHorizontal: moderateScale(15),
                                  }}>
                                  {paramsData
                                    ? `${strings.CODE} ${paramsData?.name} ${strings.APPLIED}`
                                    : strings.APPLYYOURCOUPON}
                                </Text>
                                {paramsData?.description ? (
                                  <Text
                                    style={{
                                      fontSize: textScale(10),
                                      color: colors.textGreyOpcaity7,
                                      fontFamily: fontFamily?.regular,
                                      marginHorizontal: moderateScale(15),
                                    }}>
                                    {paramsData?.description}
                                  </Text>
                                ) : null}
                              </View>
                            </View>
                            <View
                              style={{
                                alignItems: 'flex-end',
                                flex: 0.3,
                              }}>
                              {paramsData ? (
                                <>
                                  <Text
                                    style={{
                                      fontSize: textScale(12),
                                      color: colors.textBlue,
                                      fontFamily: fontFamily?.bold,
                                    }}>
                                    {`-${paramsData?.off_amount}${
                                      paramsData?.off_type ? '%' : ''
                                    }`}
                                  </Text>
                                  <Text
                                    style={{
                                      fontSize: textScale(12),
                                      color: ThemeColors?.primary_color,
                                      fontFamily: fontFamily?.bold,
                                    }}>
                                    {strings.CHANGEOFFER}
                                  </Text>
                                </>
                              ) : (
                                <Text style={{color: colors.red}}>
                                  {strings.APPLY}
                                </Text>
                              )}
                            </View>

                            <View></View>
                          </View>
                        </TouchableOpacity>
                        <View
                          style={{
                            width: width - 20,
                            marginHorizontal: moderateScale(10),
                            overflow: 'hidden',
                            shadowColor: colors.black,
                            shadowOffset: {width: 0, height: 1},
                            shadowOpacity: 0.8,
                            shadowRadius: 2,
                            elevation: 5,
                            backgroundColor: colors.white,
                            borderRadius: 5,
                          }}>
                          <View
                            style={{
                              flexDirection: 'row',
                              justifyContent: 'space-between',
                              paddingVertical: moderateVerticalScale(10),
                              paddingHorizontal: moderateScale(10),
                            }}>
                            <Text
                              style={{
                                fontSize: textScale(14),
                                fontFamily: fontFamily.regular,
                              }}>
                              {strings.TOTALITEMS}
                            </Text>
                            <Text
                              style={{
                                fontSize: textScale(14),
                                fontFamily: fontFamily.regular,
                              }}>
                              {allProducts?.length}
                            </Text>
                          </View>
                          <View
                            style={{
                              flexDirection: 'row',
                              justifyContent: 'space-between',
                              paddingVertical: moderateVerticalScale(10),
                              paddingHorizontal: moderateScale(10),
                            }}>
                            {payableAmount ? (
                              <>
                                <Text
                                  style={{
                                    fontSize: textScale(14),
                                    fontFamily: fontFamily.regular,
                                  }}>
                                  {strings.TOTALPRICE}
                                </Text>
                                <Text
                                  style={{
                                    fontSize: textScale(14),
                                    fontFamily: fontFamily.regular,
                                  }}>
                                  {payableAmount}
                                </Text>
                              </>
                            ) : null}
                          </View>
                          {totalDiscountUsingCounpon() ? (
                            <View
                              style={{
                                flexDirection: 'row',
                                justifyContent: 'space-between',
                                paddingVertical: moderateVerticalScale(10),
                                paddingHorizontal: moderateScale(10),
                              }}>
                              <Text
                                style={{
                                  fontSize: textScale(14),
                                  fontFamily: fontFamily.regular,
                                }}>
                                {strings.TOTALDISCOUNT}
                              </Text>
                              <Text
                                style={{
                                  fontSize: textScale(14),
                                  fontFamily: fontFamily.regular,
                                }}>
                                {totalDiscountUsingCounpon()}
                              </Text>
                            </View>
                          ) : null}

                          <View
                            style={{
                              height: 0.5,
                              width: width - 20,
                              backgroundColor: colors.textGrey,
                              marginTop: moderateVerticalScale(10),
                            }}
                          />
                          <View
                            style={{
                              flexDirection: 'row',
                              justifyContent: 'space-between',
                              paddingVertical: moderateVerticalScale(20),
                              paddingHorizontal: moderateScale(10),
                            }}>
                            <Text
                              style={{
                                fontSize: textScale(14),
                                fontFamily: fontFamily.regular,
                              }}>
                              {strings.FINALPRICE}
                            </Text>
                            {payableAmount ? (
                              <Text
                                style={{
                                  fontSize: textScale(14),
                                  fontFamily: fontFamily.regular,
                                }}>
                                {paramsData
                                  ? !paramsData?.off_type
                                    ? payableAmount - paramsData?.off_amount
                                    : totalDiscountUsingCounpon()
                                  : payableAmount}
                              </Text>
                            ) : null}
                          </View>
                        </View>
                        <View style={{height: moderateVerticalScale(100)}} />
                      </>
                    );
                  }
                : null
            }
          />
        </View>
        <TouchableOpacity style={styles.PlaceOrderButton} onPress={onChangeTab}>
          <Text
            style={{
              fontFamily: fontFamily?.bold,
              fontSize: textScale(14),
              color: colors.white,
            }}>
            Place Order
          </Text>
        </TouchableOpacity>
      </View>
    );
  };

  const renderPaymentView = () => {
    return (
      <View
        style={{
          flex: 1,
          paddingHorizontal: moderateScale(20),
          paddingVertical: moderateVerticalScale(10),
        }}>
        <Text
          style={{
            fontSize: textScale(15),
            fontFamily: fontFamily.regular,
            color: colors.black,
            marginVertical: moderateVerticalScale(20),
          }}>
          {strings.CHOOSEPAYMENTMETHOD}
        </Text>
        {allPaymentMethods?.map((item, index) => {
          return (
            <View>
              <View
                style={{
                  borderColor: colors.textGreyLight,
                  borderBottomWidth: 0.25,
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'space-between',
                }}>
                <Text
                  style={{
                    fontFamily: fontFamily.bold,
                    fontSize: textScale(16),
                    color: colors.black,
                    marginVertical: moderateVerticalScale(20),
                  }}>
                  {item?.title}
                </Text>
                <TouchableOpacity onPress={() => selectPaymentMethod(item)}>
                  <Image
                    source={
                      selectedPaymentMethod?.id === item?.id
                        ? imagePath.activeRadio
                        : imagePath.inActiveRadio
                    }
                  />
                </TouchableOpacity>
              </View>

              {item?.title == 'Stripe' &&
                selectedPaymentMethod?.id === item?.id && (
                  <View
                    style={{
                      // alignItems: 'center',
                      marginRight: moderateScale(20),
                    }}>
                    <CardField
                      postalCodeEnabled={false}
                      placeholder={{
                        number: '4242 4242 4242 4242',
                      }}
                      cardStyle={{
                        backgroundColor: '#FFFFFF',
                        textColor: '#000000',
                      }}
                      style={{
                        width: '100%',
                        height: 50,
                        // marginVertical: 10,
                      }}
                      onCardChange={cardDetails => {
                        console.log('cardDetails', cardDetails);
                        _onChangeStripeData(cardDetails);
                      }}
                      onFocus={focusedField => {
                        console.log('focusField', focusedField);
                      }}
                      onBlur={() => {
                        Keyboard.dismiss();
                      }}
                    />
                  </View>
                )}
            </View>
          );
        })}
        {selectedPaymentMethod && (
          <View style={{position: 'absolute', bottom: 20, alignSelf: 'center'}}>
            <TransparentButtonWithTxtAndIcon
              textStyle={{
                fontFamily: fontFamily.bold,
                fontSize: textScale(14),
              }}
              btnText={`Confirm & Pay ${
                paramsData
                  ? !paramsData?.off_type
                    ? payableAmount - paramsData?.off_amount
                    : totalDiscountUsingCounpon()
                  : payableAmount
              }`}
              btnStyle={{
                backgroundColor: ThemeColors?.primary_color,
                borderRadius: moderateScale(20),
                width: width / 1.2,
                paddingVertical: moderateVerticalScale(20),
              }}
              onPress={
                () => navigation.navigate(navigationStrings.ORDERDETAILS)
                // onPlaceOrderWithGateway()
              }
            />
          </View>
        )}
      </View>
    );
  };

  return (
    <WrapperContainer bgColor={colors.white}>
      <Header
        leftIcon={imagePath?.back}
        textStyle={{color: colors.black}}
        centerTitle={selectedIndex == 0 ? strings.CHECKOUT : strings.PAYMENT}
      />
      <View style={{marginHorizontal: moderateScale(10)}}>
        <SegmentedControlTab
          values={['Checkout', 'Payment']}
          selectedIndex={selectedIndex}
          onTabPress={handleIndexChange}
          activeTabStyle={{backgroundColor: ThemeColors?.primary_color}}
          tabStyle={{borderColor: ThemeColors?.primary_color}}
          tabTextStyle={{color: ThemeColors?.primary_color}}
        />
      </View>
      {selectedIndex == 0 ? renderCheckoutView() : renderPaymentView()}
      {paramsData ? (
        <ConfettiCannon count={20} origin={{x: 200, y: 0}} />
      ) : null}
    </WrapperContainer>
  );
}
