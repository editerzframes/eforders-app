import React, {useEffect, useState} from 'react';
import {View, Text, FlatList, TouchableOpacity, Image} from 'react-native';
import Header from '../../Components/Header';
import WrapperContainer from '../../Components/WrapperContainer';
import imagePath from '../../constants/imagePath';
import strings from '../../constants/lang';
import navigationStrings from '../../constants/navigationStrings';
import actions from '../../redux/actions';
import colors from '../../styles/colors';
import fontFamily from '../../styles/fontFamily';
import {
  moderateScale,
  moderateVerticalScale,
  textScale,
  width,
} from '../../styles/responsiveSize';
import {getColorCodeWithOpactiyNumber} from '../../utils/helperFunctions';
import {useFocusEffect} from '@react-navigation/native';
import {showMessage, hideMessage} from 'react-native-flash-message';
import TextInputWithLabel from '../../Components/TextInputWithLabel';
import {useSelector} from 'react-redux';
import {stylesFunc} from './styles';

export default function AddressList(props) {
  const {navigation} = props;
  const primaryAddress = useSelector(state => state?.home?.primaryAddress);
  const [state, setState] = useState({
    allAddresses: [],
    isDeleted: null,
    selectedPrimaryAddress: primaryAddress,
    searchAddressByUser: '',
  });

  const {allAddresses, isDeleted, selectedPrimaryAddress, searchAddressByUser} =
    state;
  const updateState = data => setState(state => ({...state, ...data}));
  const logedInUser = useSelector(state => state?.auth?.userData);
  const {ThemeColors, fontFamily} = useSelector(
    state => state?.init?.initAppData,
  );

  const styles = stylesFunc({fontFamily, ThemeColors});

  const _moveToNextScreen = (item, data) => {
    navigation.navigate(item, data);
  };

  const _onChangeText = key => val => {
    console.log(val, key);
    updateState({[key]: val});
  };

  useEffect(() => {
    getAllAddresses();
  }, [isDeleted]);

  useEffect(() => {
    getAllAddresses();
  }, [selectedPrimaryAddress]);

  useFocusEffect(
    React.useCallback(() => {
      getAllAddresses();
    }, []),
  );

  const getAllAddresses = () => {
    actions
      .getAllAddresses()
      .then(res =>
        updateState({
          allAddresses: res?.data,
        }),
      )
      .catch(error => console.log(error, 'error'));
  };

  const deleteAddress = id => {
    let query = `/${id}`;
    actions
      .deleteAddress(query)
      .then(res => {
        updateState({
          isDeleted: isDeleted + 1,
        });
        showMessage({
          message: res.message,
          type: 'success',
        });
      })
      .catch(error => {
        showMessage({
          message: error.message,
          type: 'danger',
        });
      });
  };

  const _onSelectPrimaryAddress = (item, index) => {
    let query = `/${item?.id}`;
    actions
      .setPrimaryAddress(query)
      .then(res =>
        updateState({
          selectedPrimaryAddress: res?.data,
        }),
      )
      .catch(error => console.log(error, 'error'));
  };

  useEffect(() => {
    if (searchAddressByUser?.length >= 0) {
      onSearchAddress();
    }
  }, [searchAddressByUser]);

  const onSearchAddress = () => {
    let query = `/${searchAddressByUser}?page=${
      allAddresses?.length >= 10 ? pageNo : 1
    }`;
    actions
      .searchAddress(query)
      .then(res => (
        // updateState({
        //   allSearchedProducts: res?.products?.data,
        //   allSearchedCategories: res?.categories?.data,
        // }),
        <></>
      ))
      .catch(error => console.log(error, 'error'));
  };

  return (
    <WrapperContainer>
      <View
        style={{
          flex: 1,
          alignItems: 'center',
          paddingHorizontal: moderateScale(10),
        }}>
        <Header
          leftIcon={imagePath.back}
          textStyle={{color: colors.black}}
          centerTitle={strings.ADDRESSES}
          rightIcon={imagePath.plus}
          onPressRight={() => _moveToNextScreen(navigationStrings.EDITADDRESS)}
        />

        <View>
          <FlatList
            showsVerticalScrollIndicator={false}
            data={allAddresses}
            style={{alignSelf: 'center'}}
            ItemSeparatorComponent={() => {
              return <View style={{width: moderateVerticalScale(10)}} />;
            }}
            // ListHeaderComponent={() => {
            //   return (
            //     <View
            //       style={{
            //         marginHorizontal: moderateScale(10),
            //         marginVertical: moderateVerticalScale(10),
            //         width: width - 40,
            //       }}>
            //       <TouchableOpacity
            //         onPress={() =>
            //           _moveToNextScreen(navigationStrings.GLOBALSEARCH)
            //         }>
            //         <TextInputWithLabel
            //           placeholder={strings.SEARCH}
            //           customTextStyle={{
            //             height: moderateVerticalScale(50),
            //             borderColor: getColorCodeWithOpactiyNumber(
            //               colors.textGreyLight.substr(1),
            //               20,
            //             ),
            //             backgroundColor: getColorCodeWithOpactiyNumber(
            //               colors.textGreyLight.substr(1),
            //               20,
            //             ),
            //             paddingHorizontal: moderateScale(15),
            //             fontFamily: fontFamily.regular,
            //           }}
            //           rightIcon={imagePath.search}
            //           userFocus={focus => <></>}
            //           editable={false}
            //         />
            //       </TouchableOpacity>
            //       {/* <TextInputWithLabel
            //         placeholder={strings.FINDADDRESS}
            //         customTextStyle={{
            //           height: moderateVerticalScale(50),
            //           borderColor: getColorCodeWithOpactiyNumber(
            //             colors.textGreyLight.substr(1),
            //             20,
            //           ),
            //           backgroundColor: getColorCodeWithOpactiyNumber(
            //             colors.textGreyLight.substr(1),
            //             20,
            //           ),
            //           paddingHorizontal: moderateScale(15),
            //           fontFamily: fontFamily.regular,
            //         }}
            //         rightIcon={imagePath.search}
            //         userFocus={focus => <></>}
            //         onChangeText={() => _onChangeText('searchedAddress')}
            //       /> */}
            //     </View>
            //   );
            // }}
            renderItem={({item, index}) => {
              return (
                <TouchableOpacity
                  activeOpacity={0.5}
                  style={{
                    width: moderateScale(width - 40),
                    borderColor:
                      selectedPrimaryAddress?.id === item?.id
                        ? ThemeColors?.primary_color
                        : colors.black,
                    borderWidth: 0.8,
                    borderRadius: 5,
                    paddingVertical: moderateVerticalScale(5),
                    paddingHorizontal: moderateScale(10),
                    backgroundColor: colors.white,
                    marginTop: moderateVerticalScale(20),
                    alignSelf: 'center',
                  }}
                  onPress={() => _onSelectPrimaryAddress(item, index)}>
                  <View
                    style={{
                      flexDirection: 'row',
                      justifyContent: 'space-between',
                    }}>
                    <Text
                      numberOfLines={1}
                      style={{
                        fontSize: textScale(12.5),
                        color: colors.black,
                        fontFamily: fontFamily.bold,
                      }}>
                      {item?.name}
                    </Text>
                    <Text
                      numberOfLines={1}
                      style={{
                        fontSize: textScale(12.5),
                        color:
                          selectedPrimaryAddress?.id === item?.id
                            ? ThemeColors?.primary_color
                            : colors.textGreyLight,
                        fontFamily: fontFamily.bold,
                        backgroundColor:
                          selectedPrimaryAddress?.id === item?.id
                            ? getColorCodeWithOpactiyNumber(
                                ThemeColors?.primary_color.substr(1),
                                20,
                              )
                            : getColorCodeWithOpactiyNumber(
                                colors.textGreyLight.substr(1),
                                20,
                              ),
                        paddingVertical: moderateVerticalScale(5),
                        paddingHorizontal: moderateScale(10),
                        borderRadius: 8,
                      }}>
                      {item?.address_type}
                    </Text>
                  </View>
                  <View
                    style={{
                      flexDirection: 'row',
                      justifyContent: 'space-between',
                      alignItems: 'center',
                    }}>
                    <View>
                      <Text
                        style={{
                          fontSize: textScale(12.5),
                          color: colors.black,
                          fontFamily: fontFamily.regular,
                          marginRight: moderateScale(10),
                          marginVertical: moderateVerticalScale(3),
                        }}>
                        {item?.house_no && item?.house_no} {'\n'}
                        {item?.landmark && item?.landmark} {'\n'}
                        {item?.state && item?.state}
                        {` ${item?.pincode && item?.pincode}`} {'\n'}
                        {item?.country && item?.country}
                      </Text>

                      {item?.phone_number && (
                        <Text
                          style={{
                            fontSize: textScale(12.5),
                            color: colors.black,
                            fontFamily: fontFamily.regular,
                            marginRight: moderateScale(10),
                            marginVertical: moderateVerticalScale(2),
                          }}>
                          {item?.phone_number}
                        </Text>
                      )}
                    </View>
                    <Image
                      style={{
                        height: moderateVerticalScale(100),
                        width: moderateScale(100),
                        resizeMode: 'contain',
                      }}
                      source={imagePath.map}
                    />
                  </View>
                  <View
                    style={{
                      flexDirection: 'row',
                      marginVertical: moderateScale(10),
                    }}>
                    <Image source={imagePath.location} />
                    <Text
                      style={{
                        marginHorizontal: moderateScale(10),
                        color: colors.btnABlue,
                      }}>
                      {strings.UPDATEDELIVERYLOCATION}
                    </Text>
                  </View>
                  <View
                    style={{
                      flexDirection: 'row',
                      justifyContent: 'space-between',
                      marginVertical: moderateVerticalScale(14),
                    }}>
                    <TouchableOpacity
                      style={{
                        width: moderateScale(100),
                        height: moderateVerticalScale(30),
                        borderColor: colors.black,
                        borderWidth: 0.3,
                        borderRadius: 5,
                        paddingVertical: moderateVerticalScale(5),
                        paddingHorizontal: moderateScale(10),
                        backgroundColor: colors.white,
                      }}
                      onPress={() =>
                        _moveToNextScreen(navigationStrings.EDITADDRESS, item)
                      }>
                      <Text style={{textAlign: 'center'}}>{strings.EDIT}</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                      style={{
                        width: moderateScale(100),
                        height: moderateVerticalScale(30),
                        borderColor: colors.black,
                        borderWidth: 0.3,
                        borderRadius: 5,
                        paddingVertical: moderateVerticalScale(5),
                        paddingHorizontal: moderateScale(10),
                        backgroundColor: colors.white,
                      }}
                      onPress={() => deleteAddress(item?.id)}>
                      <Text style={{textAlign: 'center'}}>
                        {strings.REMOVE}
                      </Text>
                    </TouchableOpacity>
                  </View>
                </TouchableOpacity>
              );
            }}
            ListFooterComponent={() => (
              <View style={{height: moderateVerticalScale(50)}} />
            )}
          />
        </View>
      </View>
    </WrapperContainer>
  );
}
