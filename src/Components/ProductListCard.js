import React from 'react';
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  Animated,
  StyleSheet,
} from 'react-native';

import imagePath from '../constants/imagePath';
import colors from '../styles/colors';
import fontFamily from '../styles/fontFamily';
import {
  height,
  moderateScale,
  moderateVerticalScale,
  textScale,
  width,
} from '../styles/responsiveSize';
import {
  getImageUrl,
  getScaleTransformationStyle,
  pressInAnimation,
  pressOutAnimation,
} from '../utils/helperFunctions';

import strings from '../constants/lang';
import {useSelector} from 'react-redux';
import CountDownTimer from './CountDownTimer';

export default function ProductListCard({data, onPress, horizontal}) {
  const scaleInAnimated = new Animated.Value(0);
  const totalDiscountPrice = data?.sale_price - data?.price;
  const totalDiscountPersentage = (totalDiscountPrice / data?.sale_price) * 100;
  const {ThemeColors, fontFamily} = useSelector(
    state => state?.init?.initAppData,
  );
  const styles = stylesFunc({fontFamily, ThemeColors});

  return (
    <TouchableOpacity
      style={{
        marginHorizontal: moderateScale(10),
        paddingBottom: moderateVerticalScale(10),
        backgroundColor: colors.white,
        borderRadius: 18,
        overflow: 'hidden',
        marginVertical: 8,
        ...getScaleTransformationStyle(scaleInAnimated),
      }}
      activeOpacity={1}
      onPressIn={() => pressInAnimation(scaleInAnimated)}
      onPressOut={() => pressOutAnimation(scaleInAnimated)}
      onPress={() => onPress(data)}>
      <View>
        <Image
          source={{
            uri: getImageUrl(
              data?.icon?.image_fit,
              data?.icon?.image_path,
              '1000/1000',
            ),
          }}
          style={{
            width: moderateScale(horizontal ? 180 : width),
            height: moderateVerticalScale(horizontal ? 120 : height / 4),
          }}
          resizeMode="stretch"
        />
        {totalDiscountPersentage > 0 ? (
          <View
            style={{
              height: moderateVerticalScale(100),
              width: moderateScale(22),
              backgroundColor: ThemeColors?.primary_color,
              position: 'absolute',
              transform: [{rotate: '30deg'}],
              marginTop: moderateVerticalScale(-13),

              // marginLeft: moderateScale(10),
            }}>
            <View
              style={{
                transform: [{rotate: '-90deg'}],
                flexDirection: 'row',
                width: moderateScale(50),
                marginTop: moderateVerticalScale(28),
                marginLeft: moderateScale(-13),
              }}>
              <Text
                style={{
                  color: colors.white,
                  fontSize: textScale(10),
                  fontFamily: fontFamily?.bold,
                }}>
                {`${totalDiscountPersentage.toFixed()}% `}
              </Text>
              <Text
                style={{
                  color: colors.white,
                  fontSize: textScale(10),
                  fontFamily: fontFamily?.bold,
                }}>
                {strings.OFF}
              </Text>
            </View>
          </View>
        ) : null}
        <View
          style={{
            height: moderateVerticalScale(25),
            width: moderateScale(45),
            backgroundColor: colors.green,
            position: 'absolute',
            right: 0,
            top: 10,
            borderTopLeftRadius: 20,
            borderBottomLeftRadius: 20,
            justifyContent: 'center',
            alignItems: 'center',
            flexDirection: 'row',
          }}>
          <Text
            style={{
              color: colors.white,
              fontSize: textScale(12),
              top: -1,
              fontFamily: fontFamily?.bold,
            }}>
            ☆
          </Text>
          <Text
            style={{
              color: colors.white,
              fontSize: textScale(12),
              marginHorizontal: moderateScale(2),
              fontFamily: fontFamily?.bold,
            }}>
            4.5
          </Text>
        </View>
      </View>
      <Text numberOfLines={2} style={styles.itemText}>
        {data.name}
      </Text>
      <View style={styles.priceView}>
        <Text style={styles.priceText}>{`Price :  ${data?.price}`}</Text>
        <Text
          style={[
            styles.priceText,
            {textDecorationLine: 'line-through'},
          ]}>{`${data?.sale_price}`}</Text>
      </View>
      {false && (
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            marginHorizontal: moderateScale(10),
          }}>
          <Text style={{fontFamily: fontFamily?.regular}}>
            {strings.ENDSIN}
          </Text>
          <CountDownTimer />
        </View>
      )}
    </TouchableOpacity>
  );
}
export function stylesFunc({fontFamily, ThemeColors}) {
  const styles = StyleSheet.create({
    itemText: {
      fontSize: textScale(12),
      fontFamily: fontFamily?.bold,
      marginHorizontal: moderateScale(10),
      color: colors.black,
      width: moderateScale(150),
      marginTop: moderateVerticalScale(5),
    },
    priceText: {
      fontSize: textScale(10),
      fontFamily: fontFamily?.medium,
      marginLeft: moderateScale(10),
      color: colors.black,
    },
    priceView: {
      flexDirection: 'row',
      marginVertical: moderateVerticalScale(5),
    },
  });

  return styles;
}
