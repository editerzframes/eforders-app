import React, {useState} from 'react';
import {View, Text, Image, TouchableOpacity} from 'react-native';
import AppIntroSlider from 'react-native-app-intro-slider';
import navigationStrings from '../../constants/navigationStrings';
import colors from '../../styles/colors';
import {
  height,
  moderateVerticalScale,
  textScale,
  width,
} from '../../styles/responsiveSize';

export default function OuterScreen(props) {
  const {navigation} = props;
  const [state, setState] = useState({
    slides: [
      {
        key: 1,
        title: 'Title 1',
        text: 'Description.\nSay something cool',
        image: 'https://shorturl.at/cAKX6',
        backgroundColor: '#59b2ab',
      },
      {
        key: 2,
        title: 'Title 2',
        text: 'Other cool stuff',
        image: 'https://shorturl.at/flsP8',
        backgroundColor: '#febe29',
      },
      {
        key: 3,
        title: 'Rocket guy',
        text: "I'm already out of descriptions\n\nLorem ipsum bla bla bla",
        image: 'https://shorturl.at/fFX57',
        backgroundColor: '#22bcb5',
      },
      {
        key: 4,
        title: 'Rocket guy',
        text: "I'm already out of descriptions\n\nLorem ipsum bla bla bla",
        image: 'https://shorturl.at/fFX57',
        backgroundColor: '#22bcb5',
      },
      {
        key: 5,
        title: 'Rocket guy',
        text: "I'm already out of descriptions\n\nLorem ipsum bla bla bla",
        image: 'https://shorturl.at/fFX57',
        backgroundColor: '#22bcb5',
      },
    ],
  });

  const {slides} = state;
  const updateState = data => setState(state => ({...state, ...data}));

  const _renderItem = ({item}) => {
    return (
      <View style={{flex: 1, backgroundColor: item?.backgroundColor}}>
        {/* <Text>{item.title}</Text> */}
        <Image
          style={{height: height, width: width}}
          source={{uri: item.image}}
        />
      </View>
    );
  };
  const _onDone = () => {
    // User finished the introduction. Show real app through
    // navigation or simply by controlling state
    navigation.navigate(navigationStrings.LOGIN);
  };

  return (
    <View style={{flex: 1}}>
      <AppIntroSlider renderItem={_renderItem} data={slides} onDone={_onDone} />
      <TouchableOpacity onPress={_onDone}>
        <Text
          style={{
            position: 'absolute',
            bottom: moderateVerticalScale(45),
            left: 30,
            color: colors.white,
            fontSize: textScale(16),
          }}>
          {'Skip'}
        </Text>
      </TouchableOpacity>
    </View>
  );
}
