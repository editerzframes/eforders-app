import React from 'react';
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  Animated,
  StyleSheet,
} from 'react-native';
import {useSelector} from 'react-redux';

import imagePath from '../constants/imagePath';
import colors from '../styles/colors';
import {
  height,
  moderateScale,
  moderateVerticalScale,
  textScale,
  width,
} from '../styles/responsiveSize';
import {
  getImageUrl,
  getScaleTransformationStyle,
  pressInAnimation,
  pressOutAnimation,
} from '../utils/helperFunctions';
import HTMLView from 'react-native-htmlview';
import {Rating, AirbnbRating} from 'react-native-ratings';

export default function HorizontalListCard({
  data,
  onPress,
  cratItemIncrementDecrement,
  addTocart,
}) {
  const scaleInAnimated = new Animated.Value(0);
  const {ThemeColors, fontFamily} = useSelector(
    state => state?.init?.initAppData,
  );

  console.log(data, 'datadatadata');

  const styles = stylesFunc({fontFamily, ThemeColors});
  return (
    <TouchableOpacity
      style={{
        marginHorizontal: moderateScale(10),
        width: width - 40,
        backgroundColor: colors.white,
        borderRadius: 5,
        overflow: 'hidden',
        shadowColor: '#000',
        shadowOffset: {width: 0, height: 1},
        shadowOpacity: 0.8,
        shadowRadius: 2,
        elevation: 5,
        flexDirection: 'row',
        marginVertical: 8,
        ...getScaleTransformationStyle(scaleInAnimated),
      }}
      activeOpacity={1}
      onPressIn={() => pressInAnimation(scaleInAnimated)}
      onPressOut={() => pressOutAnimation(scaleInAnimated)}
      onPress={() => onPress(data)}>
      <Image
        source={{
          uri: getImageUrl(
            data?.icon?.image_fit,
            data?.icon?.image_path,
            '1000/1000',
          ),
        }}
        style={{
          width: moderateScale(100),
          height: moderateVerticalScale(100),
          borderRadius: 8,
        }}
      />
      <View style={styles.productInfoContainer}>
        <Text numberOfLines={2} style={styles.vendorNameText}>
          {data?.name}
        </Text>
        <Text numberOfLines={2} style={styles.vendorDescriptionText}>
          {`$${data?.price}`}
        </Text>
        <View
          style={{
            alignItems: 'flex-start',
            marginVertical: moderateVerticalScale(5),
          }}>
          <Rating
            ratingCount={5}
            type="custom"
            imageSize={16}
            startingValue={5}
            isDisable={true}
            ratingColor={colors.lightYellow}
          />
        </View>

        <TouchableOpacity
          onPress={() => addTocart(data?.id, data?.price)}
          style={{
            height: moderateVerticalScale(35),
            backgroundColor: ThemeColors?.primary_color,
            borderRadius: 2,
            flexDirection: 'row',
            justifyContent: 'space-evenly',
            alignItems: 'center',
            marginBottom: moderateVerticalScale(10),
            marginRight: moderateScale(width / 4.5),
          }}>
          <View
            style={{
              backgroundColor: colors.white,
              paddingHorizontal: moderateScale(5),
              paddingVertical: moderateVerticalScale(5),
              borderRadius: 20,
            }}>
            <Image
              style={{tintColor: ThemeColors?.primary_color}}
              source={imagePath.cart}
            />
          </View>
          <Text
            style={{
              color: colors.white,
              fontFamily: fontFamily.bold,
              fontSize: textScale(12),
            }}>
            ADD TO CART
          </Text>
        </TouchableOpacity>
      </View>
    </TouchableOpacity>
  );
}

export function stylesFunc({fontFamily, ThemeColors}) {
  const styles = StyleSheet.create({
    vendorNameText: {
      color: colors.black,
      fontFamily: fontFamily.bold,
      fontSize: textScale(12),
    },
    vendorDescriptionText: {
      color: colors.black,
      fontFamily: fontFamily.regular,
      fontSize: textScale(12),
    },
    productInfoContainer: {
      width: width / 1.7,
      marginLeft: moderateScale(5),
    },
    descriptionText: {
      // a: {height: moderateVerticalScale(20)},
      // div: {
      //   height: moderateVerticalScale(18),
      // },
      // p: {
      //   fontFamily: fontFamily.regular,
      //   fontSize: textScale(14),
      //   height: moderateVerticalScale(20),
      // },
    },
  });

  return styles;
}
