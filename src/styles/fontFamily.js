export default {
  // regular:"CircularStd-",
  bold: 'CircularStd-Bold',
  regular: 'CircularStd-Book',
  medium: 'CircularStd-Medium',
};
