import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  FlatList,
  SectionList,
  TouchableOpacity,
  Image,
} from 'react-native';
import TextInputWithLabel from '../../Components/TextInputWithLabel';
import WrapperContainer from '../../Components/WrapperContainer';
import imagePath from '../../constants/imagePath';
import strings from '../../constants/lang';
import actions from '../../redux/actions';
import colors from '../../styles/colors';
import fontFamily from '../../styles/fontFamily';
import {
  height,
  moderateScale,
  moderateVerticalScale,
  textScale,
  width,
} from '../../styles/responsiveSize';
import {
  getColorCodeWithOpactiyNumber,
  getImageUrl,
} from '../../utils/helperFunctions';
import {useFocusEffect} from '@react-navigation/native';
import navigationStrings from '../../constants/navigationStrings';
import {useSelector} from 'react-redux';
import {cloneDeep, debounce} from 'lodash';

export default function Category(props) {
  const {navigation} = props;
  const [state, setState] = useState({
    selectedIndexForExpendView: null,
    categoryData: [],
    allSubCategories: [],
    pageNo: 1,
    limit: 12,
  });

  const {
    selectedIndexForExpendView,
    categoryData,
    allSubCategories,
    pageNo,
    limit,
  } = state;
  const {ThemeColors, fontFamily} = useSelector(
    state => state?.init?.initAppData,
  );
  const primaryAddress = useSelector(state => state?.home?.primaryAddress);

  const updateState = data => setState(state => ({...state, ...data}));

  const onOpenExpendView = index => {
    updateState({
      selectedIndexForExpendView: index,
    });
  };

  // useFocusEffect(
  //   React.useCallback(() => {
  //     getCategoryData();
  //   }, []),
  // );

  useEffect(() => {
    getCategoryData();
  }, [pageNo]);

  const getCategoryData = () => {
    let query = `/${''}?limit=${limit}&page=${pageNo}`;
    actions
      .categoryListing()
      .then(res => {
        const chunk = (arr, size) =>
          Array.from({length: Math.ceil(arr.length / size)}, (v, i) => {
            return {data: arr.slice(i * size, i * size + size), id: i};
          });
        let result = chunk(res.data.data, 3);
        updateState({
          categoryData: result,
        });
      })
      .catch(error => console.log(error));
  };

  const onSelectedCategory = id => {
    let query = `/${id}`;
    let data = {
      latitude: primaryAddress?.latitude,
      longitude: primaryAddress?.longitude,
    };
    actions
      .categoriesDetails(query, data)
      .then(res =>
        updateState({
          allSubCategories: res?.categories?.data,
        }),
      )
      .catch(error => console.log(error, 'error'));
  };

  const _moveToNextScreen = (redircetOn, data) => {
    navigation.navigate(redircetOn, {
      selectedProduct: data,
    });
  };

  console.log(categoryData, 'categoryDatacategoryData');

  //pagination of data
  const onEndReached = ({distanceFromEnd}) => {
    updateState({pageNo: pageNo + 1});
  };

  const onEndReachedDelayed = debounce(onEndReached, 1000, {
    leading: true,
    trailing: false,
  });

  return (
    <WrapperContainer bgColor={colors.white}>
      <View
        style={{
          marginHorizontal: moderateScale(10),
          marginVertical: moderateVerticalScale(10),
        }}>
        <TouchableOpacity
          style={{
            marginHorizontal: moderateScale(10),
            marginTop: moderateVerticalScale(-10),
          }}
          onPress={() => _moveToNextScreen(navigationStrings.GLOBALSEARCH)}>
          <TextInputWithLabel
            placeholder={'Search category'}
            customTextStyle={{
              height: moderateVerticalScale(50),
              borderColor: getColorCodeWithOpactiyNumber(
                colors.textGreyLight.substr(1),
                20,
              ),
              backgroundColor: getColorCodeWithOpactiyNumber(
                colors.textGreyLight.substr(1),
                20,
              ),
              paddingHorizontal: moderateScale(15),
              fontFamily: fontFamily?.regular,
            }}
            rightIcon={imagePath.search}
            userFocus={focus => <></>}
            editable={false}
            //   onChangeText={_onChangeText('searchedAddress')}
          />
        </TouchableOpacity>
      </View>
      <View style={{paddingHorizontal: moderateScale(10)}}>
        <SectionList
          sections={categoryData}
          keyExtractor={(item, index) => item + index}
          showsVerticalScrollIndicator={false}
          renderItem={({item, index}) => null}
          renderSectionHeader={({section}) => (
            <FlatList
              horizontal
              data={section?.data}
              renderItem={({item, index}) => {
                return (
                  <TouchableOpacity
                    activeOpacity={0.8}
                    style={{
                      marginHorizontal: moderateScale(5),
                      marginTop: moderateVerticalScale(10),
                      height: moderateVerticalScale(160),
                      width: moderateScale(110),
                      borderRadius: moderateScale(5),
                      backgroundColor: colors.white,
                      borderRadius: 10,
                      overflow: 'hidden',
                      shadowColor: colors.black,
                      shadowOffset: {width: 0, height: 1},
                      shadowOpacity: 0.8,
                      shadowRadius: 2,
                      elevation: 5,
                      marginVertical: moderateVerticalScale(10),
                    }}
                    onPress={
                      item?.subcategories?.length
                        ? () => {
                            onSelectedCategory(item?.id);
                            onOpenExpendView(section?.id);
                          }
                        : () =>
                            _moveToNextScreen(
                              navigationStrings.SUBCATEGORIES,
                              item,
                            )
                    }>
                    <View
                      style={{
                        height: '100%',
                        alignItems: 'center',
                        // paddingVertical: moderateVerticalScale(10),
                      }}>
                      <Text
                        style={{
                          color: colors.white,
                          fontSize: textScale(16),
                          fontFamily: fontFamily?.bold,
                          position: 'absolute',
                          zIndex: 100,
                        }}>
                        {item?.name}
                      </Text>
                      <Image
                        style={{
                          height: '100%',
                          width: '100%',
                        }}
                        source={{
                          uri: getImageUrl(
                            item?.icon?.image_fit,
                            item?.icon?.image_path,
                            '1000/1000',
                          ),
                        }}
                      />
                    </View>
                  </TouchableOpacity>
                );
              }}
              showsHorizontalScrollIndicator={false}
              onEndReached={onEndReachedDelayed}
              onEndReachedThreshold={0.5}
            />
          )}
          //   renderSectionHeader={({section: {title}}) => (
          //     <Text style={styles.header}>{title}</Text>
          //   )}

          renderSectionFooter={({section}) => {
            return (
              section?.id === selectedIndexForExpendView && (
                <View
                  style={{
                    backgroundColor: colors.white,
                    borderRadius: 10,
                    overflow: 'hidden',
                    shadowColor: colors.black,
                    shadowOffset: {width: 0, height: 0.5},
                    shadowOpacity: 0.8,
                    shadowRadius: 1,
                    elevation: 5,
                    alignContent: 'center',
                    width: moderateScale(width - 40),
                    alignSelf: 'center',
                    marginVertical: moderateVerticalScale(10),
                    paddingHorizontal: moderateScale(10),
                    paddingVertical: moderateVerticalScale(10),
                  }}>
                  <FlatList
                    data={allSubCategories}
                    renderItem={({item, index}) => (
                      <TouchableOpacity
                        style={{
                          marginHorizontal: moderateScale(5),
                          marginTop: moderateVerticalScale(10),
                          alignItems: 'center',
                          flexDirection: 'row',
                          borderRadius: moderateScale(5),
                        }}
                        onPress={() =>
                          _moveToNextScreen(
                            navigationStrings.SUBCATEGORIES,
                            item,
                          )
                        }>
                        <Image
                          style={{
                            height: moderateVerticalScale(40),
                            width: moderateScale(40),
                            borderRadius: moderateScale(20),
                          }}
                          source={{
                            uri: getImageUrl(
                              item?.icon?.image_fit,
                              item?.icon?.image_path,
                              '1000/1000',
                            ),
                          }}
                        />
                        <Text
                          style={{
                            marginHorizontal: moderateScale(10),
                            color: colors.black,
                            fontSize: textScale(16),
                            fontFamily: fontFamily?.bold,
                          }}>
                          {item?.name}
                        </Text>
                      </TouchableOpacity>
                    )}
                    showsHorizontalScrollIndicator={false}
                  />
                </View>
              )
            );
          }}
          ListFooterComponent={() => (
            <View style={{height: moderateVerticalScale(100)}} />
          )}
        />
      </View>
    </WrapperContainer>
  );
}
