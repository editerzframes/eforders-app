import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  ImageBackground,
  Platform,
  Image,
} from 'react-native';
import {FlatListSlider} from 'react-native-flatlist-slider';

import {
  height,
  moderateScale,
  moderateVerticalScale,
  width,
} from '../styles/responsiveSize';
import colors from '../styles/colors';
import {useSelector} from 'react-redux';
import {getImageUrl} from '../utils/helperFunctions';

const Preview = ({item, imageKey, onPress, resizeMode = 'stretch'}) => {
  const imageUrl = item?.image
    ? getImageUrl(item.image.image_fit, item.image.image_path, '1000/1000')
    : getImageUrl(item.image.image_fit, item.image.image_path, '1000/1000');

  return (
    <TouchableOpacity onPress={() => onPress(item)}>
      <View style={[styles.imageContainer]}>
        <Image
          style={{
            height: moderateVerticalScale(height / 4.5),
            width: width,
            resizeMode: resizeMode,
          }}
          source={{uri: imageUrl}}
        />
      </View>
    </TouchableOpacity>
  );
};

function Crousel({
  bannerData = {},
  cardViewStyle = {},
  indicator = true,
  autoscroll = true,
}) {
  const {ThemeColors, fontFamily} = useSelector(
    state => state?.init?.initAppData,
  );

  return (
    <View
      style={{
        alignItems: 'center',
        height: moderateVerticalScale(height / 4.5),
        width: width,
        alignContent: 'center',
      }}>
      {bannerData?.length && (
        <FlatListSlider
          data={bannerData}
          onPress={item => {}}
          component={<Preview />}
          indicator={bannerData?.length > 1 ? indicator : false}
          indicatorContainerStyle={styles.indecatorViewStyle}
          indicatorActiveColor={ThemeColors?.primary_color}
          indicatorInActiveColor={colors.white}
          indicatorActiveWidth={7}
          animation
          autoscroll={autoscroll}
          imageKey={'image'}
        />
      )}
    </View>
  );
}

const styles = StyleSheet.create({
  imageStyle: {
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
  },
  dotStyle: {
    height: 12,
    width: 12,
    borderRadius: 12 / 2,
  },
  imageContainer: {
    height: moderateVerticalScale(80),
    width: width,
    alignItems: 'center',
  },

  indecatorViewStyle: {
    position: 'absolute',
    bottom: moderateVerticalScale(35),
    elevation: 5,
    shadowRadius: 12,
    end: 20,
  },
});

export default Crousel;
