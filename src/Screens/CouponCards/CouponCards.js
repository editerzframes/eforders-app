import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  TextInput,
  FlatList,
  Image,
  TouchableOpacity,
} from 'react-native';
import {useSelector} from 'react-redux';
import Header from '../../Components/Header';
import TextInputWithLabel from '../../Components/TextInputWithLabel';
import WrapperContainer from '../../Components/WrapperContainer';
import imagePath from '../../constants/imagePath';
import strings from '../../constants/lang';
import colors from '../../styles/colors';
import {
  height,
  moderateScale,
  moderateVerticalScale,
  textScale,
  width,
} from '../../styles/responsiveSize';
import {
  getColorCodeWithOpactiyNumber,
  getImageUrl,
} from '../../utils/helperFunctions';
import {useFocusEffect} from '@react-navigation/native';
import actions from '../../redux/actions';
import navigationStrings from '../../constants/navigationStrings';
import {showMessage} from 'react-native-flash-message';

export default function CouponCards(props) {
  const {navigation} = props;
  const [state, setstate] = useState({
    allCoupons: [],
    couponName: '',
  });

  const {allCoupons, couponName} = state;

  const updateState = data => setstate(state => ({...state, ...data}));
  const {ThemeColors, fontFamily} = useSelector(
    state => state?.init?.initAppData,
  );
  const logedInUser = useSelector(state => state?.auth?.userData);

  useFocusEffect(
    React.useCallback(() => {
      if (logedInUser?.access_token) {
        onAllCouponsList();
      }
    }, []),
  );

  useEffect(() => {
    if (couponName?.length != '') {
      onSearchCoupon();
    } else {
      onAllCouponsList();

      updateState({
        couponName: '',
      });
    }
  }, [couponName]);

  const onSearchCoupon = () => {
    let query = `/${couponName}`;

    actions
      .SearchCounpon(query)
      .then(res => {
        console.log(res, 'all coupons Methods');
        updateState({
          allCoupons: res?.data,
        });
      })
      .catch(error => {
        showMessage({
          message: error.message,
          type: 'danger',
        });
      });
  };

  // list all coupon code
  const onAllCouponsList = () => {
    actions
      .couponsListing()
      .then(res => {
        console.log(res, 'all coupons Methods +++++');
        updateState({
          allCoupons: res?.data,
        });
        // actions.showCartBadge(res?.data?.total_quantity);
      })
      .catch(error => {
        showMessage({
          message: error.message,
          type: 'danger',
        });
      });
  };

  const _moveToNextScreen = (redircetOn, data) => {
    navigation.navigate(redircetOn, data);
  };

  const _onChangeText = key => val => {
    updateState({[key]: val});
  };

  return (
    <WrapperContainer bgColor={colors.white}>
      <View style={{paddingHorizontal: moderateScale(10)}}>
        <Header leftIcon={imagePath?.back} textStyle={{color: colors.black}} />
      </View>
      <View
        style={{marginHorizontal: moderateScale(10), justifyContent: 'center'}}>
        <TextInput
          placeholder={'Enter coupon code'}
          style={{
            marginVertical: moderateVerticalScale(10),
            borderBottomColor: colors.textGreyLight,
            borderBottomWidth: 0.5,
            height: moderateVerticalScale(40),
            fontSize: textScale(12),
          }}
          onChangeText={_onChangeText('couponName')}
        />
        <Text
          style={{
            position: 'absolute',
            alignSelf: 'flex-end',
            color: ThemeColors?.primary_color,
          }}>
          APPLY
        </Text>
      </View>
      <View>
        <FlatList
          data={allCoupons}
          showsVerticalScrollIndicator={false}
          keyExtractor={(item, index) => String(index)}
          style={{
            backgroundColor: colors.backgroundColor,
          }}
          ListHeaderComponent={() => {
            return (
              <View
                style={{
                  marginVertical: moderateVerticalScale(10),
                  marginHorizontal: moderateScale(10),
                }}>
                {allCoupons?.length ? (
                  <Text
                    style={{
                      fontFamily: fontFamily.bold,
                      color: colors.black,
                      fontSize: textScale(16),
                    }}>
                    Available Coupons
                  </Text>
                ) : null}
              </View>
            );
          }}
          renderItem={({item, index}) => {
            return (
              <View
                style={{
                  width: width - 20,
                  overflow: 'hidden',
                  shadowColor: colors.black,
                  shadowOffset: {width: 0, height: 1},
                  shadowOpacity: 0.8,
                  shadowRadius: 2,
                  elevation: 5,
                  backgroundColor: colors.white,
                  borderRadius: 5,
                  alignSelf: 'center',
                  paddingHorizontal: moderateScale(10),
                }}>
                <View style={{height: moderateVerticalScale(55)}}>
                  <Image
                    style={{
                      height: moderateVerticalScale(50),
                      width: moderateScale(50),
                      marginVertical: moderateVerticalScale(10),
                    }}
                    source={{
                      uri: getImageUrl(
                        item.icon.image_fit,
                        item.icon.image_path,
                        '200/200',
                      ),
                    }}></Image>
                </View>
                <View>
                  <Text
                    style={{
                      fontSize: textScale(14),
                      color: colors.textGreyLight,
                      fontFamily: fontFamily.bold,
                    }}>
                    60% of up to 100
                  </Text>
                  <Text
                    style={{
                      fontSize: textScale(12),
                      color: colors.textGreyLight,
                      fontFamily: fontFamily.regular,
                    }}>
                    {item?.description}
                  </Text>
                </View>
                <View
                  style={{
                    marginTop: moderateVerticalScale(10),
                    paddingVertical: moderateVerticalScale(20),
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    alignItems: 'center',
                    borderTopColor: colors.lightGreyBg,
                    borderBottomColor: colors.lightGreyBg,
                    borderTopWidth: 0.7,
                    borderBottomWidth: 0.7,
                  }}>
                  <View
                    style={{
                      paddingHorizontal: moderateScale(10),
                      paddingVertical: moderateVerticalScale(10),
                      borderStyle: 'dashed',
                      borderWidth: 1,
                      backgroundColor: colors.lightSky,
                      borderColor: getColorCodeWithOpactiyNumber(
                        colors.lightSky.substr(1),
                        30,
                      ),
                    }}>
                    <Text
                      style={{textTransform: 'uppercase', color: colors.black}}>
                      {item?.name}
                    </Text>
                  </View>
                  <TouchableOpacity
                    onPress={() =>
                      _moveToNextScreen(navigationStrings.CHECKOUT, item)
                    }>
                    <Text style={{color: colors.red}}>APPLY</Text>
                  </TouchableOpacity>
                </View>
                <View>
                  <Text
                    style={{
                      fontSize: textScale(10),
                      color: colors.textBlue,
                      fontFamily: fontFamily.regular,
                      marginVertical: moderateVerticalScale(5),
                    }}>
                    {` Valid on order min ${item?.min_amount}`}
                  </Text>
                </View>
              </View>
            );
          }}
          ItemSeparatorComponent={() => {
            return (
              <View
                style={{
                  height: moderateVerticalScale(20),
                }}
              />
            );
          }}
          ListEmptyComponent={() => {
            return (
              <View
                style={{
                  height: height / 1.8,
                  width: width,
                  alignItems: 'center',
                }}></View>
            );
          }}
          ListFooterComponent={() => {
            return (
              <View
                style={{
                  height: moderateVerticalScale(100),
                }}></View>
            );
          }}
        />
      </View>
    </WrapperContainer>
  );
}
