import React from 'react';
import {View, Text, Image, TouchableOpacity, StyleSheet} from 'react-native';
import {
  height,
  moderateScale,
  moderateVerticalScale,
  textScale,
  width,
} from '../styles/responsiveSize';

import colors from '../styles/colors';
import imagePath from '../constants/imagePath';
import fontFamily from '../styles/fontFamily';
import {
  getColorCodeWithOpactiyNumber,
  getImageUrl,
} from '../utils/helperFunctions';
import {useSelector} from 'react-redux';

export default function SmallCard({data, onPress}) {
  const {ThemeColors, fontFamily} = useSelector(
    state => state?.init?.initAppData,
  );
  const styles = stylesFunc({fontFamily, ThemeColors});
  return (
    <TouchableOpacity onPress={() => onPress(data)}>
      <View style={styles.rootContainer}>
        <Image
          source={{
            uri: getImageUrl(
              data?.image?.image_fit,
              data?.image?.image_path,
              '1000/1000',
            ),
          }}
          style={styles.imageStyle}
          resizeMode="cover"
        />
        <Text style={styles.textStyle}>{data.name}</Text>
      </View>
    </TouchableOpacity>
  );
}
export function stylesFunc({fontFamily, ThemeColors}) {
  const styles = StyleSheet.create({
    rootContainer: {
      marginHorizontal: moderateScale(10),
      alignItems: 'center',
      flexDirection: 'row',
      backgroundColor: getColorCodeWithOpactiyNumber(
        ThemeColors?.primary_color.substr(1),
        70,
      ),
      paddingVertical: moderateVerticalScale(5),
      paddingHorizontal: moderateScale(10),
      borderRadius: 8,
      marginVertical: moderateVerticalScale(10),
    },
    imageStyle: {
      width: moderateScale(30),
      height: moderateVerticalScale(30),
      borderRadius: 35,
    },

    textStyle: {
      marginLeft: moderateScale(10),
      fontSize: textScale(12),
      fontFamily: fontFamily?.regular,
      color: colors.white,
    },
  });
  return styles;
}
