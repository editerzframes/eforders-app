import React, {useState} from 'react';
import {View, Text, FlatList, Image, TouchableOpacity} from 'react-native';
import Header from '../../Components/Header';
import WrapperContainer from '../../Components/WrapperContainer';
import imagePath from '../../constants/imagePath';
import strings, {changeLaguage} from '../../constants/lang';
import colors from '../../styles/colors';
import {
  height,
  moderateScale,
  moderateVerticalScale,
  textScale,
  width,
} from '../../styles/responsiveSize';
import {getColorCodeWithOpactiyNumber} from '../../utils/helperFunctions';
import DropDownPicker from 'react-native-dropdown-picker';
import fontFamily from '../../styles/fontFamily';
import actions from '../../redux/actions';
import {useFocusEffect} from '@react-navigation/native';
import ModalView from '../../Components/Modal';
import {setItem} from '../../utils/utils';
import RNRestart from 'react-native-restart';
import {useSelector} from 'react-redux';

export default function Settings() {
  const primaryLanguage = useSelector(state => state?.home?.primaryLanguage);
  console.log(primaryLanguage, 'primaryLanguage');
  const [state, setState] = useState({
    allLanguages: [],
    isLanguageModalVisible: false,
    selectedAppLanguage: primaryLanguage?.name
      ? primaryLanguage
      : {id: 2, code: 'ee', name: 'English'},
  });

  const {allLanguages, isLanguageModalVisible, selectedAppLanguage} = state;
  const updateState = data => setState(state => ({...state, ...data}));
  const {ThemeColors, fontFamily} = useSelector(
    state => state?.init?.initAppData,
  );

  useFocusEffect(
    React.useCallback(() => {
      alllanguageList();
    }, []),
  );

  const alllanguageList = () => {
    actions
      .languageList()
      .then(res => {
        updateState({
          allLanguages: res?.data,
        });
      })
      .catch(error => {
        showMessage({
          message: error.message,
          type: 'danger',
        });
      });
  };

  const _onSetPrimaryLanguage = item => {
    console.log(selectedAppLanguage, 'selectedAppLanguage');

    setItem('language', selectedAppLanguage);
    changeLaguage(selectedAppLanguage?.code);
    RNRestart.Restart();
  };

  const _onSelectLanguage = item => {
    console.log(item, 'itemitemitemitem');
    updateState({
      selectedAppLanguage: item,
    });
  };

  const modalMainContent = () => {
    return (
      <View style={{height: height / 1.85}}>
        <FlatList
          data={allLanguages}
          ItemSeparatorComponent={() => {
            return <View style={{width: moderateVerticalScale(10)}} />;
          }}
          renderItem={({item, index}) => {
            return (
              <View
                style={{
                  flexDirection: 'row',
                  marginVertical: moderateVerticalScale(14),
                }}>
                <TouchableOpacity
                  style={{
                    flexDirection: 'row',
                  }}
                  onPress={() => _onSelectLanguage(item)}>
                  <Image
                    style={{marginHorizontal: moderateScale(20)}}
                    source={
                      selectedAppLanguage?.id == item?.id
                        ? imagePath.activeRadio
                        : imagePath.inActiveRadio
                    }
                  />
                  <Text
                    style={{
                      fontFamily: fontFamily.regular,
                      fontSize: textScale(16),
                    }}>
                    {item?.name}
                  </Text>
                </TouchableOpacity>
              </View>
            );
          }}
          showsHorizontalScrollIndicator={false}
        />
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            // marginVertical: moderateVerticalScale(10),
            borderTopWidth: 0.5,
            borderColor: colors.textGreyLight,
          }}>
          <View></View>
          <View
            style={{
              flexDirection: 'row',
              marginVertical: moderateVerticalScale(10),
            }}>
            <TouchableOpacity
              onPress={() =>
                updateState({
                  isLanguageModalVisible: false,
                })
              }>
              <Text
                style={{
                  fontSize: textScale(16),
                  color: ThemeColors?.primary_color,
                }}>
                Cancel
              </Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => _onSetPrimaryLanguage()}>
              <Text
                style={{
                  marginHorizontal: moderateScale(30),
                  fontSize: textScale(16),
                  color: ThemeColors?.primary_color,
                }}>
                Ok
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  };

  return (
    <WrapperContainer bgColor={colors.white} statusBarColor={colors.white}>
      <View style={{paddingHorizontal: moderateScale(10)}}>
        <Header
          leftIcon={imagePath.back}
          textStyle={{color: colors.black}}
          centerTitle={strings.SETTINGS}
        />
      </View>
      <TouchableOpacity
        style={{
          marginHorizontal: moderateScale(20),
          flexDirection: 'row',
          alignItems: 'center',
          backgroundColor: colors.white,
          shadowColor: colors.black,
          shadowOffset: {width: 2, height: 2},
          shadowOpacity: 1,
          shadowRadius: 5,
          elevation: 7,
          paddingVertical: moderateVerticalScale(5),
          marginVertical: moderateVerticalScale(20),
          justifyContent: 'space-between',
          paddingHorizontal: moderateScale(15),
        }}
        onPress={() =>
          updateState({
            isLanguageModalVisible: true,
          })
        }>
        <Text
          style={{
            marginVertical: moderateVerticalScale(10),
            fontSize: textScale(15),
            fontFamily: fontFamily.bold,
            color: colors.black,
          }}>
          {strings.LANGUAGE}
        </Text>
        <Text
          style={{
            color: ThemeColors?.primary_color,
            fontFamily: fontFamily.regular,
          }}>
          {primaryLanguage?.name ? primaryLanguage?.name : 'English'}
        </Text>
      </TouchableOpacity>
      <ModalView
        mainViewStyle={{
          borderBottomLeftRadius: 15,
          borderBottomRightRadius: 15,
          height: height / 1.7,
        }}
        isVisible={isLanguageModalVisible}
        modalMainContent={modalMainContent}
      />
    </WrapperContainer>
  );
}
