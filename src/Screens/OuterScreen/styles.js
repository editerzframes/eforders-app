import {StyleSheet} from 'react-native';
import colors from '../../styles/colors';
import fontFamily from '../../styles/fontFamily';
import {
  height,
  moderateScale,
  moderateVerticalScale,
  textScale,
  width,
} from '../../styles/responsiveSize';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.backgroundColor,
  },
  topViewContainer: {flex: 0.25, alignItems: 'center'
},

  welcomeTextStyle: {
    fontSize: textScale(32),
    fontFamily: fontFamily.semiBold,
    textAlign: 'center',
    marginHorizontal: moderateScale(20),
  
  },
  outerScreenBannerImage: {
    flex: 0.47,
    justifyContent: 'center',
    alignItems: 'center',
   
  },
  buttonContainer: {
    flex: 0.12,
  },
  buttonInnerViewStyle: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,

  },
  btnTextContainer: {flex: 1, justifyContent: 'center', alignItems: 'center'},

  btnTextStyle: {
    color: colors.white,
    textAlign: 'center',
    fontSize: textScale(24),
    fontFamily: fontFamily.semiBold,
    marginTop: moderateVerticalScale(15),
  },
  orTextWithLineStyle: {
    flex: 0.05,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    
  },
  lineStyle: {
    width: width / 2.5,
    height: 2,
    backgroundColor: colors.white,
  },
  orTextStyle: {
    fontSize: textScale(20),
    fontFamily: fontFamily.regular,
    color: colors.textGrey,
    marginHorizontal: moderateScale(10),
  },
  continueAsGuestContainer: {
    flex: 0.1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  guestTextContainer: {
    flex: 1,
   
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: moderateVerticalScale(15),
  },
  continueTextStyle: {
    color: colors.black,
    textAlign: 'center',
    fontSize: textScale(22),
    fontFamily: fontFamily.semiBold,
  },
});
