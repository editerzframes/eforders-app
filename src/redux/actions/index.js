import * as auth from './auth';
import * as home from './home';
import * as init from './init';
import * as cart from './cart';

export default {
  ...auth,
  ...home,
  ...init,
  ...cart,
};
