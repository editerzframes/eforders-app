import {apiGet, apiPost, clearUserData, setUserData} from '../../utils/utils';
import {
  SIGNUP,
  LOGIN,
  HOME,
  SUBCATEGORIES,
  PRODUCTLISTING,
  PRODUCTDETAILS,
  RELATEDPRODUCTS,
  SEARCH,
  VERIFYPHONE,
  SAVEUSERINFO,
  UPDATEUSER,
  VERIFYEMAIL,
  VERIFYMOBILE,
  ISUSERVERIFIED,
  REQUEST_OTP,
  SOCIAL_LOGIN,
  REQUESTOTPFORPASSWORD,
  VERIFYOTPFORPASSWORD,
  FORGOTPASSWORD,
} from '../../config/urls';
import ActionType from '../types';
import store from '../store';

export function register(data = {}) {
  return new Promise((resolve, reject) => {
    apiPost(SIGNUP, data)
      .then(res => {
        setUserData(res.data)
          .then(
            suc =>
              store.dispatch({
                type: ActionType.LOGIN,
                payload: res.data,
              }),

            resolve(res),
          )
          .catch(error => console.log(error, 'error in save data'));
      })
      .catch(error => {
        reject(error);
      });
  });
}

export function login(data = {}) {
  return new Promise((resolve, reject) => {
    apiPost(LOGIN, data)
      .then(res => {
        setUserData(res.data)
          .then(
            suc =>
              store.dispatch({
                type: ActionType.LOGIN,
                payload: res.data,
              }),
            resolve(res),
          )
          .catch(error => console.log(error, 'error in save data'));
      })
      .catch(error => {
        reject(error);
      });
  });
}

export function requestOtpForForgotPassword(data = {}) {
  return new Promise((resolve, reject) => {
    apiPost(REQUESTOTPFORPASSWORD, data)
      .then(res => {
        resolve(res);
      })
      .catch(error => {
        reject(error);
      });
  });
}

export function verifyOtpForForgotPassword(data = {}) {
  return new Promise((resolve, reject) => {
    apiPost(VERIFYOTPFORPASSWORD, data)
      .then(res => {
        resolve(res);
      })
      .catch(error => {
        reject(error);
      });
  });
}

export function forgotPassword(data = {}) {
  return new Promise((resolve, reject) => {
    apiPost(FORGOTPASSWORD, data)
      .then(res => {
        resolve(res);
      })
      .catch(error => {
        reject(error);
      });
  });
}

export function verifyEmail(data = {}) {
  return new Promise((resolve, reject) => {
    apiPost(VERIFYEMAIL, data)
      .then(res => {
        resolve(res);
      })
      .catch(error => {
        reject(error);
      });
  });
}

export function checkUserVerification(headers = {}, data = {}) {
  return new Promise((resolve, reject) => {
    apiGet(ISUSERVERIFIED, data, headers)
      .then(res => {
        resolve(res);
      })

      .catch(error => {
        reject(error);
      });
  });
}

export function verifyMobile(data = {}, isVerifyEmail, isVerifyMobile) {
  return new Promise((resolve, reject) => {
    apiPost(VERIFYMOBILE, data)
      .then(res => {
        resolve(res);
      })

      .catch(error => {
        reject(error);
      });
  });
}

export function requestOtp(data = {}) {
  return new Promise((resolve, reject) => {
    apiPost(REQUEST_OTP, data)
      .then(res => {
        resolve(res);
      })
      .catch(error => {
        reject(error);
      });
  });
}

export function verifyOtp(data = {}) {
  return new Promise((resolve, reject) => {
    apiPost(VERIFY_OTP, data)
      .then(res => {
        resolve(res);
      })
      .catch(error => {
        reject(error);
      });
  });
}

export function resetPassword(data = {}) {
  return new Promise((resolve, reject) => {
    apiPost(RESET_PASSWORD, data)
      .then(res => {
        resolve(res);
      })
      .catch(error => {
        reject(error);
      });
  });
}

export function verifyPhoneNumber(data = {}) {
  return new Promise((resolve, reject) => {
    apiPost(VERIFYPHONE, data)
      .then(res => {
        resolve(res);
      })
      .catch(error => {
        reject(error);
      });
  });
}

export function saveUserInfo(headers = {}, data = {}) {
  return new Promise((resolve, reject) => {
    apiPost(SAVEUSERINFO, data, headers)
      .then(res => {
        resolve(res);
      })
      .catch(error => {
        reject(error);
      });
  });
}

export const onLogout = () => {
  store.dispatch({
    type: ActionType.LOGOUT,
    payload: {},
  });
  clearUserData();
};

export function registerUserByPhone(data = {}) {
  return new Promise((resolve, reject) => {
    setUserData(data)
      .then(
        suc => resolve(suc),
        store.dispatch({
          type: ActionType.LOGIN,
          payload: data,
        }),
      )
      .catch(error => console.log(error, 'error in save data'));
  });
}

export function upadateUser(headers = {}, data = {}) {
  return new Promise((resolve, reject) => {
    apiPost(UPDATEUSER, data, headers)
      .then(res => {
        resolve(res);
      })
      .catch(error => {
        reject(error);
      });
  });
}

export function updateProfile(res) {
  setUserData(res).then(suc => {
    store.dispatch({
      type: ActionType.LOGIN,
      payload: res,
    });
  });
}

export function socialLogin(data = {}) {
  return new Promise((resolve, reject) => {
    apiPost(SOCIAL_LOGIN, data)
      .then(res => {
        console.log(res, 'response in signup');
        setUserData(res.data)
          .then(
            suc =>
              store.dispatch({
                type: ActionType.LOGIN,
                payload: res.data,
              }),
            resolve(res),
          )
          .catch(error => console.log(error, 'error in save data'));
        console.log(res, 'res');
      })

      .catch(error => {
        reject(error);
      });
  });
}
