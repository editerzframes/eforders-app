import React from 'react';
import {StyleSheet} from 'react-native';
import colors from '../../styles/colors';
import commonStyles from '../../styles/commonStyles';
import fontFamily from '../../styles/fontFamily';
import {
  height,
  moderateScale,
  moderateVerticalScale,
  textScale,
  width,
} from '../../styles/responsiveSize';

export function stylesFunc({fontFamily, ThemeColors}) {
  const styles = StyleSheet.create({
    headerContiner: {
      paddingVertical: moderateVerticalScale(10),
    },
    screenTitleTextStyle: {
      fontFamily: fontFamily?.regular,
      fontSize: textScale(16),
      marginHorizontal: moderateScale(20),
      marginVertical: moderateVerticalScale(20),
      textAlign: 'center',
    },
    socialHeadingTextStyle: {
      fontFamily: fontFamily?.bold,
      fontSize: textScale(16),
      marginHorizontal: moderateScale(20),
    },
    socialIconContainer: {
      flexDirection: 'row',
      marginHorizontal: moderateScale(25),
      justifyContent: 'space-between',
    },
    socialIconInnerContainer: {
      flexDirection: 'row',
      borderColor: colors.textGreyLight,
      borderWidth: 0.5,
      width: width / 2.6,
      justifyContent: 'center',
      alignItems: 'center',
      borderRadius: 5,
      paddingVertical: moderateVerticalScale(10),
    },
    descriptionTextStyle: {
      fontSize: textScale(14),
      fontFamily: fontFamily?.medium,
      textAlign: 'center',
    },
    forgotPasswordText: {
      marginVertical: moderateVerticalScale(10),
      textAlign: 'center',
      fontFamily: fontFamily?.medium,
      fontSize: textScale(14),
      color: ThemeColors?.primary_color,
    },
    createNewAccountText: {
      fontFamily: fontFamily?.medium,
      fontSize: textScale(14),
    },
    signUpTextStyle: {
      marginHorizontal: moderateScale(8),
      fontFamily: fontFamily?.medium,
      fontSize: textScale(14),
      color: ThemeColors?.primary_color,
    },
    root: {marginTop: 20, alignItems: 'center'},
    cell: {
      width: textScale(48),
      height: textScale(48),
      fontFamily: fontFamily?.regular,
      lineHeight: textScale(47),
      fontSize: 24,
      borderWidth: 2,
      borderRadius: textScale(8),
      borderColor: colors.black,
      color: colors.black,
      textAlign: 'center',

      marginBottom: moderateVerticalScale(20),
    },
    focusCell: {
      borderColor: ThemeColors?.primary_color,
    },
  });
  return styles;
}
