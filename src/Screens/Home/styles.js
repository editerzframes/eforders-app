import {StyleSheet} from 'react-native';
import colors from '../../styles/colors';

import {
  height,
  moderateScale,
  moderateVerticalScale,
  textScale,
  width,
} from '../../styles/responsiveSize';
import {getColorCodeWithOpactiyNumber} from '../../utils/helperFunctions';

export function stylesFunc({fontFamily, ThemeColors}) {
  const styles = StyleSheet.create({
    mainContainer: {
      flex: 1,
    },
    headerContainer: {
      justifyContent: 'center',
      marginHorizontal: moderateScale(10),
    },
    locationContainer: {
      borderWidth: 0.8,
      borderColor: colors.textGreyLight,

      borderRadius: 5,
      marginHorizontal: moderateScale(10),
    },
    loactionInnerContainer: {
      flexDirection: 'row',
      alignItems: 'center',
      // flex: 1,
      paddingHorizontal: moderateScale(10),
    },
    locationTextStyle: {
      color: colors.blackOpacity86,
      fontSize: textScale(12),
      marginHorizontal: moderateScale(10),
      fontFamily: fontFamily?.regular,
      width: width / 1.7,
    },
    mainHomeScreenDataContainer: {
      paddingTop: moderateVerticalScale(10),
    },
    sectionHeader: {
      marginHorizontal: moderateScale(10),
      fontFamily: fontFamily?.bold,
      color: colors.black,
      fontSize: textScale(16),
    },
    viewAllTextStyle: {
      marginHorizontal: moderateScale(10),
      fontSize: textScale(14),
      fontFamily: fontFamily?.bold,
      color: ThemeColors?.primary_color,
    },
    searchBoxStyle: {
      height: moderateVerticalScale(45),
      backgroundColor: colors.transparent,
      paddingHorizontal: moderateScale(15),
      fontFamily: fontFamily?.regular,
    },
    closeModalImage: {
      tintColor: colors.black,
      alignSelf: 'flex-end',
      marginTop: moderateVerticalScale(-10),
      marginRight: moderateScale(20),
    },
    chooseAddressText: {
      fontSize: textScale(16),
      fontFamily: fontFamily?.bold,
    },

    selectDeliveryLocationText: {
      fontSize: textScale(13),
      fontFamily: fontFamily?.regular,
      marginVertical: moderateVerticalScale(10),
      textAlign: 'center',
    },
    addnewAddressView: {
      height: moderateVerticalScale(150),
      width: moderateScale(150),
      borderColor: colors.black,
      borderWidth: 0.5,
      marginHorizontal: moderateScale(10),
      justifyContent: 'center',
      alignItems: 'center',
    },
    addnewaddresstext: {
      fontSize: textScale(14),
      fontFamily: fontFamily?.bold,
      color: colors.btnABlue,
      textAlign: 'center',
    },
  });
  return styles;
}
