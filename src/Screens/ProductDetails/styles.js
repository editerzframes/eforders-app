import {StyleSheet} from 'react-native';
import colors from '../../styles/colors';

import {
  height,
  moderateScale,
  moderateVerticalScale,
  textScale,
  width,
} from '../../styles/responsiveSize';

export function stylesFunc({fontFamily, ThemeColors}) {
  const styles = StyleSheet.create({
    mainContainer: {
      flex: 1,
    },
    headerContainer: {
      height: moderateVerticalScale(50),
      justifyContent: 'center',
      marginHorizontal: moderateScale(10),
    },
    productImageContainer: {
      flex: 0.55,
    },
    productInfoContainer: {
      paddingHorizontal: moderateScale(20),
    },
    productNameTextStyle: {
      fontSize: textScale(16),
      fontFamily: fontFamily.bold,
      color: colors.black,
      marginTop: moderateVerticalScale(3),
    },
    priceContainer: {
      flexDirection: 'row',
      marginVertical: moderateVerticalScale(10),
      justifyContent: 'space-between',
      alignItems: 'center',
    },
    priceContainer1: {
      flexDirection: 'row',
    },
    productPriceTextStyle: {
      fontSize: textScale(14),
      fontFamily: fontFamily.medium,
      color: colors.black,
    },
    productPriceTextStyle1: {
      fontSize: textScale(14),
      fontFamily: fontFamily.bold,
      color: colors.black,
    },
    productCartIncrimentDecrimentViewContainer: {
      flexDirection: 'row',
    },
    bottombtnContainer: {
      position: 'absolute',
      bottom: 0,
      height: moderateVerticalScale(70),
      width: width,
      flexDirection: 'row',
      justifyContent: 'space-evenly',
      backgroundColor: colors.white,
      alignItems: 'center',
    },
    cartButtonStyle: {
      width: width / 5,
      height: moderateVerticalScale(50),
      backgroundColor: colors.textGreyLight,
      borderRadius: 8,
      justifyContent: 'center',
      alignItems: 'center',
    },
    buyNowButtonStyle: {
      width: width / 1.5,
      height: moderateVerticalScale(50),
      backgroundColor: ThemeColors?.primary_color,
      borderRadius: 8,
      justifyContent: 'center',
      alignItems: 'center',
    },
    cartBtnTextStyle: {
      fontSize: textScale(16),
      color: colors.black,
    },
    productColorSizeViewContainer: {
      flexDirection: 'row',
      justifyContent: 'space-between',
    },
    productColorSizeInnerContainer: {
      justifyContent: 'center',
      alignItems: 'center',
    },
    productColorSizeLabelTextStyle: {
      fontFamily: fontFamily.bold,
      color: colors.black,
      fontSize: textScale(16),
    },
    cartIconStyle: {
      backgroundColor: colors.red,
      position: 'absolute',
      zIndex: 1,
      paddingHorizontal: 4,
      paddingVertical: 2,
      borderRadius: 50,
      alignItems: 'center',
      justifyContent: 'center',
      right: 30,
      top: 6,
    },
    wishlistIconStyle: {
      position: 'absolute',
      zIndex: 1,
      end: 20,
      top: 20,
      backgroundColor: colors.white,
      paddingHorizontal: moderateScale(3),
      paddingVertical: moderateVerticalScale(3),
      borderRadius: 20,
    },
    ratingContainerView: {flexDirection: 'row', alignItems: 'center'},
  });
  return styles;
}
