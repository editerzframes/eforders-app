import ActionTypes from '../types';

const initialState = {
  initAppData: {},
};
export default function init(state = initialState, action) {
  switch (action.type) {
    case ActionTypes.INITAPP:
      const initAppData = action.payload;

      return {
        ...state,
        initAppData: initAppData,
      };

    default:
      return state;
  }
}
