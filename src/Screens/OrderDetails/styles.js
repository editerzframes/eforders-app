import React from 'react';
import {StyleSheet} from 'react-native';
import colors from '../../styles/colors';
import commonStyles from '../../styles/commonStyles';
import fontFamily from '../../styles/fontFamily';
import {
  height,
  moderateScale,
  moderateVerticalScale,
  textScale,
  width,
} from '../../styles/responsiveSize';
import {getColorCodeWithOpactiyNumber} from '../../utils/helperFunctions';

export function stylesFunc({fontFamily, ThemeColors}) {
  const styles = StyleSheet.create({
    map: {
      ...StyleSheet.absoluteFillObject,
    },
    leftTextStyle: {
      fontSize: textScale(12),
      fontFamily: fontFamily?.bold,
      color: colors.black,
    },
    rightTextStyle: {
      fontSize: textScale(12),
      fontFamily: fontFamily?.bold,
    },
    productImagesStyle: {
      height: moderateVerticalScale(50),
      width: moderateScale(50),
      borderRadius: 25,
      marginHorizontal: moderateScale(10),
    },
    productQuantityText: {
      fontFamily: fontFamily?.medium,
      marginVertical: moderateVerticalScale(5),
      fontSize: textScale(12),
      backgroundColor: colors.green,
      borderRadius: 10,
      paddingHorizontal: moderateScale(4),
      paddingVertical: moderateVerticalScale(2),
      position: 'absolute',
      right: 5,
      top: -5,
      color: colors.white,
    },
    orderStatusContainer: {
      width: width,
      paddingVertical: moderateVerticalScale(10),
      backgroundColor: getColorCodeWithOpactiyNumber(
        colors.lightGreyBg.substr(1),
        40,
      ),
    },
    orderStatusTitle: {
      fontSize: textScale(16),
      fontFamily: fontFamily?.bold,
      marginVertical: moderateVerticalScale(10),
      marginHorizontal: moderateScale(10),
    },
    orderstatusStepContainer: {
      backgroundColor: colors.white,
      paddingVertical: moderateVerticalScale(10),
    },
    productDetailsContainer: {
      width: width,
      backgroundColor: getColorCodeWithOpactiyNumber(
        colors.lightGreyBg.substr(1),
        40,
      ),
    },
    productDetailsTitle: {
      fontSize: textScale(16),
      fontFamily: fontFamily?.bold,
      marginVertical: moderateVerticalScale(5),
      marginHorizontal: moderateScale(10),
    },
    orderSummerText: {
      fontSize: textScale(16),
      fontFamily: fontFamily?.bold,
      marginVertical: moderateVerticalScale(10),
      marginHorizontal: moderateScale(10),
    },
  });
  return styles;
}
