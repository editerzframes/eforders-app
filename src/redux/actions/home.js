import {apiGet, apiPost, primaryAddress, setUserData} from '../../utils/utils';
import ActionType from '../types';
import store from '../store';
import {
  HOME,
  PRODUCTLISTING,
  RELATEDPRODUCTS,
  SEARCH,
  ADDTOCART,
  GETCART,
  CARTITEMINCREMENTDECREMENT,
  ADDORREMOVEWISHLIST,
  GETWISHLIST,
  ADDNEWADDRESS,
  GETALLADDRESS,
  UPDATEADDRESS,
  EDITADDRESS,
  DELETEADDRESS,
  APPLYCOUPON,
  REMOVECOUPON,
  CHECKOUT,
  PRODUCTDETAILS,
  CREATEPAYMENT,
  LANGUAGES,
  SEARCHPRODUCT,
  CATEGORIESDETAILS,
  SEARCHADDRESS,
  CATEGORYLISTING,
  CATEGORYS,
  VENDORPRODUCTLIST,
  VENDORLIST,
  ALLPAYMENTMETHODS,
  PRIMARYADDRESS,
  ALLCOUPON,
} from '../../config/urls';

export function homePage(query = '', data = {}, headers = {}) {
  console.log(HOME + query, 'my custom header data >>>>>');
  return new Promise((resolve, reject) => {
    apiPost(HOME + query, data, headers)
      .then(res => {
        resolve(res);
      })
      .catch(error => {
        reject(error);
      });
  });
}
export function categoriesDetails(query = '', data = {}, headers = {}) {
  console.log(CATEGORIESDETAILS + query, 'CATEGORIESDETAILS + query');
  return new Promise((resolve, reject) => {
    apiPost(CATEGORIESDETAILS + query, data, headers)
      .then(res => {
        resolve(res);
      })
      .catch(error => {
        reject(error);
      });
  });
}

export function productlisting(query = '', data = {}, headers = {}) {
  return new Promise((resolve, reject) => {
    apiPost(PRODUCTLISTING + query, data, headers)
      .then(res => {
        resolve(res);
      })
      .catch(error => {
        reject(error);
      });
  });
}

export function productDetails(query = '', data = {}, headers = {}) {
  return new Promise((resolve, reject) => {
    apiGet(PRODUCTDETAILS + query, data, headers)
      .then(res => {
        resolve(res);
      })
      .catch(error => {
        reject(error);
      });
  });
}

export function search(query = '', data = {}, headers = {}) {
  return new Promise((resolve, reject) => {
    apiGet(SEARCH + query, data, headers)
      .then(res => {
        resolve(res);
      })
      .catch(error => {
        reject(error);
      });
  });
}

export function searchProduct(query = '', data = {}, headers = {}) {
  return new Promise((resolve, reject) => {
    apiGet(SEARCHPRODUCT + query, data, headers)
      .then(res => {
        resolve(res);
      })
      .catch(error => {
        reject(error);
      });
  });
}

export function addToCart(headers = {}, data = {}) {
  return new Promise((resolve, reject) => {
    apiPost(ADDTOCART, data, headers)
      .then(res => {
        resolve(res);
      })
      .catch(error => {
        reject(error);
      });
  });
}

export function getCart(data = {}, headers = {}) {
  return new Promise((resolve, reject) => {
    apiPost(GETCART, data, headers)
      .then(res => {
        resolve(res);
      })
      .catch(error => {
        reject(error);
      });
  });
}

export function cratItemIncrementDecrement(headers = {}, data = {}) {
  return new Promise((resolve, reject) => {
    apiPost(CARTITEMINCREMENTDECREMENT, data, headers)
      .then(res => {
        resolve(res);
      })
      .catch(error => {
        reject(error);
      });
  });
}

export function addOrRemoveWishlist(headers = {}, data = {}) {
  return new Promise((resolve, reject) => {
    apiPost(ADDORREMOVEWISHLIST, data, headers)
      .then(res => {
        resolve(res);
      })
      .catch(error => {
        reject(error);
      });
  });
}

export function getWishlist(headers = {}, data = {}) {
  return new Promise((resolve, reject) => {
    apiGet(GETWISHLIST, data, headers)
      .then(res => {
        resolve(res);
      })
      .catch(error => {
        reject(error);
      });
  });
}

export function addNewAddress(headers = {}, data = {}) {
  return new Promise((resolve, reject) => {
    apiPost(ADDNEWADDRESS, data, headers)
      .then(res => {
        resolve(res);
      })
      .catch(error => {
        reject(error);
      });
  });
}

export function getAllAddresses(headers = {}, data = {}) {
  return new Promise((resolve, reject) => {
    apiGet(GETALLADDRESS, data, headers)
      .then(res => {
        resolve(res);
      })
      .catch(error => {
        reject(error);
      });
  });
}

export function updateAddress(query = '', data = {}, headers = {}) {
  return new Promise((resolve, reject) => {
    apiPost(UPDATEADDRESS + query, data, headers)
      .then(res => {
        resolve(res);
      })
      .catch(error => {
        reject(error);
      });
  });
}

export function editAddress(query = '', data = {}, headers = {}) {
  console.log(EDITADDRESS + query, 'EDITADDRESS + query');
  return new Promise((resolve, reject) => {
    apiGet(EDITADDRESS + query, data, headers)
      .then(res => {
        resolve(res);
      })
      .catch(error => {
        reject(error);
      });
  });
}

export function deleteAddress(query = '', data = {}, headers = {}) {
  return new Promise((resolve, reject) => {
    apiGet(DELETEADDRESS + query, data, headers)
      .then(res => {
        resolve(res);
      })
      .catch(error => {
        reject(error);
      });
  });
}

export function setPrimaryAddress(query = '', data = {}, headers = {}) {
  return new Promise((resolve, reject) => {
    apiGet(PRIMARYADDRESS + query, data, headers)
      .then(res => {
        console.log(res, 'primary address');
        store.dispatch({
          type: ActionType.SETPRIMARYADDRESS,
          payload: res?.data,
        });
        resolve(res);
      })
      .catch(error => {
        reject(error);
      });
  });
}

export function _setPrimaryAddressForApplicationGlobaly(data = {}) {
  store.dispatch({
    type: ActionType.SETPRIMARYADDRESS,
    payload: data,
  });
}

export function applyCoupon(query = '', data = {}, headers = {}) {
  return new Promise((resolve, reject) => {
    apiGet(APPLYCOUPON + query, data, headers)
      .then(res => {
        resolve(res);
      })
      .catch(error => {
        reject(error);
      });
  });
}

export function removeCoupon(headers = {}, data = {}) {
  console.log(headers, data, 'datadatadatadatadata');
  return new Promise((resolve, reject) => {
    apiPost(REMOVECOUPON, data, headers)
      .then(res => {
        resolve(res);
      })
      .catch(error => {
        reject(error);
      });
  });
}

export function checkoutOrder(headers = {}, data = {}) {
  return new Promise((resolve, reject) => {
    apiPost(CHECKOUT, data, headers)
      .then(res => {
        resolve(res);
      })
      .catch(error => {
        reject(error);
      });
  });
}

export function createPayment(headers = {}, data = {}) {
  return new Promise((resolve, reject) => {
    apiPost(CREATEPAYMENT, data, headers)
      .then(res => {
        resolve(res);
      })
      .catch(error => {
        reject(error);
      });
  });
}
export function languageList(headers = {}, data = {}) {
  return new Promise((resolve, reject) => {
    apiGet(LANGUAGES, data, headers)
      .then(res => {
        resolve(res);
      })
      .catch(error => {
        reject(error);
      });
  });
}

export function searchAddress(query = '', data = {}, headers = {}) {
  return new Promise((resolve, reject) => {
    apiGet(SEARCHADDRESS + query, data, headers)
      .then(res => {
        resolve(res);
      })
      .catch(error => {
        reject(error);
      });
  });
}

export function categoryListing(query = '', headers = {}, data = {}) {
  return new Promise((resolve, reject) => {
    apiGet(CATEGORYS + query, data, headers)
      .then(res => {
        resolve(res);
      })
      .catch(error => {
        reject(error);
      });
  });
}

export function vendorProductListing(url = '', headers = {}, data = {}) {
  return new Promise((resolve, reject) => {
    apiPost(VENDORPRODUCTLIST + url, data, headers)
      .then(res => {
        resolve(res);
      })
      .catch(error => {
        reject(error);
      });
  });
}

export function vendorListing(data = {}, headers = {}) {
  return new Promise((resolve, reject) => {
    apiGet(VENDORLIST, data, headers)
      .then(res => {
        resolve(res);
      })
      .catch(error => {
        reject(error);
      });
  });
}

export function allPaymentMethods(data = {}, headers = {}) {
  return new Promise((resolve, reject) => {
    apiGet(ALLPAYMENTMETHODS, data, headers)
      .then(res => {
        resolve(res);
      })
      .catch(error => {
        reject(error);
      });
  });
}

export function couponsListing(headers = {}, data = {}) {
  return new Promise((resolve, reject) => {
    apiGet(ALLCOUPON, data, headers)
      .then(res => {
        resolve(res);
      })
      .catch(error => {
        reject(error);
      });
  });
}
