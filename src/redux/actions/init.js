import {apiGet, apiPost, clearUserData, setUserData} from '../../utils/utils';
import {APP_INITIAL_SETTINGS} from '../../config/urls';
import ActionType from '../types';
import store from '../store';

export function initAppSetting(data = {}) {
  return new Promise((resolve, reject) => {
    apiGet(APP_INITIAL_SETTINGS, data)
      .then(res => {
        let appData = res?.appStylings;
        let appInitialThemeData = {};
        let appInitialFontData = {};
        let appAllKeysData = res?.client_details;
        console.log(appData, 'appDataappData');

        appData.map((item, index) => {
          switch (index) {
            case 0:
              appInitialFontData['regular'] = item?.data?.name;
              break;
            case 1:
              appInitialFontData['medium'] = item?.data?.name;
              break;
            case 2:
              appInitialFontData['bold'] = item?.data?.name;
              break;
            case 3:
              appInitialThemeData['primary_color'] = item?.data?.name;
              break;
            case 4:
              appInitialThemeData['secondary_color'] = item?.data?.name;
              break;

            default:
              break;
          }
        });

        let initAppData = {
          ThemeColors: appInitialThemeData,
          fontFamily: appInitialFontData,
          appAllKeysData: appAllKeysData,
        };

        store.dispatch({
          type: ActionType.INITAPP,
          payload: initAppData,
        }),
          resolve(res);
      })
      .catch(error => {
        reject(error);
      });
  });
}
