import React from 'react';
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  Animated,
  StyleSheet,
} from 'react-native';
import {useSelector} from 'react-redux';

import imagePath from '../constants/imagePath';
import colors from '../styles/colors';
import {
  height,
  moderateScale,
  moderateVerticalScale,
  textScale,
  width,
} from '../styles/responsiveSize';
import {
  getImageUrl,
  getScaleTransformationStyle,
  pressInAnimation,
  pressOutAnimation,
} from '../utils/helperFunctions';
import {Rating, AirbnbRating} from 'react-native-ratings';

export default function VendorCard({
  data,
  onPress,
  cratItemIncrementDecrement,
  addTocart,
}) {
  const scaleInAnimated = new Animated.Value(0);
  const {ThemeColors, fontFamily} = useSelector(
    state => state?.init?.initAppData,
  );

  const styles = stylesFunc({fontFamily, ThemeColors});
  return (
    <TouchableOpacity
      style={{
        marginHorizontal: moderateScale(10),
        width: width - 40,
        backgroundColor: colors.white,
        borderRadius: 5,
        overflow: 'hidden',
        shadowColor: '#000',
        shadowOffset: {width: 0, height: 1},
        shadowOpacity: 0.8,
        shadowRadius: 2,
        elevation: 5,

        marginVertical: 8,
        ...getScaleTransformationStyle(scaleInAnimated),
      }}
      activeOpacity={1}
      onPressIn={() => pressInAnimation(scaleInAnimated)}
      onPressOut={() => pressOutAnimation(scaleInAnimated)}
      onPress={() => onPress(data)}>
      <Image
        source={{
          uri: getImageUrl(
            data?.image?.image_fit,
            data?.image?.image_path,
            '1000/1000',
          ),
        }}
        style={{
          resizeMode: 'stretch',
          width: moderateScale(width - 40),
          height: moderateVerticalScale(150),
        }}
      />
      <View style={styles.productInfoContainer}>
        <Text numberOfLines={2} style={styles.vendorNameText}>
          {data?.name}
        </Text>

        <Text
          numberOfLines={2}
          style={[
            styles.vendorDescriptionText,
            {marginBottom: moderateVerticalScale(10)},
          ]}>
          {data?.description}
        </Text>
      </View>
      <View
        style={{
          position: 'absolute',
          right: 15,
          bottom: moderateVerticalScale(75),
          marginTop: moderateVerticalScale(5),

          justifyContent: 'center',
          alignItems: 'center',
          flexDirection: 'row',
        }}>
        <Rating
          ratingCount={5}
          type="custom"
          imageSize={16}
          startingValue={5}
          isDisable={true}
          ratingBackgroundColor={'transparent'}
          ratingColor={'yellow'}
          tintColor={'#242A38'}
        />
        <Text
          style={{
            color: ThemeColors?.secondary_color,
            marginHorizontal: moderateScale(5),
          }}>
          (1075)
        </Text>
      </View>
    </TouchableOpacity>
  );
}

export function stylesFunc({fontFamily, ThemeColors}) {
  const styles = StyleSheet.create({
    vendorNameText: {
      color: colors.black,
      fontFamily: fontFamily.bold,
      fontSize: textScale(15),
      marginTop: moderateVerticalScale(10),
    },
    vendorDescriptionText: {
      color: colors.black,
      fontFamily: fontFamily.regular,
      fontSize: textScale(13),
    },
    productInfoContainer: {
      width: width / 1.7,
      paddingHorizontal: moderateScale(17),
    },
  });

  return styles;
}
