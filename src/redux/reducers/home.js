import ActionTypes from '../types';

const initialState = {
  primaryAddress: {},
  primaryLanguage: {},
};
export default function home(state = initialState, action) {
  switch (action.type) {
    case ActionTypes.SETPRIMARYADDRESS:
      const primaryAddress = action.payload;

      return {
        ...state,
        primaryAddress: primaryAddress,
      };

    case ActionTypes.PRIMARYLANGUAGE:
      const primaryLanguage = action.payload;

      return {
        ...state,
        primaryLanguage: primaryLanguage,
      };

    default:
      return state;
  }
}
