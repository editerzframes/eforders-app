import React from 'react';
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  Animated,
  StyleSheet,
} from 'react-native';
import {useSelector} from 'react-redux';

import imagePath from '../constants/imagePath';
import colors from '../styles/colors';

import {
  height,
  moderateScale,
  moderateVerticalScale,
  textScale,
  width,
} from '../styles/responsiveSize';
import {
  getImageUrl,
  getScaleTransformationStyle,
  pressInAnimation,
  pressOutAnimation,
} from '../utils/helperFunctions';

export default function LargeCartList({
  data,
  onPress,
  cratItemIncrementDecrement,
}) {
  const scaleInAnimated = new Animated.Value(0);
  const {ThemeColors, fontFamily} = useSelector(
    state => state?.init?.initAppData,
  );
  const styles = stylesFunc({fontFamily, ThemeColors});
  console.log(data, 'data in home vandor');
  const imageUrl = data?.image
    ? getImageUrl(data.image.image_fit, data.image.image_path, '1000/1000')
    : getImageUrl(data.image.image_fit, data.image.image_path, '1000/1000');

  console.log(imageUrl, 'imageUrl');
  return (
    <TouchableOpacity
      style={{
        marginHorizontal: moderateScale(10),

        backgroundColor: colors.white,
        borderRadius: 5,
        overflow: 'hidden',
        shadowColor: '#000',
        shadowOffset: {width: 0, height: 1},
        shadowOpacity: 0.8,
        shadowRadius: 2,
        elevation: 5,

        marginVertical: 8,
        ...getScaleTransformationStyle(scaleInAnimated),
      }}
      activeOpacity={1}
      onPressIn={() => pressInAnimation(scaleInAnimated)}
      onPressOut={() => pressOutAnimation(scaleInAnimated)}
      onPress={() => onPress(data)}>
      <Image
        source={{
          uri: imageUrl,
        }}
        style={{
          width: moderateScale(width / 1.8),
          height: moderateVerticalScale(160),
        }}
        resizeMode="stretch"
        blurRadius={2}
      />
      <View style={styles.vendorInfoView}>
        <Text numberOfLines={1} style={styles.vendorNameText}>
          {data?.name}
        </Text>
        <Text numberOfLines={2} style={styles.vendorDescriptionText}>
          {data?.description}
        </Text>
      </View>
    </TouchableOpacity>
  );
}
export function stylesFunc({fontFamily, ThemeColors}) {
  const styles = StyleSheet.create({
    vendorNameText: {
      color: colors.white,
      fontFamily: fontFamily?.bold,
      fontSize: textScale(18),
    },
    vendorDescriptionText: {
      color: colors.white,
      fontFamily: fontFamily?.regular,
      fontSize: textScale(14),
    },
    vendorInfoView: {
      position: 'absolute',
      bottom: 20,
      left: 10,
      width: moderateScale(160),
    },
  });
  return styles;
}
