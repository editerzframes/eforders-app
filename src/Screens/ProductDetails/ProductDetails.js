import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  Image,
  ScrollView,
  TouchableOpacity,
  FlatList,
} from 'react-native';
import Crousel from '../../Components/Crousel';

import Header from '../../Components/Header';
import ProductIntroCrousel from '../../Components/ProductIntroCrousel';
import WrapperContainer from '../../Components/WrapperContainer';
import imagePath from '../../constants/imagePath';
import strings from '../../constants/lang';
import colors from '../../styles/colors';
import {
  height,
  moderateScale,
  moderateVerticalScale,
  textScale,
  width,
} from '../../styles/responsiveSize';
import {stylesFunc} from './styles';
import DropDownPicker from 'react-native-dropdown-picker';
import actions from '../../redux/actions';
import {showMessage} from 'react-native-flash-message';
import ProductListCard from '../../Components/ProductListCard';
import {useSelector} from 'react-redux';
import navigationStrings from '../../constants/navigationStrings';
import HTMLView from 'react-native-htmlview';

import {getColorCodeWithOpactiyNumber} from '../../utils/helperFunctions';

import CountDownTimer from '../../Components/CountDownTimer';
import UserRating from '../../Components/UserRating';

export default function ProductDetails({navigation, route}) {
  const data = route?.params;
  const [state, setState] = useState({
    banner: [
      {
        key: '1',
        text: 'Item text ',
        video: true,
        uri: 'https://www.kindpng.com/picc/m/697-6974065_vivo-best-phone-under-20000-hd-png-download.png',
      },
      {
        key: '2',
        text: 'Item text 2',
        video: false,
        uri: 'https://m.media-amazon.com/images/I/71bcl7VlwVL._SL1500_.jpg',
      },

      {
        key: '3',
        text: 'Item text 3',
        video: false,
        uri: 'https://static.turbosquid.com/Preview/2019/10/11__08_30_44/01_sig.jpg11DC4C03-8C40-4BE6-9BF0-04E15818942BLarge.jpg',
      },
      {
        key: '4',
        text: 'Item text 4',
        video: false,
        uri: 'https://jpstream.net/wp-content/uploads/2020/09/x19-04.jpg',
      },
      {
        key: '5',
        text: 'Item text 5',
        video: false,
        uri: 'https://m.media-amazon.com/images/I/61woHrH2KjL._AC_SL1000_.jpg',
      },
    ],
    open: false,
    dropdownitems: [
      {label: '1', value: '1'},
      {label: '2', value: '2'},
      {label: '3', value: '3'},
      {label: '4', value: '4'},
      {label: '5', value: '5'},
    ],
    userPresedProduct: null,
    relatedProducts: [],
    inWishlist: null,
    cartBadge,
    selectedRelatedProductId: null,
    isLoading: false,
    productAllImages: [],
  });
  const [dropdownValue, setdropdownValue] = useState(1);

  const {
    banner,
    open,
    dropdownitems,
    userPresedProduct,
    relatedProducts,
    inWishlist,
    cartBadge,
    selectedRelatedProductId,
    isLoading,
    productAllImages,
  } = state;

  const updateState = data => setState(state => ({...state, ...data}));
  const logedInUser = useSelector(state => state?.auth?.userData);
  const totalCartBadge = useSelector(state => state?.cart?.cartBadge);
  const {ThemeColors, fontFamily} = useSelector(
    state => state?.init?.initAppData,
  );
  const styles = stylesFunc({fontFamily, ThemeColors});

  const setDropdownOptions = (type, value) => {
    if (type === 'item') {
      updateState({
        dropdownitems: value,
      });
    }
    if (type === 'open') {
      updateState({
        open: value,
      });
    }
  };

  useEffect(() => {
    loadProductDetails();
  }, [inWishlist]);

  useEffect(() => {
    loadProductDetails();
  }, [selectedRelatedProductId]);

  const loadProductDetails = () => {
    updateState({
      isLoading: true,
    });

    let query = `/${
      selectedRelatedProductId
        ? selectedRelatedProductId
        : data?.selectedProduct
    }`;
    actions
      .productDetails(query, {}, {})
      .then(res => {
        updateState({
          isLoading: false,
        });
        console.log(res, 'resresres');
        updateState({
          relatedProducts: res?.data?.similar_products,
          inWishlist: res?.data?.in_wishlist,
          userPresedProduct: res?.data?.product,
          productAllImages: res?.data?.product?.product_images,
        });
      })
      .catch(error => {
        console.log(error);
        updateState({
          isLoading: false,
        });
      });
  };

  const addWishListData = id => {
    const data = {
      product_id: id,
    };
    actions
      .addOrRemoveWishlist(logedInUser?.access_token, data)
      .then(res => {
        console.log(res, 'resss');
        updateState({
          inWishlist: res?.data?.in_wishlist,
        });
        showMessage({
          message: res.message,
          type: 'success',
        });
      })
      .catch(error => {
        showMessage({
          message: error.message,
          type: 'danger',
        });
      });
  };

  const addToCart = (productId, buyProductNow) => {
    const data = {
      product_id: selectedRelatedProductId
        ? selectedRelatedProductId
        : productId,
    };

    console.log(productId, buyProductNow, 'productIdproductIdproductId');
    actions
      .addToCart(logedInUser?.access_token, data)
      .then(res => {
        showMessage({
          message: res.message,
          type: 'success',
        });
        getCartData();
        if (buyProductNow?.buyNow) {
          navigation.navigate(navigationStrings.CHECKOUT);
        }
        //actions.showCartBadge(1);
      })
      .catch(error => {
        showMessage({
          message: error.message,
          type: 'danger',
        });
      });
  };

  // get Cart data

  const getCartData = () => {
    actions
      .getCart({}, logedInUser?.access_token)
      .then(res => {
        console.log(res, 'response');
        updateState({
          cartBadge: res?.data?.total_quantity,
        });
        actions.showCartBadge(res?.data?.item_count);
      })
      .catch(error => {
        showMessage({
          message: error.message,
          type: 'danger',
        });
      });
  };

  const _moveToNextScreen = (item, data) => {
    navigation.navigate(item, {selectedProduct: data?.id});
  };

  const onProductDetails = data => {
    updateState({
      selectedRelatedProductId: data?.id,
    });
  };

  const relatedProduct = item => {
    console.log(item, 'item');
    return (
      <ProductListCard
        data={item}
        horizontal={true}
        onPress={data => onProductDetails(data)}
      />
    );
  };

  const _buyNow = productId => {
    addToCart(productId, {buyNow: true});
  };

  return (
    <WrapperContainer bgColor={colors.white} isLoading={isLoading}>
      {userPresedProduct && (
        <View style={styles.mainContainer}>
          <View style={styles.headerContainer}>
            <Header
              leftIcon={imagePath.back}
              textStyle={{color: colors.black}}
              centerTitle={strings.PRODUCTDETAILS}
              rightIcon={imagePath.share}
              rightIcon1={imagePath.cart}
            />
            {totalCartBadge > 0 && logedInUser?.access_token ? (
              <View style={styles.cartIconStyle}>
                <Text
                  style={{
                    color: colors.white,
                    fontSize: textScale(8),
                    fontFamily: fontFamily?.bold,
                  }}>
                  {totalCartBadge}
                </Text>
              </View>
            ) : null}
            <TouchableOpacity
              style={{position: 'absolute', end: 40}}
              onPress={() => _moveToNextScreen(navigationStrings.BASKET)}>
              <Image source={imagePath.cart} />
            </TouchableOpacity>
          </View>
          <ScrollView showsVerticalScrollIndicator={false}>
            <View>
              <View style={{flex: 1}}>
                <View>
                  <TouchableOpacity
                    style={styles.wishlistIconStyle}
                    onPress={
                      logedInUser?.access_token
                        ? () => addWishListData(userPresedProduct?.id)
                        : null
                    }>
                    {inWishlist ? (
                      <Image source={imagePath.inactivelove} />
                    ) : (
                      <Image
                        style={{tintColor: colors.red}}
                        source={imagePath.love}
                      />
                    )}
                  </TouchableOpacity>
                  <ProductIntroCrousel
                    indicator
                    bannerData={productAllImages}
                    autoscroll={false}
                  />
                </View>
                <View style={[styles.productInfoContainer]}>
                  <Text numberOfLines={2} style={styles.productNameTextStyle}>
                    {userPresedProduct?.name}
                  </Text>

                  <View style={styles.ratingContainerView}>
                    <UserRating />
                    <Text
                      style={{
                        marginHorizontal: moderateScale(8),
                        color: colors.textGreyOpcaity7,
                        fontFamily: fontFamily?.regular,
                        fontSize: textScale(12.5),
                      }}>
                      3 ratings
                    </Text>
                  </View>

                  <View
                    style={{
                      marginVertical: moderateVerticalScale(5),
                      alignItems: 'center',
                      backgroundColor: 'rgba(0,128,0,0.07)',
                      paddingHorizontal: moderateScale(5),
                    }}>
                    <Text
                      style={{
                        color: colors.green,
                        fontFamily: fontFamily?.regular,
                      }}>
                      Deal of the Day ends in
                    </Text>
                    <CountDownTimer />
                  </View>
                  <View style={styles.priceContainer}>
                    <View style={styles.priceContainer1}>
                      {userPresedProduct?.sale_price > 0 && (
                        <Text
                          style={[
                            styles.productPriceTextStyle,
                            {marginHorizontal: moderateScale(5)},
                          ]}>{`$${userPresedProduct?.sale_price} `}</Text>
                      )}

                      <Text
                        style={[
                          styles.productPriceTextStyle1,
                          {
                            textDecorationLine:
                              userPresedProduct?.sale_price != 0 &&
                              'line-through',
                          },
                        ]}>
                        {`$${userPresedProduct?.price}`}
                      </Text>
                    </View>
                    <View style={{width: width / 3.5}}></View>
                  </View>

                  <View style={styles.productColorSizeViewContainer}>
                    <View style={styles.productColorSizeInnerContainer}>
                      <Text style={styles.productColorSizeLabelTextStyle}>
                        red
                      </Text>
                      <Text>{strings.COLOR}</Text>
                    </View>
                    <View style={styles.productColorSizeInnerContainer}>
                      <Text style={styles.productColorSizeLabelTextStyle}>
                        M
                      </Text>
                      <Text>{strings.SIZE}</Text>
                    </View>
                    <View style={styles.productColorSizeInnerContainer}>
                      <Text style={styles.productColorSizeLabelTextStyle}>
                        {userPresedProduct?.quantity}
                      </Text>
                      <Text>{strings.STOCK}</Text>
                    </View>
                  </View>

                  {true && (
                    <View>
                      <View style={{marginTop: moderateVerticalScale(20)}}>
                        <Text
                          style={{
                            fontFamily: fontFamily.bold,
                            color: colors.black,
                            fontSize: textScale(16),
                          }}>
                          {strings.COLOUR}
                        </Text>
                      </View>
                      <ScrollView
                        horizontal
                        style={{marginTop: moderateVerticalScale(10)}}>
                        {[
                          {id: 1, color: 'red'},
                          {id: 1, color: 'blue'},
                          {id: 1, color: 'green'},
                        ].map((item, index) => {
                          return (
                            <View
                              style={{
                                height: 40,
                                width: 40,
                                borderRadius: 20,
                                backgroundColor: item.color,
                                marginRight: moderateScale(10),
                                borderWidth: 3,
                                borderColor: ThemeColors?.primary_color,
                              }}></View>
                          );
                        })}
                      </ScrollView>

                      <View style={{marginTop: moderateVerticalScale(20)}}>
                        <Text
                          style={{
                            fontFamily: fontFamily.bold,
                            color: colors.black,
                            fontSize: textScale(16),
                          }}>
                          {strings.SIZE}
                        </Text>
                      </View>
                      <ScrollView
                        horizontal
                        style={{
                          marginVertical: moderateVerticalScale(10),
                        }}>
                        {[1, 2, 3].map((item, index) => {
                          return (
                            <View
                              style={{
                                height: moderateVerticalScale(40),
                                width: moderateScale(40),
                                borderWidth: 0.8,
                                borderColor: colors.textGreyB,
                                marginRight: moderateScale(10),
                                justifyContent: 'center',
                                alignItems: 'center',
                              }}>
                              <Text style={{fontSize: textScale(14)}}>XL</Text>
                            </View>
                          );
                        })}
                      </ScrollView>
                    </View>
                  )}
                  {userPresedProduct?.description != null && (
                    <View style={{marginVertical: moderateVerticalScale(5)}}>
                      <HTMLView
                        value={userPresedProduct?.description}
                        stylesheet={styles.descriptionText}
                      />
                    </View>
                  )}

                  {relatedProducts?.length && (
                    <View>
                      {relatedProducts?.length ? (
                        <Text style={styles.productPriceTextStyle1}>
                          {strings.RELATEDPRODUCTS}
                        </Text>
                      ) : null}

                      <FlatList
                        horizontal
                        data={relatedProducts}
                        renderItem={({item}) => relatedProduct(item)}
                        showsHorizontalScrollIndicator={false}
                      />
                    </View>
                  )}
                </View>
              </View>
            </View>

            <View
              style={{
                height:
                  userPresedProduct?.description === '' &&
                  relatedProducts?.length
                    ? moderateVerticalScale(100)
                    : moderateVerticalScale(80),
              }}
            />
          </ScrollView>
          <View style={styles.bottombtnContainer}>
            <TouchableOpacity
              style={styles.cartButtonStyle}
              onPress={() => addToCart(data?.selectedProduct, {buyNow: false})}>
              <Image
                style={{tintColor: colors.white}}
                source={imagePath.cart}
              />
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.buyNowButtonStyle}
              onPress={() => _buyNow(data?.selectedProduct)}>
              <Text style={{fontSize: textScale(16), color: colors.white}}>
                {strings.BUYNOW}
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      )}
    </WrapperContainer>
  );
}
