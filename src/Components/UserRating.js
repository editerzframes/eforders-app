import {View, Text} from 'react-native';
import React from 'react';
import {Rating, AirbnbRating} from 'react-native-ratings';
import {moderateVerticalScale} from '../styles/responsiveSize';
import {getColorCodeWithOpactiyNumber} from '../utils/helperFunctions';
import colors from '../styles/colors';

export default function UserRating() {
  return (
    <Rating
      style={{
        alignSelf: 'flex-start',
        marginVertical: moderateVerticalScale(5),
      }}
      ratingCount={5}
      type="custom"
      imageSize={16}
      startingValue={5}
      isDisable={true}
      ratingColor={colors.green}
      ratingBackgroundColor={getColorCodeWithOpactiyNumber(
        colors.green.substring(1),
        20,
      )}
    />
  );
}
