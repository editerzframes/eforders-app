import React, {useState} from 'react';
import {View, Text, Image, TouchableOpacity} from 'react-native';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import HeaderWithFilters from '../../Components/HeaderWithFilters';
import WrapperContainer from '../../Components/WrapperContainer';
import imagePath from '../../constants/imagePath';
import colors from '../../styles/colors';
import {
  height,
  moderateScale,
  moderateVerticalScale,
  width,
  textScale,
} from '../../styles/responsiveSize';
import {stylesFunc} from './styles';
import ButtonComponent from '../../Components/ButtonComponent';
import fontFamily from '../../styles/fontFamily';
import TextInputWithLabel from '../../Components/TextInputWithLabel';
import {
  GoogleSignin,
  statusCodes,
} from '@react-native-google-signin/google-signin';
import {
  LoginManager,
  AccessToken,
  GraphRequest,
  GraphRequestManager,
} from 'react-native-fbsdk';
import validations from '../../utils/validations';
import {showMessage, hideMessage} from 'react-native-flash-message';
import actions from '../../redux/actions';
import {getColorCodeWithOpactiyNumber} from '../../utils/helperFunctions';
import {useSelector} from 'react-redux';
import ModalView from '../../Components/Modal';

export default function Register(props) {
  const {navigation} = props;
  const [state, setState] = useState({
    securePassord: true,
    confirmPassword: true,
    email: '',
    name: '',
    phoneNumber: '',
    password: '',
    confirmPass: '',
    focusInput: '',
  });

  const {
    securePassord,
    confirmPassword,
    email,
    name,
    phoneNumber,
    password,
    confirmPass,
    focusInput,
  } = state;

  const updateState = data => setState(state => ({...state, ...data}));
  const {ThemeColors, fontFamily, appAllKeysData} = useSelector(
    state => state?.init?.initAppData,
  );
  const styles = stylesFunc({fontFamily, ThemeColors});

  const _securePasswordEnterEnable = type => {
    if (type === 'password') {
      updateState({
        securePassord: securePassord ? false : true,
      });
    } else {
      updateState({
        confirmPassword: confirmPassword ? false : true,
      });
    }
  };

  const _moveToLogin = () => {};

  const isValidData = () => {
    const error = validations({
      email: email,
      password: password,
      name: name,
      phoneNumber: phoneNumber,
      confirmPassword: confirmPass,
    });
    if (error) {
      showMessage({
        message: error,
        type: 'danger',
      });
      return;
    }
    return true;
  };

  const Register = () => {
    let {callingCode} = state;
    const checkValid = isValidData();
    if (!checkValid) {
      return;
    }

    let data = {
      name: name,
      email: email,
      phone_number: phoneNumber,
      password: password,
    };

    actions
      .register(data)
      .then(res => {
        showMessage({
          message: res.message,
          type: 'success',
        });
      })
      .catch(error =>
        showMessage({
          message: error.message,
          type: 'danger',
        }),
      );
  };

  //google login code

  const googleLogin = async () => {
    GoogleSignin.configure();
    try {
      // await GoogleSignin.hasPlayServices();
      // await GoogleSignin.revokeAccess();
      await GoogleSignin.signOut();
      const userInfo = await GoogleSignin.signIn();

      const data = {
        email: userInfo.user.email,
        name: userInfo.user.name,
        type: 'google',
        social_id: userInfo.user.id,
      };

      console.log(data, 'datadata');
      actions
        .socialLogin(data)
        .then(res => {
          console.log(res, 'response');
          showMessage({
            message: res.message,
            type: 'success',
          });
          navigation.navigate(navigationStrings.HOME);
        })
        .catch(error => {
          showMessage({
            message: error.message,
            type: 'danger',
          });
        });
    } catch (error) {
      console.log(error, 'errorerror');
      if (error.code === statusCodes.SIGN_IN_CANCELLED) {
        return error;
      } else if (error.code === statusCodes.IN_PROGRESS) {
        return error;
      } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
        return error;
      } else {
        return error;
      }
    }
  };

  // facebook login code

  const loginWithFacebook = () => {
    // Attempt a login using the Facebook login dialog asking for default permissions.
    LoginManager.logInWithPermissions(['public_profile']).then(
      login => {
        if (login.isCancelled) {
          console.log('Login cancelled');
        } else {
          AccessToken.getCurrentAccessToken().then(data => {
            const accessToken = data.accessToken.toString();
            getInfoFromToken(accessToken);
          });
        }
      },
      error => {
        console.log('Login fail with error: ' + error);
      },
    );
  };

  const getInfoFromToken = token => {
    const PROFILE_REQUEST_PARAMS = {
      fields: {
        string: 'id, name,  first_name, last_name',
      },
    };
    const profileRequest = new GraphRequest(
      '/me',
      {token, parameters: PROFILE_REQUEST_PARAMS},
      (error, result) => {
        if (error) {
          console.log('login info has error: ' + error);
        } else {
          console.log('result:', result);

          const data = result.email
            ? {
                email: result.email,
                name: result.name,
                type: 'facebook',
                social_id: result.id,
              }
            : {
                name: result.name,
                type: 'facebook',
                social_id: result.id,
              };
          console.log(data, 'data object');
          actions
            .socialLogin(data)
            .then(res => {
              showMessage({
                message: res.message,
                type: 'success',
              });
            })
            .catch(error => {
              console.log(error, 'error');
              showMessage({
                message: error.message,
                type: 'danger',
              });
            });
        }
      },
    );
    new GraphRequestManager().addRequest(profileRequest).start();
  };

  const _onChangeText = key => val => {
    updateState({[key]: val});
  };

  const userFocus = firstname => {
    updateState({
      focusInput: firstname,
    });
  };

  return (
    <WrapperContainer bgColor={{backgroundColor: colors.white}}>
      <View style={styles.headerContiner}>
        <HeaderWithFilters
          leftIcon={imagePath.backArrow}
          // centerTitle={'Sign In'}
        />
      </View>

      <KeyboardAwareScrollView
        style={{
          height: height - 20,
          marginHorizontal: moderateScale(10),
          paddingVertical: moderateVerticalScale(10),
        }}
        showsVerticalScrollIndicator={false}>
        <View
          style={{
            // backgroundColor: colors.textGreyB,
            paddingBottom: moderateVerticalScale(10),
            paddingHorizontal: moderateScale(20),
          }}>
          <Text
            style={{
              color: colors.black,
              fontSize: textScale(20),
              fontFamily: fontFamily?.bold,
              marginVertical: moderateScale(10),
              textAlign: 'center',
            }}>
            Let's Get Started!
          </Text>
          <Text
            style={{
              color: colors.textGreyLight,
              fontSize: textScale(15),
              fontFamily: fontFamily?.regular,
              textAlign: 'center',
            }}>
            Create an account to get all the features
          </Text>
        </View>
        <View
          style={{
            backgroundColor: colors.white,
            shadowColor: colors.black,
            shadowOffset: {width: 0.3, height: 0.3},
            shadowOpacity: 0.3,
            shadowRadius: 8,
            elevation: 5,
            paddingHorizontal: moderateScale(10),
            paddingVertical: moderateVerticalScale(20),

            borderRadius: 10,
            marginHorizontal: 10,
          }}>
          <TextInputWithLabel
            placeholder={'Your name'}
            customTextStyle={{
              marginVertical: moderateVerticalScale(10),
              borderColor:
                focusInput === 'name'
                  ? ThemeColors?.primary_color
                  : getColorCodeWithOpactiyNumber(
                      colors.textGreyLight.substr(1),
                      60,
                    ),
              borderRadius: 8,
              height: moderateVerticalScale(55),
              paddingHorizontal: moderateScale(16),
              fontSize: textScale(15),
            }}
            onChangeText={_onChangeText('name')}
            userFocus={focus => userFocus('name')}
            label={'Name'}
          />
          <TextInputWithLabel
            placeholder={'Your email address'}
            customTextStyle={{
              marginVertical: moderateVerticalScale(10),
              borderColor:
                focusInput === 'email'
                  ? ThemeColors?.primary_color
                  : getColorCodeWithOpactiyNumber(
                      colors.textGreyLight.substr(1),
                      60,
                    ),
              borderRadius: 8,
              height: moderateVerticalScale(55),
              paddingHorizontal: moderateScale(16),
              fontSize: textScale(15),
            }}
            onChangeText={_onChangeText('email')}
            userFocus={focus => userFocus('email')}
            label={'E-mail Address'}
          />
          <TextInputWithLabel
            placeholder={'Mobile number'}
            customTextStyle={{
              marginVertical: moderateVerticalScale(10),
              borderColor:
                focusInput === 'mobile'
                  ? ThemeColors?.primary_color
                  : getColorCodeWithOpactiyNumber(
                      colors.textGreyLight.substr(1),
                      60,
                    ),
              borderRadius: 8,
              height: moderateVerticalScale(55),
              paddingHorizontal: moderateScale(16),
              fontSize: textScale(15),
            }}
            onChangeText={_onChangeText('phoneNumber')}
            userFocus={focus => userFocus('mobile')}
            label={'Mobile Number'}
          />

          <TextInputWithLabel
            placeholder={'Password'}
            customTextStyle={{
              marginVertical: moderateVerticalScale(10),
              borderRadius: 8,
              borderColor:
                focusInput === 'password'
                  ? ThemeColors?.primary_color
                  : getColorCodeWithOpactiyNumber(
                      colors.textGreyLight.substr(1),
                      60,
                    ),
              height: moderateVerticalScale(55),
              paddingHorizontal: moderateScale(16),
              fontSize: textScale(15),
            }}
            rightIcon={securePassord ? imagePath.close_eye : imagePath.open_eye}
            onPressRightIcon={() => _securePasswordEnterEnable('password')}
            secureTextEntry={securePassord}
            onChangeText={_onChangeText('password')}
            userFocus={focus => userFocus('password')}
            label={'Password'}
          />

          <TextInputWithLabel
            placeholder={'Confirm password'}
            customTextStyle={{
              marginVertical: moderateVerticalScale(10),
              borderRadius: 8,
              borderColor:
                focusInput === 'confirmpassword'
                  ? ThemeColors?.primary_color
                  : getColorCodeWithOpactiyNumber(
                      colors.textGreyLight.substr(1),
                      60,
                    ),
              height: moderateVerticalScale(55),
              paddingHorizontal: moderateScale(16),
              fontSize: textScale(15),
            }}
            rightIcon={
              confirmPassword ? imagePath.close_eye : imagePath.open_eye
            }
            onPressRightIcon={() => _securePasswordEnterEnable('code')}
            secureTextEntry={confirmPassword}
            onChangeText={_onChangeText('confirmPass')}
            userFocus={focus => userFocus('confirmpassword')}
            label={'Confirm Password'}
          />

          <ButtonComponent
            btnText={'Sign Up'}
            btnStyle={{
              backgroundColor: ThemeColors?.primary_color,
              height: moderateVerticalScale(60),
              width: moderateScale(width / 2),
              borderRadius: 50,
              marginVertical: moderateVerticalScale(20),
              alignSelf: 'center',
            }}
            textStyle={{fontFamily: fontFamily?.bold, fontSize: textScale(18)}}
            onPress={Register}
          />
        </View>

        {appAllKeysData?.google_login || appAllKeysData?.facebook_login ? (
          <Text style={styles.screenTitleTextStyle}>Or connect with</Text>
        ) : null}
        <View
          style={[
            styles.socialIconContainer,
            {marginVertical: moderateVerticalScale(10)},
          ]}>
          {appAllKeysData?.google_login ? (
            <TouchableOpacity
              style={
                appAllKeysData?.google_login && appAllKeysData?.facebook_login
                  ? [styles.socialIconInnerContainer]
                  : [styles.socialIconInnerContainer, {width: width / 1.25}]
              }
              onPress={googleLogin}>
              <Image
                style={styles.socialIconStyle}
                source={imagePath.icon_google}
              />
              <Text style={styles.socialHeadingTextStyle}>Google</Text>
            </TouchableOpacity>
          ) : null}

          {appAllKeysData?.facebook_login ? (
            <TouchableOpacity
              style={
                appAllKeysData?.google_login && appAllKeysData?.facebook_login
                  ? [styles.socialIconInnerContainer]
                  : [styles.socialIconInnerContainer, {width: width / 1.25}]
              }
              onPress={loginWithFacebook}>
              <Image
                style={styles.socialIconStyle}
                source={imagePath.icon_facebook}
              />
              <Text
                style={[
                  styles.socialHeadingTextStyle,
                  {marginHorizontal: moderateScale(10)},
                ]}>
                Facebook
              </Text>
            </TouchableOpacity>
          ) : null}
        </View>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'center',
            marginVertical: moderateVerticalScale(20),
          }}>
          <Text style={styles.createNewAccountText}>Already Login ?</Text>
          <TouchableOpacity onPress={() => navigation.goBack()}>
            <Text style={styles.signUpTextStyle}>Sign In</Text>
          </TouchableOpacity>
        </View>
      </KeyboardAwareScrollView>
    </WrapperContainer>
  );
}
