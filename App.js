import React, {useEffect, useState} from 'react';
import {ActivityIndicator, View} from 'react-native';
import {SafeAreaProvider} from 'react-native-safe-area-context';
import FlashMessage from 'react-native-flash-message';
import store from './src/redux/store';
import {Provider} from 'react-redux';
import Routes from './src/Navigation/Routes';
import requestUserPermission from './src/utils/notificationServices';
import {
  getcartBadge,
  getItem,
  getPrimaryAddress,
  getUserData,
} from './src/utils/utils';
import ActionType from './src/redux/types';
import colors from './src/styles/colors';
import SplashScreen from 'react-native-splash-screen';
import strings from './src/constants/lang';
import actions from './src/redux/actions';
import {update} from 'lodash';
import {height} from './src/styles/responsiveSize';
export default function App() {
  const [state, setState] = useState({
    isLoading: true,
  });

  const {isLoading} = state;
  const updateState = data => setState(state => ({...state, ...data}));

  useEffect(() => {
    // requestUserPermission();
    getUserData()
      .then(res => {
        store.dispatch({
          type: ActionType.LOGIN,
          payload: res,
        });
      })
      .catch(error => {});

    getPrimaryAddress()
      .then(res => {
        store.dispatch({
          type: ActionType.SETPRIMARYADDRESS,
          payload: res,
        });
      })
      .catch(error => {});

    getcartBadge()
      .then(res => {
        store.dispatch({
          type: ActionType.CARTBADGE,
          payload: res,
        });
      })
      .catch(error => {});

    // SplashScreen.hide();
  }, []);

  useEffect(() => {
    async function fetchLangugae() {
      const getLanguage = await getItem('language');

      if (getLanguage) {
        console.log(getLanguage, 'my get language');
        strings.setLanguage(getLanguage?.code);
        store.dispatch({
          type: ActionType.PRIMARYLANGUAGE,
          payload: getLanguage,
        });
      }
    }
    fetchLangugae();
  }, []);

  // INTI APP SETTINGS

  const initAppSettings = () => {
    actions
      .initAppSetting()
      .then(res =>
        updateState({
          isLoading: false,
        }),
      )
      .catch(error => console.log(error, 'error'));
  };

  useEffect(() => {
    initAppSettings();
  }, []);

  return (
    <SafeAreaProvider style={{flex: 1}}>
      <Provider store={store}>
        {isLoading ? (
          <View
            style={{
              flex: 1,
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <ActivityIndicator size="small" color="#0000" />
          </View>
        ) : (
          <Routes />
        )}
      </Provider>
      <FlashMessage position="top" />
    </SafeAreaProvider>
  );
}
