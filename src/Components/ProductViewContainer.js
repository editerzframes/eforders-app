import React from 'react';
import {View, Text, TouchableOpacity, Image} from 'react-native';
import {useSelector} from 'react-redux';
import colors from '../styles/colors';

import {
  height,
  moderateScale,
  moderateVerticalScale,
  textScale,
  width,
} from '../styles/responsiveSize';
import {getImageUrl} from '../utils/helperFunctions';

export default function ProductViewContainer({data, onPress, addTocart}) {
  const {ThemeColors, fontFamily} = useSelector(
    state => state?.init?.initAppData,
  );

  return (
    <TouchableOpacity
      style={{
        width: width / 2.2,
        marginHorizontal: moderateScale(5),
        height: moderateVerticalScale(height / 3.2),
        backgroundColor: colors.white,
        shadowColor: colors.black,
        shadowOffset: {width: 1, height: 1},
        shadowOpacity: 1,
        shadowRadius: 1,
        elevation: 5,
        marginVertical: moderateVerticalScale(5),
        borderRadius: 8,
        overflow: 'hidden',
      }}
      onPress={() => onPress(data)}>
      <Image
        style={{
          width: width / 2.2,
          height: height / 4.7,
          backgroundColor: colors.textGreyLight,
        }}
        source={{
          uri: getImageUrl(
            data?.icon?.image_fit,
            data?.icon?.image_path,
            '1000/1000',
          ),
        }}
      />
      <View
        style={{
          height: moderateVerticalScale(25),
          width: moderateScale(45),
          backgroundColor: colors.green,
          position: 'absolute',
          right: 0,
          top: 10,
          borderTopLeftRadius: 20,
          borderBottomLeftRadius: 20,
          justifyContent: 'center',
          alignItems: 'center',
          flexDirection: 'row',
        }}>
        <Text
          style={{
            color: colors.white,
            fontSize: textScale(12),
            top: -1,
            fontFamily: fontFamily.bold,
          }}>
          ☆
        </Text>
        <Text
          style={{
            color: colors.white,
            fontSize: textScale(11),
            marginHorizontal: moderateScale(2),
            fontFamily: fontFamily.bold,
          }}>
          4.5
        </Text>
      </View>

      <Text
        numberOfLines={2}
        style={{
          fontSize: textScale(14),
          fontFamily: fontFamily.bold,
          color: colors.black,
          marginHorizontal: moderateScale(15),
          marginTop: moderateVerticalScale(5),
        }}>
        {data?.name}
      </Text>

      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-around',
          marginVertical: moderateVerticalScale(10),
        }}>
        <Text
          numberOfLines={1}
          style={{
            fontSize: textScale(14),
            fontFamily: fontFamily.regular,
            color: colors.black,
            marginHorizontal: moderateScale(5),
          }}>
          {`$${data?.price}`}
        </Text>
        <TouchableOpacity onPress={() => addTocart(data?.id, data?.price)}>
          <Text
            style={{
              fontSize: textScale(12),
              fontFamily: fontFamily.regular,
              color: ThemeColors?.primary_color,
            }}>
            ADD TO CART
          </Text>
        </TouchableOpacity>
      </View>
    </TouchableOpacity>
  );
}
