import React from 'react';
import {StyleSheet} from 'react-native';
import colors from '../../styles/colors';
import commonStyles from '../../styles/commonStyles';
import fontFamily from '../../styles/fontFamily';
import {
  height,
  moderateScale,
  moderateVerticalScale,
  textScale,
  width,
} from '../../styles/responsiveSize';

export function stylesFunc({fontFamily}) {
  const styles = StyleSheet.create({
    headerContiner: {
      paddingVertical: moderateVerticalScale(20),
    },
    screenTitleTextStyle: {
      fontFamily: fontFamily.bold,
      fontSize: textScale(18),
      marginHorizontal: moderateScale(20),
      marginVertical: moderateVerticalScale(20),
    },
    socialHeadingTextStyle: {
      fontFamily: fontFamily.bold,
      fontSize: textScale(16),
      marginHorizontal: moderateScale(20),
    },
    socialIconContainer: {
      flexDirection: 'row',
      marginHorizontal: moderateScale(20),
      justifyContent: 'space-between',
    },
    socialIconInnerContainer: {
      flexDirection: 'row',
      borderColor: colors.textGreyLight,
      borderWidth: 0.5,
      width: width / 2.35,
      justifyContent: 'center',
      alignItems: 'center',
      borderRadius: 5,
      paddingVertical: moderateVerticalScale(10),
    },
    descriptionTextStyle: {
      fontSize: textScale(14),
      fontFamily: fontFamily.medium,
      textAlign: 'center',
    },
    forgotPasswordText: {
      textAlign: 'right',
      fontFamily: fontFamily.medium,
      fontSize: textScale(14),
    },
    createNewAccountText: {
      fontFamily: fontFamily.medium,
      fontSize: textScale(14),
    },
    signUpTextStyle: {
      marginHorizontal: moderateScale(8),
      fontFamily: fontFamily.medium,
      fontSize: textScale(14),
      color: colors.themeColor,
    },
  });
  return styles;
}
