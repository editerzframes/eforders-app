import {StyleSheet} from 'react-native';
import colors from '../../styles/colors';
import fontFamily from '../../styles/fontFamily';
import {
  height,
  moderateScale,
  moderateVerticalScale,
  textScale,
  width,
} from '../../styles/responsiveSize';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.backgroundColor,
  },
  topViewContainer: {flex: 0.25, alignItems: 'center'},

  welcomeTextStyle: {
    fontSize: textScale(20),
    fontFamily: fontFamily.semiBold,
    textAlign: 'center',
    marginHorizontal: moderateScale(20),
  },
  outerScreenBannerImage: {
    flex: 0.47,
    justifyContent: 'center',
    alignItems: 'center',
  },
  socailLoginMainContainer: {
    flex: 0.25,
    justifyContent: 'center',
  },
  socailLoginInnerContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginHorizontal: moderateScale(5),
  },
});
