import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import colors from '../styles/colors';
import {
  height,
  moderateVerticalScale,
  textScale,
  width,
} from '../styles/responsiveSize';

export default function CustomButton({btnText, onPress}) {
  const {ThemeColors, fontFamily} = useSelector(
    state => state?.init?.initAppData,
  );
  return (
    <TouchableOpacity onPress={() => onPress()}>
      <View
        style={{
          width: '95%',
          alignSelf: 'center',
          height: moderateVerticalScale(50),
          backgroundColor: colors.black,
          alignItems: 'center',
          justifyContent: 'center',

          borderRadius: 40,
        }}>
        <Text
          style={{
            color: colors.white,
            textAlign: 'center',
            fontFamily: fontFamily.semiBold,
            fontSize: textScale(16),
          }}>
          {btnText}
        </Text>
      </View>
    </TouchableOpacity>
  );
}
