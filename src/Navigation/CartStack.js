import React from 'react';
import navigationStrings from '../constants/navigationStrings';
import {createStackNavigator} from '@react-navigation/stack';
import {
  Account,
  Basket,
  Checkout,
  CouponCards,
  Login,
  OrderDetails,
} from '../Screens';

const Stack = createStackNavigator();

export default function CartStack() {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name={navigationStrings.BASKET}
        options={{
          headerShown: false,
        }}
        component={Basket}
      />
      <Stack.Screen
        name={navigationStrings.CHECKOUT}
        options={{
          headerShown: false,
        }}
        component={Checkout}
      />
      <Stack.Screen
        name={navigationStrings.COUPONCARDS}
        options={{
          headerShown: false,
        }}
        component={CouponCards}
      />
      <Stack.Screen
        name={navigationStrings.ORDERDETAILS}
        options={{
          headerShown: false,
        }}
        component={OrderDetails}
      />
    </Stack.Navigator>
  );
}
