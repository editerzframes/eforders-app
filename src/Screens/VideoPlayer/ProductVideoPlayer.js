import React, {useState} from 'react';
import {View, Text} from 'react-native';

import Header from '../../Components/Header';
import ProductIntroCrousel from '../../Components/ProductIntroCrousel';
import WrapperContainer from '../../Components/WrapperContainer';
import imagePath from '../../constants/imagePath';
import strings from '../../constants/lang';
import colors from '../../styles/colors';
import fontFamily from '../../styles/fontFamily';
import {stylesFunc} from './styles';
import VideoPlayer from 'react-native-video-controls';

export default function ProductVideoPlayer(props) {
  const {videoUrl} = props?.route?.params;

  const styles = stylesFunc({fontFamily});

  return (
    <WrapperContainer bgColor={colors.white}>
      <View style={styles.mainContainer}>
        <View style={styles.productVideoContainer}>
          <VideoPlayer
            navigator={props?.navigation}
            disableVolume={true}
            source={{uri: videoUrl}}
          />
        </View>
      </View>
    </WrapperContainer>
  );
}
