import React from 'react';
import {
  Login,
  LoginOuterScreen,
  OuterScreen,
  Register,
  SetLocationInMap,
  SignInWithPhone,
} from '../Screens';
import navigationStrings from '../constants/navigationStrings';

export default function (Stack) {
  return (
    <>
      <Stack.Screen
        name={navigationStrings.OUTERSCREEN}
        component={OuterScreen}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name={navigationStrings.LOGIN}
        component={Login}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name={navigationStrings.LOGINOUTERSCREEN}
        component={LoginOuterScreen}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name={navigationStrings.SIGNINWITHPHONE}
        component={SignInWithPhone}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name={navigationStrings.REGISTER}
        component={Register}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name={navigationStrings.SETLOCATIONINMAP}
        component={SetLocationInMap}
        options={{headerShown: false}}
      />
    </>
  );
}
