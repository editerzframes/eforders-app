import {
  apiGet,
  apiPost,
  clearUserData,
  setUserData,
  cartBadge,
} from '../../utils/utils';
import {
  APP_INITIAL_SETTINGS,
  EMPTYCART,
  SEARCHCOUPON,
  SINGLEITEMREMOVECART,
} from '../../config/urls';
import ActionType from '../types';
import store from '../store';

export function showCartBadge(data = {}) {
  cartBadge(data).then(suc => {
    store.dispatch({
      type: ActionType.CARTBADGE,
      payload: data,
    });
  });
}

export function singleItemRemoveFromCart(headers = {}, data = {}) {
  return new Promise((resolve, reject) => {
    apiPost(SINGLEITEMREMOVECART, data, headers)
      .then(res => {
        resolve(res);
      })
      .catch(error => {
        reject(error);
      });
  });
}

export function AllRemoveFromCart(headers = {}, data = {}) {
  return new Promise((resolve, reject) => {
    apiPost(EMPTYCART, data, headers)
      .then(res => {
        resolve(res);
      })
      .catch(error => {
        reject(error);
      });
  });
}

export function SearchCounpon(query = '', data = {}, headers = {}) {
  return new Promise((resolve, reject) => {
    apiGet(SEARCHCOUPON + query, data, headers)
      .then(res => {
        resolve(res);
      })
      .catch(error => {
        reject(error);
      });
  });
}
