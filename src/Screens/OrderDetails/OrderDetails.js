import React, {useEffect, useRef, useState} from 'react';
import {
  View,
  Text,
  FlatList,
  Image,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import Header from '../../Components/Header';
import WrapperContainer from '../../Components/WrapperContainer';
import imagePath from '../../constants/imagePath';
import strings, {changeLaguage} from '../../constants/lang';
import colors from '../../styles/colors';
import {
  height,
  moderateScale,
  moderateVerticalScale,
  textScale,
  width,
} from '../../styles/responsiveSize';
import {getColorCodeWithOpactiyNumber} from '../../utils/helperFunctions';
import DropDownPicker from 'react-native-dropdown-picker';
import fontFamily from '../../styles/fontFamily';
import actions from '../../redux/actions';
import {useFocusEffect} from '@react-navigation/native';
import ModalView from '../../Components/Modal';
import {setItem} from '../../utils/utils';
import RNRestart from 'react-native-restart';
import {useSelector} from 'react-redux';
import {stylesFunc} from './styles';
import MapView, {PROVIDER_GOOGLE} from 'react-native-maps';
import MapViewDirections from 'react-native-maps-directions';
import CustomStepIndicator from '../../Components/CustomStepIndicator';
import LeftRightText from '../../Components/LeftRightText';

export default function OrderDetails() {
  const primaryLanguage = useSelector(state => state?.home?.primaryLanguage);
  console.log(primaryLanguage, 'primaryLanguage');
  const [state, setState] = useState({
    coordinates: [
      {latitude: 30.72267, longitude: 76.79825},
      {latitude: 30.7181, longitude: 76.8043},
    ],
  });

  const {coordinates} = state;
  const updateState = data => setState(state => ({...state, ...data}));
  const {ThemeColors, fontFamily} = useSelector(
    state => state?.init?.initAppData,
  );
  const styles = stylesFunc({fontFamily, ThemeColors});
  const mapRef = useRef();

  // Call fitToSuppliedMarkers() method on the MapView after markers get updated
  useEffect(() => {
    if (mapRef.current) {
      // list of _id's must same that has been provided to the identifier props of the Marker
      fitToMap();
    }
  }, [coordinates]);

  const fitToMap = () => {
    if (coordinates && coordinates.length) {
      // animate(region);
      setTimeout(() => {
        // animate(region);
        fitPadding(coordinates);
      }, 500);
    }
  };

  const fitPadding = newArray => {
    // console.log([[{latitude, longitude}, ...newArray], 'newArraynewArray');
    if (mapRef.current) {
      mapRef.current.fitToCoordinates(newArray, {
        edgePadding: {top: 80, right: 80, bottom: 80, left: 80},
        animated: true,
      });
    }
  };

  const OrderProductView = ({item, index}) => {
    return (
      <View style={{alignItems: 'center'}}>
        <Image style={styles.productImagesStyle} source={{uri: item?.image}} />
        <Text style={styles.productQuantityText}>x1</Text>
      </View>
    );
  };

  //

  return (
    <WrapperContainer bgColor={colors.white} statusBarColor={colors.white}>
      <View style={{paddingHorizontal: moderateScale(10)}}>
        <Header
          leftIcon={imagePath.back}
          textStyle={{color: colors.black}}
          centerTitle={strings.ORDERDETAILS}
        />
      </View>
      <ScrollView showsVerticalScrollIndicator={false}>
        <View>
          <View style={{height: moderateVerticalScale(height / 2.8)}}>
            <MapView
              ref={mapRef}
              style={styles.map}
              // customMapStyle={styles.map}
              provider={PROVIDER_GOOGLE}
              region={{
                latitude: 30.72267,
                longitude: 76.79825,
                latitudeDelta: 0.0922,
                longitudeDelta: 0.0421,
              }}>
              {coordinates?.map((coordinate, index) => (
                <MapView.Marker
                  key={`coordinate_${index}`}
                  coordinate={coordinate}
                  image={
                    index ? imagePath.orderByUser : imagePath.productDelivery
                  }
                />
              ))}

              <MapViewDirections
                origin={coordinates[0]}
                destination={coordinates[1]}
                apikey={'AIzaSyC0dlyxmE3ngkM8QnG6WY4exuHtFaLzNTY'} // insert your API Key here
                strokeWidth={4}
                strokeColor={ThemeColors.primary_color}
                optimizeWaypoints={true}
                mode="driving"
              />
            </MapView>
          </View>
          <View style={styles.orderStatusContainer}>
            <Text style={styles.orderStatusTitle}>Order Status</Text>
            <View style={styles.orderstatusStepContainer}>
              <CustomStepIndicator />
            </View>
          </View>
          <View style={styles.productDetailsContainer}>
            <Text style={styles.productDetailsTitle}>Product Details</Text>
            <View
              style={{
                backgroundColor: colors.white,
                paddingVertical: moderateVerticalScale(10),
              }}>
              <FlatList
                horizontal
                data={[
                  {
                    id: 1,
                    image:
                      'https://images.unsplash.com/photo-1505740420928-5e560c06d30e?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8M3x8cHJvZHVjdHxlbnwwfHwwfHw%3D&w=1000&q=80',
                  },
                  {
                    id: 2,
                    image:
                      'https://images.unsplash.com/photo-1523275335684-37898b6baf30?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8Mnx8cHJvZHVjdHxlbnwwfHwwfHw%3D&w=1000&q=80',
                  },
                  {
                    id: 3,
                    image:
                      'https://img.pixelz.com/blog/using-product-images-on-ecommerce-site/F_Purse1_drop.jpg?w=1000',
                  },
                  {
                    id: 4,
                    image:
                      'https://www.feedmotorsports.com/photos/product/4/176/4.jpg',
                  },
                ]}
                showsVerticalScrollIndicator={false}
                keyExtractor={(item, index) => String(index)}
                renderItem={({item, index}) => {
                  return <OrderProductView item={item} index={index} />;
                }}
                ItemSeparatorComponent={() => {
                  return (
                    <View
                      style={{
                        height: moderateVerticalScale(20),
                      }}
                    />
                  );
                }}
              />
            </View>
            <Text style={styles.orderSummerText}>Order Summery</Text>
          </View>

          <LeftRightText
            style={{marginTop: moderateVerticalScale(10)}}
            leftTextStyle={styles.leftTextStyle}
            rightTextStyle={styles.rightTextStyle}
            leftText={'Order Id'}
            rightText={'#254521'}
          />
          <LeftRightText
            style={{marginVertical: moderateVerticalScale(10)}}
            leftTextStyle={styles.leftTextStyle}
            rightTextStyle={styles.rightTextStyle}
            leftText={'Coupon Applied'}
            rightText={'Coupon Name'}
          />
          <LeftRightText
            style={{marginVertical: moderateVerticalScale(10)}}
            leftTextStyle={styles.leftTextStyle}
            rightTextStyle={styles.rightTextStyle}
            leftText={'Total Discount'}
            rightText={'20%'}
          />
          <LeftRightText
            style={{marginVertical: moderateVerticalScale(10)}}
            leftTextStyle={styles.leftTextStyle}
            rightTextStyle={styles.rightTextStyle}
            leftText={'Payment Method'}
            rightText={'Cash On Delivery'}
          />
          <LeftRightText
            style={{marginVertical: moderateVerticalScale(10)}}
            leftTextStyle={styles.leftTextStyle}
            rightTextStyle={styles.rightTextStyle}
            leftText={'Total'}
            rightText={'1000'}
          />
        </View>
      </ScrollView>
    </WrapperContainer>
  );
}
