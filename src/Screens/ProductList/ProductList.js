import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  ScrollView,
  TouchableOpacity,
  Image,
  FlatList,
} from 'react-native';
import {cloneDeep, debounce} from 'lodash';
import {showMessage} from 'react-native-flash-message';
import Header from '../../Components/Header';
import ModalView from '../../Components/Modal';
import WrapperContainer from '../../Components/WrapperContainer';
import imagePath from '../../constants/imagePath';
import strings from '../../constants/lang';
import actions from '../../redux/actions';
import colors from '../../styles/colors';
import fontFamily from '../../styles/fontFamily';
import {
  height,
  moderateScale,
  moderateVerticalScale,
  textScale,
  width,
} from '../../styles/responsiveSize';
import {stylesFunc} from './styles';
import navigationStrings from '../../constants/navigationStrings';
import TextInputWithLabel from '../../Components/TextInputWithLabel';
import {getColorCodeWithOpactiyNumber} from '../../utils/helperFunctions';
import ProductViewContainer from '../../Components/ProductViewContainer';
import LargeCartList from '../../Components/LargeCartList';
import HorizontalListCard from '../../Components/HorizontalListCard';
import {useSelector} from 'react-redux';

import {useFocusEffect} from '@react-navigation/native';

export default function ProductList(props) {
  const {navigation, route} = props;
  const paramData = route?.params;
  console.log(paramData, 'paramData');
  const [state, setState] = useState({
    filters: [
      {
        id: 1,
        filtertitle: 'High to Low',
        key: 'high_to_low',
      },
      {
        id: 2,
        filtertitle: 'Low to high',
        key: 'low_to_high',
      },

      {
        id: 3,
        filtertitle: 'New Arrials',
        key: 'new_arrival',
      },
    ],
    filtersByPrice: [
      {
        id: 1,
        filtertitle: '100-200',
        min_price: 100,
        max_price: 200,
      },
      {
        id: 2,
        filtertitle: '200-500',
        min_price: 200,
        max_price: 500,
      },

      {
        id: 3,
        filtertitle: '500-1000',
        min_price: 500,
        max_price: 1000,
      },
    ],
    isModalVisible: false,
    pageNo: 1,
    limit: 10,
    allLoadedProducts: [],
    selectedFilterWithPrice: null,
    selectedFilterWithRange: null,
    filterModalType: null,
    gridView: true,
    searchedQuery: '',
    allSearchedProducts: [],
  });

  const {
    filters,
    filtersByPrice,
    isModalVisible,
    pageNo,
    limit,
    allLoadedProducts,
    selectedFilterWithPrice,
    selectedFilterWithRange,
    filterModalType,
    gridView,
    searchedQuery,
    allSearchedProducts,
  } = state;
  const updateState = data => setState(state => ({...state, ...data}));

  const logedInUser = useSelector(state => state?.auth?.userData);
  const {ThemeColors, fontFamily} = useSelector(
    state => state?.init?.initAppData,
  );
  const styles = stylesFunc({fontFamily, ThemeColors});

  const setModalVisiblity = type => {
    updateState({
      filterModalType: type,
      isModalVisible: isModalVisible ? false : true,
    });
  };

  useEffect(() => {
    if (searchedQuery !== '') {
      onSearchProduct();
    } else if (searchedQuery == '') {
      if (paramData?.isVendor) {
        allVendorProducts();
      } else {
        loadProducts();
      }
    }
  }, [searchedQuery, pageNo, selectedFilterWithPrice, selectedFilterWithRange]);

  const onSearchProduct = () => {
    let query = `/${searchedQuery}?limit=${limit}&page=${pageNo}`;

    actions
      .searchProduct(query)
      .then(res =>
        updateState({
          allSearchedProducts:
            pageNo == 1
              ? res.data.data
              : [...allSearchedProducts, ...res.data.data],
        }),
      )
      .catch(error => console.log(error, 'error'));
  };

  // useEffect(() => {
  //   // do something
  //   if (paramData?.isVendor) {
  //     allVendorProducts();
  //   } else if (searchedQuery == '') {
  //     loadProducts();
  //   }
  // }, [pageNo, selectedFilterWithPrice, selectedFilterWithRange]);

  const loadProducts = (productId, productPrice) => {
    let query = `/${''}?limit=${limit}&page=${pageNo}`;
    const data = {
      sort: selectedFilterWithRange ? selectedFilterWithRange?.key : null,
      min_price: selectedFilterWithPrice
        ? selectedFilterWithPrice?.min_price
        : null,
      max_price: selectedFilterWithPrice
        ? selectedFilterWithPrice?.max_price
        : null,
    };

    actions
      .productlisting(query, data)
      .then(res => {
        updateState({
          allLoadedProducts:
            pageNo == 1
              ? res.data.data
              : [...allLoadedProducts, ...res.data.data],
        });
        console.log(res, 'res');
        // showMessage({
        //   message: res.message,
        //   type: 'success',
        // });
      })
      .catch(error => {
        showMessage({
          message: error.message,
          type: 'danger',
        });
      });
  };

  const allVendorProducts = (productId, productPrice) => {
    let query = `/${paramData?.selectedVendor?.id}?limit=${limit}&page=${pageNo}`;
    const data = {
      sort: selectedFilterWithRange ? selectedFilterWithRange?.key : null,
      min_price: selectedFilterWithPrice
        ? selectedFilterWithPrice?.min_price
        : null,
      max_price: selectedFilterWithPrice
        ? selectedFilterWithPrice?.max_price
        : null,
    };
    actions
      .vendorProductListing(query, data)
      .then(res => {
        updateState({
          allLoadedProducts:
            pageNo == 1
              ? res.data.data
              : [...allLoadedProducts, ...res.data.data],
        });
        console.log(res, 'res');
      })
      .catch(error => {
        showMessage({
          message: error.message,
          type: 'danger',
        });
      });
  };

  //pagination of data
  const onEndReached = ({distanceFromEnd}) => {
    updateState({pageNo: pageNo + 1});
  };

  const onEndReachedDelayed = debounce(onEndReached, 1000, {
    leading: true,
    trailing: false,
  });

  const selectedFilterForSort = item => {
    updateState({
      selectedFilterWithPrice: item,
    });
  };

  const selectedFilterForSortWithRange = item => {
    updateState({
      selectedFilterWithRange: item,
    });
  };

  const _moveToNextScreen = (item, data) => {
    // console.log(, 'datatatata');
    navigation.navigate(item, {selectedProduct: data?.id});
  };

  const _onChangeText = key => val => {
    updateState({[key]: val});
  };

  const modalMainContent = () => {
    return (
      <View
        style={{
          marginHorizontal: moderateVerticalScale(5),
        }}>
        <TouchableOpacity onPress={() => setModalVisiblity()}>
          <Image
            style={{
              tintColor: colors.black,
              alignSelf: 'flex-end',
              marginTop: moderateVerticalScale(-20),
              marginRight: moderateScale(20),
            }}
            source={imagePath.close}
          />
        </TouchableOpacity>
        {filterModalType == 'filter' && (
          <>
            <Text
              style={{
                marginHorizontal: moderateVerticalScale(5),
                fontFamily: fontFamily.bold,
                color: colors.black,
                fontSize: textScale(14),
                marginVertical: moderateVerticalScale(10),
              }}>
              By Price
            </Text>
            <View
              style={{
                flexDirection: 'row',
                flexWrap: 'wrap',
                alignItems: 'center',
              }}>
              {filtersByPrice && filtersByPrice.length
                ? filtersByPrice.map((item, index) => {
                    return (
                      <View>
                        <TouchableOpacity
                          style={{
                            flexDirection: 'row',
                            borderColor:
                              selectedFilterWithPrice?.id === item?.id
                                ? ThemeColors?.primary_color
                                : colors.textGreyB,
                            borderWidth: 0.5,
                            borderRadius: 4,
                            justifyContent: 'center',
                            alignItems: 'center',
                            marginHorizontal: moderateScale(5),
                            paddingHorizontal: moderateScale(15),
                            paddingVertical: moderateVerticalScale(15),
                            marginVertical: moderateVerticalScale(10),
                          }}
                          key={index}
                          onPress={() => selectedFilterForSort(item)}>
                          <View>
                            <Text
                              style={{
                                fontSize: 15,
                                fontFamily: fontFamily.medium,
                                color: colors.greyLight,
                              }}>
                              {item?.filtertitle}
                            </Text>
                          </View>
                        </TouchableOpacity>
                      </View>
                    );
                  })
                : null}
            </View>
          </>
        )}
        {filterModalType === 'sort' && (
          <View style={{marginVertical: moderateVerticalScale(20)}}>
            <Text
              style={{
                marginHorizontal: moderateVerticalScale(5),
                fontFamily: fontFamily.bold,
                color: colors.black,
                fontSize: textScale(14),
                marginVertical: moderateVerticalScale(10),
              }}>
              Sort by
            </Text>
            <View
              style={{
                flexDirection: 'row',
                flexWrap: 'wrap',
                alignItems: 'center',
              }}>
              {filters && filters.length
                ? filters.map((item, index) => {
                    return (
                      <View>
                        <TouchableOpacity
                          style={{
                            flexDirection: 'row',
                            borderColor:
                              selectedFilterWithRange?.id === item?.id
                                ? ThemeColors?.primary_color
                                : colors.textGreyB,
                            borderWidth: 0.5,
                            borderRadius: 4,
                            justifyContent: 'center',
                            alignItems: 'center',
                            marginHorizontal: moderateScale(5),
                            paddingHorizontal: moderateScale(10),
                            paddingVertical: moderateVerticalScale(10),
                            marginVertical: moderateVerticalScale(10),
                          }}
                          key={index}
                          onPress={() => selectedFilterForSortWithRange(item)}>
                          <View>
                            <Text
                              style={{
                                fontSize: 15,
                                fontFamily: fontFamily.medium,
                                color: colors.greyLight,
                              }}>
                              {item.filtertitle}
                            </Text>
                          </View>
                        </TouchableOpacity>
                      </View>
                    );
                  })
                : null}
            </View>
          </View>
        )}
      </View>
    );
  };

  const setListViewType = () => {
    updateState({
      gridView: !gridView,
    });
  };

  const addToCart = (productId, productPrice) => {
    const data = {
      product_id: productId,
    };
    actions
      .addToCart(logedInUser?.access_token, data)
      .then(res => {
        showMessage({
          message: res.message,
          type: 'success',
        });
        getCartData();
        //actions.showCartBadge(1);
      })
      .catch(error => {
        showMessage({
          message: error.message,
          type: 'danger',
        });
      });
  };

  // get Cart data

  const getCartData = () => {
    actions
      .getCart({}, logedInUser?.access_token)
      .then(res => {
        console.log(res, 'response');
        updateState({
          cartBadge: res?.data?.total_quantity,
        });
        actions.showCartBadge(res?.data?.item_count);
      })
      .catch(error => {
        showMessage({
          message: error.message,
          type: 'danger',
        });
      });
  };

  return (
    <WrapperContainer>
      <View style={styles.mainContainer}>
        <View style={styles.headerContainer}>
          <Header
            leftIcon={imagePath.back}
            textStyle={{color: colors.black}}
            centerTitle={
              paramData?.selectedVendor?.name
                ? paramData?.selectedVendor?.name
                : strings.ALLPRODUCTS
            }
            // rightIcon={imagePath.filter}

            headerStyle={{marginHorizontal: moderateScale(10)}}
          />
        </View>
        <View
          style={{
            marginHorizontal: moderateScale(10),
            marginTop: moderateVerticalScale(-10),
          }}
          onPress={() =>
            _moveToNextScreen(navigationStrings.GLOBALSEARCH, 'search')
          }>
          <TextInputWithLabel
            placeholder={strings.SEARCHPRODUCT}
            customTextStyle={{
              height: moderateVerticalScale(50),
              borderColor: getColorCodeWithOpactiyNumber(
                colors.textGreyLight.substr(1),
                20,
              ),
              backgroundColor: getColorCodeWithOpactiyNumber(
                colors.textGreyLight.substr(1),
                20,
              ),
              paddingHorizontal: moderateScale(15),
              fontFamily: fontFamily.regular,
            }}
            editable={false}
            rightIcon={imagePath.search}
            userFocus={focus => <></>}
            onChangeText={_onChangeText('searchedQuery')}
          />
        </View>

        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-around',
            marginVertical: moderateVerticalScale(16),
          }}>
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <View
              style={{
                width: width / 2,
              }}>
              {gridView ? (
                <TouchableOpacity onPress={setListViewType}>
                  <Image
                    style={{marginHorizontal: moderateScale(10)}}
                    source={imagePath.grid}
                  />
                </TouchableOpacity>
              ) : (
                <TouchableOpacity onPress={setListViewType}>
                  <Image
                    style={{marginHorizontal: moderateScale(10)}}
                    source={imagePath.list}
                  />
                </TouchableOpacity>
              )}
            </View>
          </View>
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <TouchableOpacity
              style={{flexDirection: 'row', alignItems: 'center'}}
              onPress={() => setModalVisiblity('filter')}>
              <Text
                style={{
                  fontSize: textScale(15),
                  color: colors.black,
                  fontFamily: fontFamily.medium,
                }}>
                {strings.FILTER}
              </Text>
              {/* <Image source={imagePath.filter} /> */}
            </TouchableOpacity>
            <TouchableOpacity
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                marginHorizontal: moderateScale(16),
              }}
              onPress={() => setModalVisiblity('sort')}>
              <Text
                style={{
                  fontSize: textScale(15),
                  color: colors.black,
                  fontFamily: fontFamily.medium,
                }}>
                {strings.SORTBY}
              </Text>
              {/* <Image source={imagePath.sort} /> */}
            </TouchableOpacity>
          </View>
        </View>

        <View
          style={[
            allSearchedProducts?.length
              ? {marginLeft: moderateScale(8)}
              : {
                  alignItems: 'center',
                },
          ]}>
          <FlatList
            numColumns={gridView ? 2 : 1}
            key={gridView ? 2 : 1}
            data={
              allSearchedProducts?.length || searchedQuery != ''
                ? allSearchedProducts
                : allLoadedProducts
            }
            showsVerticalScrollIndicator={false}
            renderItem={({item, index}) => {
              return gridView ? (
                <ProductViewContainer
                  data={item}
                  addTocart={addToCart}
                  onPress={() =>
                    _moveToNextScreen(navigationStrings.PRODUCT_DETAILS, item)
                  }
                />
              ) : (
                <HorizontalListCard
                  data={item}
                  addTocart={addToCart}
                  onPress={() =>
                    _moveToNextScreen(navigationStrings.PRODUCT_DETAILS, item)
                  }
                />
              );
            }}
            onEndReached={onEndReachedDelayed}
            onEndReachedThreshold={0.5}
            ItemSeparatorComponent={() => {
              return (
                <View
                  style={{
                    height: moderateVerticalScale(1),
                  }}
                />
              );
            }}
            ListFooterComponent={() => {
              return (
                <View
                  style={{
                    height: gridView
                      ? moderateVerticalScale(170)
                      : moderateVerticalScale(170),
                  }}
                />
              );
            }}
          />
        </View>
        <ModalView
          mainViewStyle={{width: width}}
          modalStyle={{
            position: 'absolute',
            bottom: 0,
            marginHorizontal: moderateScale(0),
            marginVertical: 0,
          }}
          isVisible={isModalVisible}
          modalMainContent={modalMainContent}
        />
      </View>
    </WrapperContainer>
  );
}
