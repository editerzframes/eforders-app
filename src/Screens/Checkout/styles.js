import {StyleSheet} from 'react-native';
import colors from '../../styles/colors';
import {
  height,
  moderateScale,
  moderateVerticalScale,
  textScale,
  width,
} from '../../styles/responsiveSize';
import {getColorCodeWithOpactiyNumber} from '../../utils/helperFunctions';

export function stylesFunc({fontFamily, ThemeColors}) {
  const styles = StyleSheet.create({
    addressViewContainer: {
      marginHorizontal: moderateScale(10),
      paddingHorizontal: moderateScale(10),
      paddingVertical: moderateVerticalScale(20),
      overflow: 'hidden',
      shadowColor: colors.black,
      shadowOffset: {width: 0, height: 1},
      shadowOpacity: 0.8,
      shadowRadius: 2,
      elevation: 5,
      backgroundColor: colors.white,
      borderRadius: 5,
      marginVertical: moderateVerticalScale(10),
    },
    addressViewInnerContainer1: {
      flex: 0.22,
      flexDirection: 'row',
      justifyContent: 'space-between',
    },
    addressViewInnerContainer2: {
      flex: 1,
      flexDirection: 'row',
      alignItems: 'center',
    },
    shppingAddressTitleText: {
      fontSize: textScale(16),
      fontFamily: fontFamily.bold,
      marginHorizontal: moderateScale(10),
    },
    shppingAddressTextContainer: {
      flex: 0.5,
      alignItems: 'center',
      marginRight: moderateScale(20),
      paddingVertical: moderateVerticalScale(10),
    },
    checkoutProductContainer: {
      height: moderateVerticalScale(200),
      width: width - 20,
      overflow: 'hidden',
      shadowColor: colors.black,
      shadowOffset: {width: 0, height: 1},
      shadowOpacity: 0.8,
      shadowRadius: 2,
      elevation: 5,
      backgroundColor: colors.white,
      borderRadius: 5,
      alignSelf: 'center',
    },
    checkoutProductInnerContainer: {
      flex: 0.2,
      justifyContent: 'center',
      borderBottomWidth: 0.5,
      borderColor: colors.textGreyLight,
    },
    productImageContainer: {
      flex: 0.6,
      paddingVertical: moderateVerticalScale(4),
      paddingHorizontal: moderateScale(10),
      flexDirection: 'row',
    },
    productBottomViewContainer: {
      flex: 0.2,
      backgroundColor: getColorCodeWithOpactiyNumber(
        colors.textGreyLight.substr(1),
        20,
      ),
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'space-between',
    },
    PlaceOrderButton: {
      position: 'absolute',
      backgroundColor: ThemeColors.primary_color,
      paddingHorizontal: moderateScale(35),
      paddingVertical: moderateVerticalScale(14),
      borderRadius: moderateScale(20),
      bottom: moderateScale(20),
      right: moderateScale(20),
    },

    paymentSummeryAndCouponView: {
      marginHorizontal: moderateScale(10),
      marginVertical: moderateVerticalScale(10),
      overflow: 'hidden',
      shadowColor: colors.black,
      shadowOffset: {width: 0, height: 1},
      shadowOpacity: 0.8,
      shadowRadius: 2,
      elevation: 5,
      backgroundColor: colors.white,
      borderRadius: 5,
      paddingHorizontal: moderateScale(15),
      paddingVertical: moderateVerticalScale(15),
      marginVertical: moderateVerticalScale(20),
    },
  });

  return styles;
}
