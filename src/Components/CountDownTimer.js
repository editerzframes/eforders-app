import {View, Text} from 'react-native';
import React from 'react';
import CountDown from 'react-native-countdown-component';
import {useSelector} from 'react-redux';
import colors from '../styles/colors';
import {textScale} from '../styles/responsiveSize';

export default function CountDownTimer({DealTime = 60 * 60 * 100}) {
  const {ThemeColors, fontFamily} = useSelector(
    state => state?.init?.initAppData,
  );

  return (
    <CountDown
      size={10}
      until={DealTime}
      //onFinish={() => alert('Finished')}
      digitTxtStyle={{
        color: colors.green,
        fontFamily: fontFamily?.regular,
        fontSize: textScale(10),
      }}
      digitStyle={{
        backgroundColor: 'rgba(0,128,0,0.05)',
      }}
      timeLabelStyle={{color: colors.white}}
      separatorStyle={{color: colors.textGreyLight}}
      timeToShow={['D', 'H', 'M', 'S']}
      timeLabels={{m: null, s: null}}
      showSeparator
    />
  );
}
