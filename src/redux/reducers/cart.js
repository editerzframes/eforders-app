import ActionTypes from '../types';

const initialState = {
  cartBadge: 0,
};
export default function cart(state = initialState, action) {
  switch (action.type) {
    case ActionTypes.CARTBADGE:
      const cartBadge = action.payload;

      return {
        ...state,
        cartBadge: cartBadge,
      };

    default:
      return state;
  }
}
