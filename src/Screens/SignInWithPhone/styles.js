import {StyleSheet} from 'react-native';
import colors from '../../styles/colors';
import fontFamily from '../../styles/fontFamily';
import {
  height,
  moderateScale,
  moderateVerticalScale,
  textScale,
  width,
} from '../../styles/responsiveSize';

export const CELL_SIZE = 55;
export const CELL_BORDER_RADIUS = 8;
export const DEFAULT_CELL_BG_COLOR = '#fff';
export const NOT_EMPTY_CELL_BG_COLOR = '#3557b7';
export const ACTIVE_CELL_BG_COLOR = '#f7fafe';

export const styles = StyleSheet.create({
  container: {
    height: height,
    backgroundColor: colors.black,
  },
  InnerMainBlackContainer: {
    height: height,
    backgroundColor: colors.black,
  },
  userInfoContainer: {
    flex: 0.08,
    backgroundColor: colors.black,
    justifyContent: 'center',
    marginHorizontal: moderateScale(20),
  },
  InnerMainWhiteContainer: {
    flex: 0.92,
    backgroundColor: colors.backgroundColor,
    borderTopLeftRadius: 50,
    borderTopRightRadius: 50,
  },
  screenTitle: {
    color: colors.white,
    fontFamily: fontFamily.bold,
    fontSize: textScale(20),
  },
  topViewContainer: {flex: 0.25, alignItems: 'center'},

  welcomeTextStyle: {
    fontSize: textScale(20),
    fontFamily: fontFamily.semiBold,
    textAlign: 'center',
    marginHorizontal: moderateScale(20),
  },
  welcomeTextStyle1: {
    fontSize: textScale(16),
    fontFamily: fontFamily.Medium,
    textAlign: 'center',
    marginHorizontal: moderateScale(40),
    marginTop: moderateVerticalScale(10),
  },
  searchBarView: {
    height: moderateVerticalScale(height / 15),
    width: moderateScale(width / 1.25),
    borderRadius: 5,
    alignSelf: 'center',
    flexDirection: 'row',
    alignItems: 'center',
  },
  searcTextInput: {
    width: '45%',
    height: textScale(40),
    fontFamily: fontFamily.semiBold,
    fontSize: textScale(14),
    marginTop: moderateVerticalScale(2),
    color: colors.textGreyLight,
    marginLeft: moderateScale(-10),
  },
  searcTextInput1: {
    width: '55%',
    height: textScale(40),
    fontFamily: fontFamily.semiBold,
    fontSize: textScale(14),
    marginTop: moderateVerticalScale(12),
    color: colors.textGreyLight,

    marginLeft: moderateScale(-10),
  },
  buttonInnerViewStyle: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
  },
  btnTextContainer: {flex: 1, justifyContent: 'center', alignItems: 'center'},

  btnTextStyle: {
    color: colors.white,
    textAlign: 'center',
    fontSize: textScale(22),
    fontFamily: fontFamily.semiBold,
    marginTop: moderateVerticalScale(-15),
  },
  rootStyle: {
    height: moderateVerticalScale(60),
    marginHorizontal: moderateScale(20),
    marginVertical: moderateVerticalScale(30),
    justifyContent: 'space-between',
  },
  focusCell: {
    borderColor: colors.themeColor,
  },
  cell: {
    marginHorizontal: moderateScale(10),
    fontSize: textScale(22),
    fontFamily: fontFamily.semiBold,
  },
});
