import React, {Component} from 'react';
import {Text, View, Image} from 'react-native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';

import imagePath from '../constants/imagePath';
import navigationStrings from '../constants/navigationStrings';
import colors from '../styles/colors';
import {Basket, Category, Favourites, Home, Search} from '../Screens';
import AccountsStack from './AccountsStack';
import CustomBottomTabBar from '../Components/CustomBottomTabBar';
import Accounts from './AccountsStack';
import HomeStack from './HomeStack';
import CartStack from './CartStack';

import {moderateScale, moderateVerticalScale} from '../styles/responsiveSize';
import strings from '../constants/lang';
import {useSelector} from 'react-redux';

const Tab = createBottomTabNavigator();

export default function TabRoutes(Stack) {
  const {ThemeColors, fontFamily} = useSelector(
    state => state?.init?.initAppData,
  );
  return (
    <Tab.Navigator
      tabBarOptions={{
        // activeTintColor: colors.themeColor,
        keyboardHidesTabBar: true,
      }}
      tabBar={props => {
        return <CustomBottomTabBar {...props} />;
      }}>
      <Tab.Screen
        name={navigationStrings.HOME_STACK}
        component={HomeStack}
        options={{
          headerShown: false,
          tabBarLabel: strings.HOME,
          tabBarIcon: ({focused, tintColor}) => (
            <Image
              style={{
                tintColor: focused
                  ? ThemeColors?.primary_color
                  : colors.inactiveColor,
              }}
              source={imagePath.home}
            />
          ),
          // unmountOnBlur: true,
        }}
      />
      <Tab.Screen
        name={navigationStrings.CATEGORY}
        component={Category}
        options={{
          headerShown: false,
          tabBarLabel: strings.CATEGORY,
          tabBarIcon: ({focused, color, size}) => (
            <Image
              style={{
                tintColor: focused
                  ? ThemeColors?.primary_color
                  : colors.inactiveColor,
              }}
              source={imagePath.category}
            />
          ),
        }}
      />
      <Tab.Screen
        name={navigationStrings.CART_STACK}
        component={CartStack}
        options={{
          headerShown: false,
          tabBarLabel: strings.CART,

          tabBarIcon: ({focused, color, size}) => (
            <Image
              style={{
                tintColor: focused
                  ? ThemeColors?.primary_color
                  : colors.inactiveColor,
              }}
              source={imagePath.cart}
            />
          ),
        }}
      />

      <Tab.Screen
        name={navigationStrings.ACCOUNT}
        component={Accounts}
        options={{
          headerShown: false,
          tabBarLabel: strings.ACCOUNT,
          tabBarIcon: ({focused, color, size}) => (
            <Image
              style={{
                tintColor: focused
                  ? ThemeColors?.primary_color
                  : colors.inactiveColor,
              }}
              source={imagePath.account}
            />
          ),
        }}
      />
    </Tab.Navigator>
  );
}
