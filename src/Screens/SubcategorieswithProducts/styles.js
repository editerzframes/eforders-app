import {StyleSheet} from 'react-native';
import colors from '../../styles/colors';
import fontFamily from '../../styles/fontFamily';
import {
  height,
  moderateScale,
  moderateVerticalScale,
  textScale,
  width,
} from '../../styles/responsiveSize';

export const styles = StyleSheet.create({
  container: {
    height: height,
    backgroundColor: colors.black,
  },
  InnerMainBlackContainer: {
    height: height,
    backgroundColor: colors.black,
  },
  userInfoContainer: {
    flex: 0.12,
    backgroundColor: colors.black,
    justifyContent: 'center',
    marginHorizontal: moderateScale(20),
  },
  InnerMainWhiteContainer: {
    flex: 0.88,
    backgroundColor: colors.backgroundColor,
    borderTopLeftRadius: 50,
    borderTopRightRadius: 50,
  },
});
