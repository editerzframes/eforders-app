import {StyleSheet} from 'react-native';
import colors from '../../styles/colors';
import fontFamily from '../../styles/fontFamily';
import {moderateScale, textScale, width} from '../../styles/responsiveSize';

export default StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  circularView: {
    height: moderateScale(70),
    width: moderateScale(70),
    borderWidth: 1,
    borderColor: colors.borderColorc,
    borderRadius: moderateScale(width / 8),
    alignItems: 'center',
    justifyContent: 'center',
    overflow: 'hidden',
  },
  categoryText: {
    color: colors.black,
    fontSize: textScale(12),
    fontFamily: fontFamily.bold,
  },
  circularListImage: {height: moderateScale(70), width: moderateScale(70)},
});
