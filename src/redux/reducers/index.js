import {combineReducers} from 'redux';
import auth from './auth';
import home from './home';
import init from './init';
import cart from './cart';

export default combineReducers({
  auth,
  home,
  init,
  cart,
});
