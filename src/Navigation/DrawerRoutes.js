import * as React from 'react';

import {createDrawerNavigator} from '@react-navigation/drawer';
import navigationStrings from '../constants/navigationStrings';
import DrawerContent from '../Components/DrawerContent';

import TabRoutes from './TabRoutes';
import {
  height,
  moderateScale,
  moderateVerticalScale,
  width,
} from '../styles/responsiveSize';
import {Platform} from 'react-native';

const Drawer = createDrawerNavigator();

export default function DrawerRoutes() {
  return (
    <>
      <Drawer.Navigator
        screenOptions={{
          drawerType: 'front',
          drawerStyle: {
            // backgroundColor: 'red',
            borderTopRightRadius: moderateScale(50),
            borderBottomRightRadius: moderateScale(50),
            marginTop: Platform?.OS
              ? moderateVerticalScale(35)
              : moderateVerticalScale(0),
          },
        }}
        drawerContent={props => <DrawerContent {...props} />}>
        <Drawer.Screen
          name={navigationStrings.TAB_ROUTES}
          component={TabRoutes}
          options={{
            headerShown: false,
          }}
        />
      </Drawer.Navigator>
    </>
  );
}
