import {useNavigation} from '@react-navigation/native';
import React from 'react';
import {
  I18nManager,
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {useSelector} from 'react-redux';
import imagePath from '../constants/imagePath';
import colors from '../styles/colors';
import fontFamily from '../styles/fontFamily';

import {
  moderateScale,
  StatusBarHeight,
  textScale,
} from '../styles/responsiveSize';

const Header = ({
  leftIcon = imagePath.back,
  centerTitle,
  textStyle = {},
  horizontLine = true,
  rightIcon = '',
  onPressLeft,
  onPressRight,
  customRight,
  hideRight = true,
  headerStyle,
  noLeftIcon = false,
  rightViewStyle = {},
  customLeft,
  rightIconStyle = {},
  showImageAlongwithTitle = false,
  imageAlongwithTitle = imagePath.dropDownSingle,
  imageAlongwithTitleStyle = {tintColor: colors.black},
  onPressImageAlongwithTitle,
  onPressCenterTitle,
  leftIconStyle,
}) => {
  const {ThemeColors, fontFamily} = useSelector(
    state => state?.init?.initAppData,
  );
  const styles = stylesFunc({fontFamily, ThemeColors});
  const navigation = useNavigation();
  return (
    <>
      <View
        style={{
          ...headerStyle,
          ...styles.headerStyle,
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'space-between',
        }}>
        <View
          style={{
            alignItems: 'flex-start',
            flex: 0.2,
            ...rightViewStyle,
          }}>
          {!noLeftIcon &&
            (customLeft ? (
              customLeft()
            ) : (
              <TouchableOpacity
                hitSlop={{
                  top: 50,
                  right: 50,
                  left: 50,
                  bottom: 50,
                }}
                activeOpacity={0.7}
                onPress={
                  !!onPressLeft
                    ? onPressLeft
                    : () => {
                        navigation.goBack();
                      }
                }>
                <Image
                  resizeMode="contain"
                  source={leftIcon}
                  style={{
                    transform: [{scaleX: I18nManager.isRTL ? -1 : 1}],
                    ...leftIconStyle,
                  }}
                />
              </TouchableOpacity>
            ))}
        </View>
        <View
          style={{flex: 0.8, alignItems: 'center', justifyContent: 'center'}}>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Text
              onPress={onPressCenterTitle}
              numberOfLines={1}
              style={{
                ...styles.textStyle,
                ...textStyle,
                // width: moderateScale(150),
              }}>
              {centerTitle}
            </Text>
            {!!showImageAlongwithTitle && (
              <TouchableOpacity onPress={onPressImageAlongwithTitle}>
                <Image
                  style={imageAlongwithTitleStyle}
                  source={imageAlongwithTitle}
                />
              </TouchableOpacity>
            )}
          </View>
        </View>

        <View
          style={{
            flex: 0.2,
            alignItems: 'flex-end',
          }}>
          {!!rightIcon ? (
            <TouchableOpacity onPress={onPressRight}>
              <Image style={rightIconStyle} source={rightIcon} />
            </TouchableOpacity>
          ) : !!customRight ? (
            customRight()
          ) : hideRight ? (
            <View style={{width: 25}} />
          ) : (
            <Image source={imagePath.cartShop} />
          )}
        </View>
        <View style={{position: 'absolute', bottom: 0}}></View>
      </View>
    </>
  );
};
export default Header;

export function stylesFunc({fontFamily, ThemeColors}) {
  const styles = StyleSheet.create({
    headerStyle: {
      // padding: moderateScaleVertical(16),
      height: StatusBarHeight,
    },

    textStyle: {
      color: colors.black2Color,
      fontSize: textScale(18),
      lineHeight: textScale(28),
      textAlign: 'center',
      fontFamily: fontFamily?.bold,
    },
  });
  return styles;
}
